<?php
include('header.php');
?>
<div id="fond_overlay"></div>
<div id="overlay_vert">
	Message Envoyé
</div>
<?php
unset($_SESSION['formulaire_password']); // SESSION POUR LE BLOC MDP
unset($_SESSION['formulaire_email']); // SESSION POUR LE BLOC EMAIL
unset($_SESSION['numero_partie']); 
unset($_SESSION['envoyer_message']); // SESSION POUR LE BLOC MESSAGE

if (isset($_POST['recherche']))
	$_SESSION['recherche'] = $_POST['recherche'];
	
if (isset($_GET['page']))
{
	$numero_page = $_GET['page'];
	$numero_page--;
	$numero_page = 15*$numero_page;
}
else
	$numero_page = 0;
	
$resu = $bdd->prepare('SELECT COUNT(*) AS resultats FROM membres
					WHERE identifiant LIKE :recherche')
					or die(print_r($bdd->errorInfo()));
$resu->execute(array('recherche' => $_SESSION['recherche'].'%'))
					or die(print_r($bdd->errorInfo()));		
$resultat = $resu->fetch();		

$repo = $bdd->prepare('SELECT * FROM membres WHERE identifiant LIKE :recherche 
					ORDER BY identifiant LIMIT '.$numero_page.',15')
					or die(print_r($bdd->errorInfo()));
$repo->execute(array('recherche' => $_SESSION['recherche'].'%'))
					or die(print_r($bdd->errorInfo()));
			
?>			

<div class="contener_total" id="bandeau_recherche">
	<div class="contener_980">
<?php

	echo'
	<div id="recherche_bloc_intro">
		<div id="recherche_intro1">
			<form action="recherche" method="post">
				Votre recherche:';
				
				if(isset($_POST['recherche']) AND $_POST['recherche'] !='')
					echo'<input id="recherche_input" name="recherche" type="text" value="'.stripslashes(htmlspecialchars($_POST['recherche'])).'" />';
				else
					echo'<input id="recherche_input" name="recherche" type="text" placeholder=" continuer la recherche" />';
			
			echo'
			</form>
		</div>
		<div id="recherche_intro2">';
		
			if($resultat['resultats'] > 0)
			{
				echo'<span class="nbr">'.$resultat['resultats'].'</span> résultat';if($resultat['resultats']>1){echo's';}
			}
			
		echo'
		</div>	
	</div>';
	$i = 0;
	while ($donnees = $repo->fetch())
	{
		echo'
		<div class="bloc_recherche" id="'.$donnees['id'].'">
			<div class="recherche_bloc1">';
			
				if(isset($donnees['photo_profil']) 
				AND $donnees['photo_profil'] != ''
				AND $donnees['photo_profil'] != 0)
				{  
					echo'
					<div class="centre_image45">';
					
					$source = getimagesize('images_utilisateurs/'.$donnees['photo_profil']); 	// La photo est la source
					if ($source[0] <= 30 AND $source[1] <= 30)
						echo '<img src="images_utilisateurs/'.$donnees['photo_profil'].'" alt="Photo de profil" />';
					else
						echo '<img src="images_utilisateurs/mini_1_'.$donnees['photo_profil'].'" alt="Photo de profil" />';
					
					echo'
					</div>';
				}
				else
					echo'<img src="images/image_defaut.png" alt="Image"/>';
			
			echo'
				<p>
					<a href="'.urlencode(stripslashes(htmlspecialchars($donnees['identifiant']))).'">
						'.stripslashes(htmlspecialchars($donnees['identifiant'])).'
					</a>
					<br />
					'.$donnees['kp'].' Kp
				
				
				</p>

			</div>';
			
			// REQUETE POUR SAVOIR SI EN ATTENTE OU PAS
			$r_attente = $bdd->prepare('SELECT ami_confirm FROM amis 
					WHERE ami_from=:ami_from1 AND ami_to=:ami_to1
					OR ami_from=:ami_from2 AND ami_to=:ami_to2')
					or die(print_r($bdd->errorInfo()));
			$r_attente->execute(array(
								// SESSION a fait la demande
								'ami_from1' => $_SESSION['id_membre'],
								'ami_to1' => $donnees['id'],
								// On a fait la demande à SESSION
								'ami_from2' => $donnees['id'],
								'ami_to2' => $_SESSION['id_membre']))
								or die(print_r($bdd->errorInfo()));
			$d_attente = $r_attente->fetch();
			
			if($donnees['identifiant'] != $_SESSION['identifiant'])
			{
			
				if(isset($donnees['kp']) AND $donnees['kp'] > 2)
				{
					echo'
					<div class="recherche_defis1" title="Défier"></div>';
				}
				else
				{
					echo'
					<div class="recherche_defis2" title="Défier"></div>';
				}
				
				
				if(isset($d_attente['ami_confirm']) AND $d_attente['ami_confirm'] == 0) // EN ATTENTE
				{
					echo'
					<div class="recherche_bloc7" title="En attente"></div>
					<a href="#" class="message">
						<div class="recherche_bloc4" title="Envoyer un message"></div>
					</a>';
				}
				elseif(isset($d_attente['ami_confirm']) AND $d_attente['ami_confirm'] == 1) // AMI
				{
					echo'
					<div class="recherche_bloc8" title="Ami"></div>
					<a href="#" class="message">
						<div class="recherche_bloc4" title="Envoyer un message"></div>
					</a>';
				}
				else
				{
					echo'
					<a href="ami_post.php?id_ami='.$donnees['id'].'&identifiant='.urlencode(stripslashes(htmlspecialchars($donnees['identifiant']))).'&ajouter">
						<div class="recherche_bloc3" title="Ajouter en ami"></div>
					</a>
					<a href="#" class="message">
						<div class="recherche_bloc4" title="Envoyer un message"></div>
					</a>';
				}
			}
			else
			{
				// C'EST SON COMPTE
				echo'
				<div class="recherche_bloc8" title="Vous" style="margin-top:35px;margin-left:55px;"></div>';
			}
			
		echo'
		</div>';
		
		echo'
		<div id="recherche_message" class="recherche_message" style="display:none;color:grey">
			<p>
				Envoyer un message à 
				<a class="message_to" href="'.urlencode(stripslashes(htmlspecialchars($donnees['identifiant']))).'">
					'.stripslashes(htmlspecialchars($donnees['identifiant'])).'					
				</a>
			</p>
			
			<textarea rows="3" cols="30" id="'.urlencode(stripslashes(htmlspecialchars($donnees['identifiant']))).'" ></textarea>
			<input type="submit" class="envoyer_ajax" value="Envoyer"/>
			<a href="#" class="fermer_message">
				<div class="message_supprimer" style="position:relative;bottom:100px;left:10px;" title="Supprimer" ></div>
			</a>
		</div>';
		$i++;
	}

	if($resultat['resultats'] == 0)
	{
		echo'
		<p id="recherche_aucun">
			<br />
			Aucun joueur trouvé.<br /><br /> Essayez une autre recherche.
		</p>';
	}
		
?>
		<div style="width:980px;height:1px;clear:left;"></div>
	</div>
</div>
<div class="contener_980">
<?php

	$nbre_page = 1;
	$nbr_entrees = $resultat['resultats'];
	
	if (isset ($_GET['page']))
		$current_page = $_GET['page'];
	else
		$current_page = 1;

	$nbr_affichage = 15;
	$nom_page = 'recherche';
	include('pagination.php');

?>
</div>


<?php
include('footer.php');
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
	<script>
	
	var div = document.getElementsByTagName('div'),
	divTaille = div.length;
	var div2 = document.getElementsByTagName('div');

			for (var i = 0 ; i < divTaille ; i++) {
				if (div[i] && div[i].className == 'bloc_recherche')
				{	
					var message = document.getElementsByTagName('a'),
					messageTaille = message.length;
					for (var j = 0 ; j < messageTaille ; j++) {
						if (message[j] && message[j].className == 'message')
						{
							message[j].indice = i;
							message[j].onclick = function() {
									this.parentNode.style.display = 'none';
									this.parentNode.nextElementSibling.style.display="block";
									return false; // on bloque la redirection
							}; 
						}
					}
				}		
			}
			
	var fermer = document.getElementsByTagName('a'),
		fermerTaille = fermer.length;
	for (var i =0; i < fermerTaille; i++) {
		if (fermer[i] && fermer[i].className == 'fermer_message')
		{
			fermer[i].onclick = function() {
				this.previousElementSibling.previousElementSibling.value = '';
				this.parentNode.style.display = 'none';
				this.parentNode.previousElementSibling.style.display="block";
				return false; // on bloque la redirection
			};
		}
	}

	
	var envoyer = document.getElementsByTagName('input'),
		envoyerTaille = envoyer.length;
	for (var i = 0 ; i < envoyerTaille ; i++) {
		if (envoyer[i] && envoyer[i].className == 'envoyer_ajax')
		{
			envoyer[i].onclick = function() { 
				textarea = this.previousElementSibling;
				message = textarea.value;
				destinataire = textarea.id;
				$.ajax({ 
					type: "POST",
					url: "message_post.php",
					data: "message="+message+"&message_to="+destinataire+"&recherche=",
					success: function(msg){
						textarea.value = '';
						textarea.parentNode.style.display = 'none';
						textarea.parentNode.previousElementSibling.style.display="block";
						document.getElementById('overlay_vert').style.display="block";
						document.getElementById('fond_overlay').style.display="block";
						setTimeout(function() {
							document.getElementById('overlay_vert').style.display="none";
							document.getElementById('fond_overlay').style.display="none";
						}, 1000);
						
					},
					error: function(msg){ 
						// On alerte d'une erreur
						alert('Erreur'); 
					}
				});
			};
		}
	}
					
	</script>
	
</body>

</html>