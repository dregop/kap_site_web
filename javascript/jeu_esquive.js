function include(fileName){
document.write("<script type='text/javascript' src='"+fileName+"'></script>" );
}
include('explosion.js');


var CODE_TOUCHE_GAUCHE = 37;
var CODE_TOUCHE_DROITE = 39;
var ALLER_GAUCHE = false;
var ALLER_DROITE = false;

window.requestAnimFrame = (function(){
	return window.requestAnimationFrame       || // La forme standardisée
	window.webkitRequestAnimationFrame || // Pour Chrome et Safari
	window.mozRequestAnimationFrame    || // Pour Firefox
	window.oRequestAnimationFrame      || // Pour Opera
	window.msRequestAnimationFrame     || // Pour Internet Explorer
	function(callback){                   // Pour les élèves du dernier rang
	window.setTimeout(callback, 1000 / 60);
	};
})();

var canvasTerrainContext;
var terrainLongueur = 640;
var terrainLargeur = 450;
var filetLargeur = 6;
var compteur = 0;
var fin = 0;
var vieCollision = 0;
var sablierCollision = 0;


var canvas  = document.querySelector('#canvas');
var canvasTerrainContext = canvas.getContext('2d');

// EXPLOSION -------------------------------------------------------------------
	var particles = [];
	function randomFloat (min, max)
	{
		return min + Math.random()*(max-min);
	}
	function Particle ()
	{
		this.scale = 1.0;
		this.x = 0;
		this.y = 0;
		this.radius = 20;
		this.color = "#000";
		this.velocityX = 0;
		this.velocityY = 0;
		this.scaleSpeed = 0.5;
		
		this.update = function(ms)
		{
		//	shrinking
			this.scale -= this.scaleSpeed * ms / 1000.0;
			
			if (this.scale <= 0)
			{
				this.scale = 0;
			}
			
		//	moving away from explosion center
			this.x += this.velocityX * ms/1000.0;
			this.y += this.velocityY * ms/1000.0;
		};
		
		this.draw = function(canvasTerrainContext)
		{
		//	translating the 2D context to the particle coordinates
			canvasTerrainContext.save();
			canvasTerrainContext.translate(this.x, this.y);
			canvasTerrainContext.scale(this.scale, this.scale);
			
		//	drawing a filled circle in the particle's local space
			canvasTerrainContext.beginPath();
			canvasTerrainContext.arc(0, 0, this.radius, 0, Math.PI*2, true);
			canvasTerrainContext.closePath();
			
			canvasTerrainContext.fillStyle = this.color;
			canvasTerrainContext.fill();
			
			canvasTerrainContext.restore();
		};
	}
	function createExplosion(x, y, color)
	{
		var minSize = 10;
		var maxSize = 30;
		var count = 10;
		var minSpeed = 60.0;
		var maxSpeed = 200.0;
		var minScaleSpeed = 1.0;
		var maxScaleSpeed = 4.0;
		
		
		for (var angle=0; angle<360; angle += Math.round(360/count))
		{
			var particle = new Particle();
			
			particle.x = x;
			particle.y = y;
			
			particle.radius = randomFloat(minSize, maxSize);
			
			particle.color = color;
			
			particle.scaleSpeed = randomFloat(minScaleSpeed, maxScaleSpeed);
			
			var speed = randomFloat(minSpeed, maxSpeed);
			
			particle.velocityX = speed * Math.cos(angle * Math.PI / 180.0);
			particle.velocityY = speed * Math.sin(angle * Math.PI / 180.0);
			
			particles.push(particle);
		}
	}
// FIN EXPLOSION ---------------------------------------------------------------

var vaisseau = {
	x: terrainLongueur/2 ,
	y: terrainLargeur - 20,
	w: 22,
	h: 36,
	corx: -17,
	cory: -18,
	collision : [
		[8, 16],
		[0, -18],
		[-8, 16]
	]
};

var vie = {
	x: Math.floor((Math.random() * 580) + 1),
	y: -100,
	w: 40,
	h: 40,
	corx : -31,
	cory: -32,
	collision : [
		[10, -20],
		[18, -1],
		[0, 20],
		[-20, -1],
		[-10, -20]
	]
};	

var sablier = {
	x: Math.floor((Math.random() * 580) + 1),
	y: -100,
	w: 40,
	h: 40,
	corx : -31,
	cory: -32,
	collision : [
		[10, -20],
		[18, -1],
		[0, 20],
		[-20, -1],
		[-10, -20]
	]
};	

var caisse = {
	x: Math.floor((Math.random() * 580) + 1),
	y: 100,
	w: 56,
	h: 68,
	corx: -31,
	cory: -32,
	collision : [
		[20, 20],
		[26, 0],
		[20, -30],
		[-25, -28],
		[-25, 2],
		[-15, 15],
		[-15, 30]
	]
};

var caisseBis = {
	x: Math.floor((Math.random() * 580) + 1),
	y: -100,
	w: 56,
	h: 68,
	corx: -31,
	cory: -32,
	collision : [
		[20, 20],
		[26, 0],
		[20, -30],
		[-25, -28],
		[-25, 2],
		[-15, 15],
		[-15, 30]
	]
};

var caisse2 = {
	x: vaisseau.x,
	y: -100,
	w: 60,
	h: 68,
	corx: -38,
	cory: -32,
	collision : [
		[-11, -30],
		[3, -31],
		[13, -27],
		[15, -5],
		[31, 27],
		[10, 33],
		[-10, 24],
		[-28, 5],
		[-30, -10],
	]
};
	
var caisse3 = {
	x: Math.floor((Math.random() * 580) + 1),
	y: -100,
	w: 60,
	h: 68,
	corx: -25,
	cory: -40,
	collision : [
		[-32, -14],
		[5, -27],
		[18, -20],
		[20, -5],
		[16, 20],
		[-32, 33]
	]
};

var caisse4 = {
	x: vaisseau.x,
	y: -100,
	w: 50,
	h: 40,
	corx: -27,
	cory: -25,
	collision : [
		[-22, -16],
		[-8, -22],
		[25, -5],
		[20, 13],
		[-3, 20],
		[-2, 19],
		[-17, 13]
	]
};

var caisse5 = {
	x: Math.floor((Math.random() * 580) + 1),
	y: -100,
	w: 50,
	h: 70,
	corx: -40,
	cory: -35,
	collision : [
		[-4, -33],
		[17, -24],
		[17, 12],
		[7, 35],
		[-22, 26],
		[-18, 10],
		[-21, -21]
	]
};

var caisse5Bis = {
	x: Math.floor((Math.random() * 580) + 1),
	y: -100,
	w: 50,
	h: 70,
	corx: -40,
	cory: -35,
	collision : [
		[-4, -33],
		[17, -24],
		[17, 12],
		[7, 35],
		[-22, 26],
		[-18, 10],
		[-21, -21]
	]
};

	var dessinerTerrain = function() { 
			canvasTerrainContext.font = "18px Arial,Calibri,Geneva";
			canvasTerrainContext.fillStyle = "white";
			canvasTerrainContext.fillText(compteur, 55, 25);
    }
	var dessinerDecompte = function() {
		var decompte= new Image();
		decompte.src = 'images/decompte_jeu.png';
		decompte.onload = function() {
			canvasTerrainContext.fillStyle = "rgba(68,133,147,0.9)";
			canvasTerrainContext.fillRect(0, 0,80, 35);
			canvasTerrainContext.drawImage(decompte, 20, 5); 
		}
	}

var dessinerVaisseau = function() {
	var vaisseau_img = new Image();
	vaisseau_img.src = 'images/vaisseau.png';
	vaisseau_img.onload = function() {
		canvasTerrainContext.clearRect(0, 0, terrainLongueur, terrainLargeur);
		canvasTerrainContext.drawImage(vaisseau_img, vaisseau.x + vaisseau.corx, vaisseau.y + vaisseau.cory); 
	};
};
	var couleurBouclier = "rgba(134, 196, 209,0)";
    var dessinerBouclier = function() {
	if (vieCollision != 0)
	{
		couleurBouclier = "red";
		vie.y = 1000;
	}
	canvasTerrainContext.strokeStyle = couleurBouclier;
	canvasTerrainContext.beginPath(); // La bouche, un arc de cercle
	canvasTerrainContext.arc(vaisseau.x, vaisseau.y, 20, Math.PI, 0); // Ici aussi
	canvasTerrainContext.stroke();
    } 

var animerVaisseau = function() {
	if (ALLER_DROITE &&  vaisseau.x < terrainLongueur - 12)
	vaisseau.x+=7;
	else if (ALLER_GAUCHE && vaisseau.x > 15)
	vaisseau.x-=7;
};

//faire tomber les caisses aléatoires
var dessinerCaisseAlea = function() {
	var caisse_img = new Image();
	caisse_img.src = 'images/t_Missile.png';
	caisse_img.onload = function() {
		canvasTerrainContext.drawImage(caisse_img, caisse.x + caisse.corx, caisse.y + caisse.cory); 
	};
};

var dessinerCaisseAleaBis = function() {
	var caisse_imgBis = new Image();
	caisse_imgBis.src = 'images/t_Missile.png';
	caisse_imgBis.onload = function() {
		canvasTerrainContext.drawImage(caisse_imgBis, caisseBis.x + caisseBis.corx, caisseBis.y + caisseBis.cory); 
	};	
};

// faire tomber des caisses sur la position du vaisseau
var dessinerCaissePos = function() {
	var caisse_img2 = new Image();
	caisse_img2.src = 'images/missile3.png';
	caisse_img2.onload = function() {
		canvasTerrainContext.drawImage(caisse_img2, caisse2.x + caisse2.corx, caisse2.y + caisse2.cory); 
	};
};

// Caisse qui te poursuivent
var dessinerCaissePoursuit = function() {
	var caisse_img3 = new Image();
	caisse_img3.src = 'images/Rocket.png';
	caisse_img3.onload = function() {
		canvasTerrainContext.drawImage(caisse_img3, caisse3.x + caisse3.corx, caisse3.y + caisse3.cory); 
	};	
};

// Caisse qui vont vite
var dessinerCaisseRapide = function() {
	var caisse_img4 = new Image();
	caisse_img4.src = 'images/t_Missile_Rapide.png';
	caisse_img4.onload = function() {
		canvasTerrainContext.drawImage(caisse_img4, caisse4.x + caisse4.corx, caisse4.y + caisse4.cory); 
	};
};

// meteorite
var dessinerMeteorite = function() {
	var caisse_img5 = new Image();
	caisse_img5.src = 'images/meteorite.png';
	caisse_img5.onload = function() {
		canvasTerrainContext.drawImage(caisse_img5, caisse5.x + caisse5.corx, caisse5.y + caisse5.cory); 
	};
};

var dessinerMeteorite2 = function() {
	var caisse_img6 = new Image();
	caisse_img6.src = 'images/meteorite.png';
	caisse_img6.onload = function() {
		canvasTerrainContext.drawImage(caisse_img6, caisse5Bis.x + caisse5Bis.corx, caisse5Bis.y + caisse5Bis.cory); 
	};
};

// ------------------------- BONUS ---------------------------
var dessinerVie = function() {
	var vieImg = new Image();
	vieImg.src = 'images/vie.png';
	vieImg.onload = function() {
		canvasTerrainContext.drawImage(vieImg, vie.x + vie.corx, vie.y + vie.cory); 
	};
};
var dessinerSablier = function() {
	var sablierImg = new Image();
	sablierImg.src = 'images/sablier.png';
	sablierImg.onload = function() {
		canvasTerrainContext.drawImage(sablierImg, sablier.x + sablier.corx, sablier.y + sablier.cory); 
	};
};
// -----------------------------------------------------------

var animerCaisse = function() {

	if (caisse.y > terrainLargeur + 10)
	{
		caisse.y = 0;
		caisse.x = Math.floor((Math.random() * 580) + 1);
	}
	
	if (caisseBis.y > terrainLargeur + 10)
	{
		caisseBis.y = 0;
		caisseBis.x = Math.floor((Math.random() * 580) + 1);
	}
	
	if (caisse2.y > terrainLargeur + 10)
	{
		caisse2.y = 0;
		caisse2.x = vaisseau.x;
	}
	
	if(vaisseau.x - 4 < caisse3.x && caisse3.x < vaisseau.x + 4 && caisse3.y > 40)
	{
		caisse3.y +=3;
		caisse3.corx = -32;
	}
	else if (caisse3.x > vaisseau.x && caisse3.y > 40)
	{
		caisse3.x -=3;
		caisse3.corx =-32;
	}
	else if (caisse3.x < vaisseau.x && caisse3.y > 40)
	{
		caisse3.x +=3;
		caisse3.corx =-25;
	}
    
	if (caisse3.y > terrainLargeur + 10)
	{
		caisse3.y = 0;
		caisse3.x = Math.floor((Math.random() * 580) + 1);
	}
	
	if (caisse4.y > terrainLargeur + 10)
	{
		caisse4.y = 0;
		caisse4.x = vaisseau.x;
	}
	if (caisse4.y == 0)
	{
		caisse4.x = vaisseau.x;
	}
	if (caisse5.y > terrainLargeur + 10)
	{
		caisse5.y = 0;
		caisse5.x = Math.floor((Math.random() * 580) + 1);
	}
	if (caisse5Bis.y > terrainLargeur + 10)
	{
		caisse5Bis.y = 0;
		caisse5Bis.x = Math.floor((Math.random() * 580) + 1);
	}

	caisse.y+=4;	

	if (compteur > 2)
        caisseBis.y+=5;
	if (compteur > 5)
		caisse2.y+=6;	
	if (compteur > 10)
        caisse3.y+=4;
	if (compteur > 18)
        caisse4.y+=7;

	// A partir de 25 sec
	if (compteur > 25) 
	{
		caisse.y+=2;
		caisseBis.y+=2;
		caisse2.y+=2;
		caisse3.y+=1;
		caisse4.y+=2;
	}

	// -------------------------- BONUS --------------------------
	if (compteur > 5)
		vie.y+=5;		
	if (compteur > 15 && vieCollision != 0)
	{
		vieCollision = 0;		
		couleurBouclier = "rgba(134, 196, 209,0)";
	}
	if (compteur > 2)
		sablier.y+=5;
	if (sablierCollision != 0)
	{
		caisse.y-=2;
		caisseBis.y-=2;
		caisse2.y-=2;
		caisse3.y-=2;
		sablier.y = 1000;
	}
	if (compteur > 12 && sablierCollision != 0)
	{
		sablierCollision = 0;
		caisse.y+=2;
		caisseBis.y+=2;
		caisse2.y+=2;
		caisse3.y+=2;
	}
	// -----------------------------------------------------------
	
	//A partir de 35 sec
	if (compteur > 35)
		caisse5.y+=8;

	// A partir de 40 sec
	if (compteur > 40)
		caisse5Bis.y+=8;

	// A partir de 50 sec
	if (compteur > 55) {
		caisse.y+=2;
		caisseBis.y+=2;
		caisse2.y+=2;
		caisse3.y+=1;
		caisse4.y+=2;
		caisse5.y+=2;
		caisse5Bis.y+=2;
	}
	
}

var initialisation = function() {
	compteur = 0;
	vieCollision = 0;
	sablierCollision = 0;
	setInterval(function() { 
		compteur++;
	}
	,1000);	

	// le code de l'initialisation
	dessinerTerrain();
	dessinerVaisseau();
	dessinerBouclier();
	dessinerCaisseAlea();
	dessinerCaisseAleaBis();
	dessinerCaissePos();
	dessinerCaissePoursuit();
	dessinerCaisseRapide();
	dessinerMeteorite();
	dessinerMeteorite2();
	dessinerVie();
	dessinerSablier();
	dessinerDecompte();

	requestAnimId = window.requestAnimFrame(principale); // premier appel de principale au rafraichissement de la page
}


// Test de collision-box centré + polygone par méthode du point infini
function testerCollisionVaisseauCaisse(elementA, elementB)
{
	var i = 0;
	
	// Rouge
	// canvasTerrainContext.fillStyle = '#ff0000';
	
	// Affichage du centre du vaiseau
	// canvasTerrainContext.beginPath();
	// canvasTerrainContext.arc(elementA.x, elementA.y, 2, 0, Math.PI*2);
	// canvasTerrainContext.fill();
	// canvasTerrainContext.closePath();
	
	// Affichage du centre de la caisse X
	// canvasTerrainContext.beginPath();
	// canvasTerrainContext.arc(elementB.x, elementB.y, 2, 0, Math.PI*2);
	// canvasTerrainContext.fill();
	// canvasTerrainContext.closePath();
	
	// Vert fin
	// canvasTerrainContext.strokeStyle = '#00ff00';
	// canvasTerrainContext.strokeWidth = 1;
	
	// Affichage de la collision box du vaiseau
	// canvasTerrainContext.strokeRect(elementA.x - elementA.w/2, elementA.y - elementA.h/2, elementA.w, elementA.h);
	
	// Affichage de la collision box de la caisse X
	// canvasTerrainContext.strokeRect(elementB.x - elementB.w/2, elementB.y - elementB.h/2, elementB.w, elementB.h);
	
	// Bleu fin
	// canvasTerrainContext.strokeStyle = '#0000ff';
	// canvasTerrainContext.strokeWidth = 1;
	
	// Affichage de la collision polygon du vaiseau
	// canvasTerrainContext.beginPath();
	// canvasTerrainContext.moveTo(elementA.x + elementA.collision[0][0], elementA.y + elementA.collision[0][1]);
	// for(i=1; i<elementA.collision.length; i++)
	// {
		// canvasTerrainContext.lineTo(elementA.x + elementA.collision[i][0], elementA.y + elementA.collision[i][1]);
	// }
	// canvasTerrainContext.lineTo(elementA.x + elementA.collision[0][0], elementA.y + elementA.collision[0][1]);
	// canvasTerrainContext.stroke();
	// canvasTerrainContext.closePath();
	
	// Affichage de la collision polygon de la caisse X
	// canvasTerrainContext.beginPath();
	// canvasTerrainContext.moveTo(elementB.x + elementB.collision[0][0], elementB.y + elementB.collision[0][1]);
	// for(i=1; i<elementB.collision.length; i++)
	// {
		// canvasTerrainContext.lineTo(elementB.x + elementB.collision[i][0], elementB.y + elementB.collision[i][1]);
	// }
	// canvasTerrainContext.lineTo(elementB.x + elementB.collision[0][0], elementB.y + elementB.collision[0][1]);
	// canvasTerrainContext.stroke();
	// canvasTerrainContext.closePath();
	
	// Si les box ne se chevauchent pas
	if
	(
		(elementB.x - elementB.w/2 >= elementA.x + elementA.w/2) ||	// trop à droite
		(elementB.x + elementB.w/2 <= elementA.x - elementA.w/2) ||	// trop à gauche
		(elementB.y - elementB.h/2 >= elementA.y + elementA.h/2) ||	// trop en bas
		(elementB.y + elementB.h/2 <= elementA.y - elementA.h/2)	// trop en haut
	)
	{
		// On arrête les test maintenant
		return false;
	}
	else
	{
		// On fait des test supplémentaires pour augmenter réalisme
		for(i=0; i<elementA.collision.length; i++)
		{
			if(point_infini(elementB, [elementA.x + elementA.collision[i][0], elementA.y + elementA.collision[i][1]]))
			{
				return true;
			}
		}
		
		return false;
	}
}

function intersectsegment(A, B, I, P)
{
	// Variables
	var
		D = [0,0],
		E = [0,0],
		t = null,
		u = null,
		denon = null
	;
	
	// Calcul des vecteurs
	D[0] = B[0] - A[0];
	D[1] = B[1] - A[1];
	E[0] = P[0] - I[0];
	E[1] = P[1] - I[1];
	
	denom = D[0]*E[1] - D[1]*E[0];
	
	// Erreur, cas limite
	if (denom==0)
	{
		return -1;
	}
	
	t = - (A[0]*E[1] - I[0]*E[1] - E[0]*A[1] + E[0]*I[1]) / denom;
	
	if (t<0 || t>=1)
	{
		return 0;
	}
	
	u = - (-D[0]*A[1] + D[0]*I[1] + D[1]*A[0] - D[1]*I[0]) / denom;
	
	if (u<0 || u>=1)
	{
		return 0;
	}
	
	return 1;
}

function point_infini(elementB, P)
{
	// Variables
	var
		// Indice de boucle
		i = null,
		// Point infini aléatoire
		I = [
			10000 + Math.random()*100,
			10000 + Math.random()*100
		],
		// Point segment AB
		A = [0,0],
		B = [0,0],
		// Nombre d'intersections
		nb = 0,
		// Test d'intersections
		iseg = null
	;
	
	// On test pour tous les segments de elementB
	for(i=0; i<elementB.collision.length; i++)
	{
		A[0] = elementB.x + elementB.collision[i][0];
		A[1] = elementB.y + elementB.collision[i][1];
		
		// Si c'est le dernier point, on relie au premier
		if(i == elementB.collision.length-1)
		{
			B[0] = elementB.x + elementB.collision[0][0];
			B[1] = elementB.y + elementB.collision[0][1];
		}
		// Sinon on relie au suivant
		else
		{
			B[0] = elementB.x + elementB.collision[i+1][0];
			B[1] = elementB.y + elementB.collision[i+1][1];
		}
		
		iseg = intersectsegment(A, B, I, P);
		
		// Cas limite, on relance la fonction
		if (iseg == -1)
		{
			return point_infini(elementB, P);
		}
		
		nb+=iseg;
	}
	
	// Si le nombre d'intersections est impaire
	if(nb%2==1)
	{
		// Le point est dans le polygone de elementB
		return true;
	}
	// Sinon le nombre d'intersections est paire
	else
	{
		// Le point n'est pas dans le polygone de elementB
		return false;
	}
}

var principale = function() {

				for (var i=0; i<particles.length; i++)
				{
					var particle = particles[i];
					
					particle.update(17);
					particle.draw(canvasTerrainContext);
				}
				

	(function() { // si l'onglet est changé pendant le jeu, on quitte le jeu
		var hidden = "hidden";

		// Standards:
		if (hidden in document)
			document.addEventListener("visibilitychange", onchange);
		else if ((hidden = "mozHidden") in document)
			document.addEventListener("mozvisibilitychange", onchange);
		else if ((hidden = "webkitHidden") in document)
			document.addEventListener("webkitvisibilitychange", onchange);
		else if ((hidden = "msHidden") in document)
			document.addEventListener("msvisibilitychange", onchange);
		// IE 9 and lower:
		else if ('onfocusin' in document)
			document.onfocusin = document.onfocusout = onchange;
		// All others:
		else
			window.onpageshow = window.onpagehide = window.onfocus = window.onblur = onchange;

		function onchange (evt) {
			var v = 'visible', h = 'hidden',
			evtMap = { 
				focus:v, focusin:v, pageshow:v, blur:h, focusout:h, pagehide:h 
			};

			evt = evt || window.event;
			if (evt.type in evtMap)
				document.body.className = evtMap[evt.type];
			else     
			{
				alert('Merci de na pas changer d\'onglet pendant une partie');
				if (defi == 'off')
				{
					$.get('traitement_esquive.php?onglet', function() { });
					document.location.href="esquive-onglet";
				}
				else
					document.location.href="defi_jeu.php?onglet";
			}
		}
	})();

	// le code du jeu
	animerVaisseau();
	animerCaisse();
	dessinerTerrain();
	dessinerBouclier();
	dessinerVaisseau();
	dessinerCaisseAlea();
	dessinerCaisseAleaBis();
	dessinerCaissePos();
	dessinerCaissePoursuit();
	dessinerCaisseRapide();
	dessinerMeteorite();
	dessinerMeteorite2();
	dessinerVie();
	dessinerSablier();
	dessinerDecompte();

	// IL Y A SESSION TOURNOI OU SESSION ENTRAINEMENT DONC PAS BESOIN DE GERER ESQUIVE
		if (testerCollisionVaisseauCaisse(vaisseau, caisse)) 
		{
			if (vieCollision == 0)
			{
				if (defi == 'off')
					document.location.href="esquive.php?fin_partie&temps="+compteur;
				else
					document.location.href="defi_jeu.php?fin_partie&temps="+compteur;
			}
			else
			{
				// explosion !
				canvasTerrainContext.clearRect(0, 0, terrainLongueur, terrainLargeur);
				createExplosion(caisse.x, caisse.y, "#525252");		
				createExplosion(caisse.x, caisse.y, "#FFA318");
							
				caisse.y = -50;		
				// fin explosion	
			}
		}
		if (testerCollisionVaisseauCaisse(vaisseau, caisseBis)) 
		{
			if (vieCollision == 0)
			{
				if (defi == 'off')
					document.location.href="esquive.php?fin_partie&temps="+compteur;
				else
					document.location.href="defi_jeu.php?fin_partie&temps="+compteur;
			}
			else
			{
				// explosion !
				canvasTerrainContext.clearRect(0, 0, terrainLongueur, terrainLargeur);
				createExplosion(caisseBis.x, caisseBis.y, "#525252");		
				createExplosion(caisseBis.x, caisseBis.y, "#FFA318");
					
				caisseBis.y = -50;	
				// fin explosion		
			}
		}
		if (testerCollisionVaisseauCaisse(vaisseau,caisse2)) 
		{
			if (vieCollision == 0)
			{
				if (defi == 'off')
					document.location.href="esquive.php?fin_partie&temps="+compteur;
				else
					document.location.href="defi_jeu.php?fin_partie&temps="+compteur;
			}
			else
			{
				// explosion !
				canvasTerrainContext.clearRect(0, 0, terrainLongueur, terrainLargeur);
				createExplosion(caisse2.x, caisse2.y, "#525252");		
				createExplosion(caisse2.x, caisse2.y, "#FFA318");
				
				caisse2.y = -50;	
				// fin explosion		
			}
		}
		if (testerCollisionVaisseauCaisse(vaisseau,caisse3)) 
		{
			if (vieCollision == 0)
			{
				if (defi == 'off')
					document.location.href="esquive.php?fin_partie&temps="+compteur;
				else
					document.location.href="defi_jeu.php?fin_partie&temps="+compteur;
			}
			else
			{
				// explosion !
				canvasTerrainContext.clearRect(0, 0, terrainLongueur, terrainLargeur);
				createExplosion(caisse3.x, caisse3.y, "#525252");		
				createExplosion(caisse3.x, caisse3.y, "#FFA318");
				
				caisse3.y = -50;	
				// fin explosion		
			}
		}
		if (testerCollisionVaisseauCaisse(vaisseau,caisse4)) 
		{
			if (defi == 'off')
				document.location.href="esquive.php?fin_partie&temps="+compteur;
			else
				document.location.href="defi_jeu.php?fin_partie&temps="+compteur;
		}
		if (testerCollisionVaisseauCaisse(vaisseau,caisse5)) 
		{
			if (defi == 'off')
				document.location.href="esquive.php?fin_partie&temps="+compteur;
			else
				document.location.href="defi_jeu.php?fin_partie&temps="+compteur;
		}	
		if (testerCollisionVaisseauCaisse(vaisseau,caisse5Bis)) 
		{
			if (defi == 'off')
				document.location.href="esquive.php?fin_partie&temps="+compteur;
			else
				document.location.href="defi_jeu.php?fin_partie&temps="+compteur;
		}

		
	if (testerCollisionVaisseauCaisse(vaisseau,vie)) 
	{
		vieCollision++;
	}
	if (testerCollisionVaisseauCaisse(vaisseau,sablier)) 
	{		
		sablierCollision++;
	}

		requestAnimId = window.requestAnimFrame(principale); // rappel de principale au prochain rafraichissement de la page
}

var onKeyDown = function(event) {
	if ( event.keyCode == CODE_TOUCHE_GAUCHE ) {
		ALLER_GAUCHE = true;
	} else if ( event.keyCode == CODE_TOUCHE_DROITE ) {
		ALLER_DROITE = true; 
	}
}

var onKeyUp = function(event) {
	if ( event.keyCode == CODE_TOUCHE_GAUCHE ) {
		ALLER_GAUCHE = false;
	} else if ( event.keyCode == CODE_TOUCHE_DROITE ) {
		ALLER_DROITE = false;
	}
}

// association des mÃ©thodes aux Ã©vÃ¨nements :
// onKeyDown = Ã  l'appui de la touche
// onKeyUp = au relÃ¨vement de la touche
window.onkeydown = onKeyDown;
window.onkeyup = onKeyUp;

var clique = document.getElementById('clique');
clique.onclick = function() {
	initialisation(); // appel de la fonction initialisation au chargement de la page
	var remplace = document.createElement('div');
		remplace.id = "en_cours";
	var remplaceText = document.createTextNode("En cours");
		remplace.appendChild(remplaceText);
	clique.parentNode.replaceChild(remplace, clique);
	remplace.style.backgroundColor = "#448593";
	remplace.style.paddingTop = "7px";
	remplace.style.paddingBottom = "7px";
	remplace.style.textAlign = "center";
	remplace.style.color = "white";
	remplace.style.fontSize = "large";
	var canvas  = document.querySelector('#canvas');
	canvas.style.backgroundColor = "#dedede";
	canvas.style.backgroundImage = "none";
	if(document.getElementById('jeu_defi_intro'))
	{
		jeu_defi_intro.style.display = "none";
	}
	if(document.getElementById('tournoi_kp'))
	{
		tournoi_kp.style.display = "none";
	}		
		
	if (tournoi == 'on' && defi == 'off' && entrainement == 'off') // C'est tournoi
		$.get('traitement_esquive.php?debut_partie&tournoi', function() { });
	else if (defi == 'on' && tournoi == 'off' && entrainement == 'off' ) // C'est defi
		$.get('defi_jeu.php?debut_partie', function() { });	
	else if (entrainement == 'on' && tournoi == 'off' && defi == 'off' ) // C'est entrainement
		$.get('traitement_esquive.php?debut_partie', function() { });
		
	canvas.style.opacity = "1";
	fin_partie.style.display = "none";
};