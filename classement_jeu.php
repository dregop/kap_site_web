<?php
if((isset($_SESSION['tournoi_lettre'])
AND $_SESSION['tournoi_lettre'] == 'on') OR (isset($_SESSION['tournoi_cible'])
AND $_SESSION['tournoi_cible'] == 'on') )
{
	// TABLEAU CLASSEMENT POUR LE TOURNOI
	echo'
	<table class="affichage_classement_tournoi" id="affichage_classement">';
}
else
{
	// TABLEAU CLASSEMENT POUR ENTRAINEMENT
	echo'
	<table class="affichage_classement" id="affichage_classement" >';
}

	$a = 0;
	$aucun_score = $bdd->prepare('SELECT id FROM '.$classement_jeu.'
							WHERE identifiant=:identifiant')
							or die(print_r($bdd->errorInfo()));
	$aucun_score->execute(array('identifiant' => $_SESSION['identifiant']))
							or die(print_r($bdd->errorInfo()));
	while ($aucune_donnees = $aucun_score->fetch())
	{
		$a++;
	}

	if($a > 0)
	{
		if($classement_jeu =='meilleur_lettre' OR $classement_jeu =='tournoi_meilleur_lettre')
		{
			echo'
			<tr>
				<td style="color:#ca5151;">Place</td>
				<td style="color:#ca5151;">Pseudo</td>
				<td style="color:#ca5151;">Score</td>
				<td style="color:#ca5151;">Temps</td>
			</tr>';
		}
		elseif($classement_jeu =='meilleur_cible' OR $classement_jeu =='tournoi_meilleur_cible')
		{
			echo'
			<tr>
				<td style="color:#528661">Place</td>
				<td style="color:#528661">Pseudo</td>
				<td style="color:#528661">Score</td>
				<td style="color:#528661">Temps</td>
			</tr>';
		}
	}
	
	$i = 0;
	$tableau_score = '';
	$j = 1;
	$nbre = 0;
	$score = $bdd->query('SELECT * FROM '.$classement_jeu.' 
							ORDER BY score DESC,temps DESC')
							or die(print_r($bdd->errorInfo()));
	while ($donnees = $score->fetch())
	{	
		if ($donnees['identifiant'] == $_SESSION['identifiant'])
		{
			$classement = $j;
			$nbre = $classement - 4;
		}
			
		$j++;
	}
	if (isset($classement))
	{
		if ($classement < 4)
			$nbre = 0;
		else if ($j - $classement == 3 AND $classement > 4)
			$nbre = $nbre - 1;
		else if ($j - $classement == 2 AND $classement > 5)
			$nbre = $nbre - 2;
		else if ($j - $classement == 1 AND $classement > 6)
			$nbre = $nbre - 3;

		$r_score = $bdd->query('SELECT * FROM '.$classement_jeu.'  
								ORDER BY score DESC,temps DESC
								LIMIT '.$nbre.',7')
							or die(print_r($bdd->errorInfo()));
		$c = $classement - 3;
		if ($classement == 1)
			$c = $classement;
		else if ($classement == 2)
			$c = $classement - 1;
		else if ($classement == 3)
			$c = $classement - 2;
		else if ($j - $classement == 3 AND $classement > 4)
			$c = $c - 1;
		else if ($j - $classement == 2 AND $classement > 5)
			$c = $c - 2;
		else if ($j - $classement == 1 AND $classement > 6)
			$c = $c - 3;
		while($d_score = $r_score->fetch())
		{
			if($d_score['identifiant'] == $_SESSION['identifiant'])
			{
				echo'
				<tr class="fonce">';
					echo '
					<td>
						'.$c.'
					</td>
					<td>
						'.stripslashes(htmlspecialchars($d_score['identifiant'])).' 
					</td>
					<td>
						'.$d_score['score'].'
					</td>
					<td>
						'.$d_score['temps'].' s
					</td>';
				echo'			
				</tr>';
			}
			else
			{
				echo'
				<tr>';
					echo '
					<td>
						'.$c.'
					</td>
					<td>
						'.stripslashes(htmlspecialchars($d_score['identifiant'])).' 
					</td>
					<td>
						'.$d_score['score'].'
					</td>
					<td>
						'.$d_score['temps'].' s
					</td>';
				
			echo'
			</tr>';
			}
			$i++;	
			$c++;
		}
	}
	else
	{
		if(!isset($_SESSION['tournoi_lettre']) AND !isset($_SESSION['tournoi_cible']))
		{
			echo '
			<tr>
				<td style="border:none;text-align:left;padding-left:15px;">
					Aucun score enregistré. 
				</td>
			</tr>';
		}
		echo'
		<tr>
			<td style="border:none;text-align:left;padding-left:15px;">
				Vous ne pouvez pas être classé...
			</td>
		</tr>';
		if(!isset($_SESSION['tournoi_lettre']) AND !isset($_SESSION['tournoi_cible']))
		{
			echo'
			<tr>
				<td style="border:none;padding-top:30px;">
					Lancez une partie.
				</td>
			</tr>';
		}
	}				
?>
</table>