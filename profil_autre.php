<?php
// REQUETES POUR AVOIR SON NOMBRE TOTAL DE PARTIES ---------------------------//

$r1 = $bdd->prepare('SELECT COUNT(id) AS nombre FROM traitement_esquive
					WHERE identifiant = :identifiant')
					or die(print_r($bdd->errorInfo()));
$r1->execute(array('identifiant' => $_GET['identifiant']))
					or die(print_r($bdd->errorInfo()));		
$esquive = $r1->fetch();	

$r2 = $bdd->prepare('SELECT COUNT(id) AS nombre FROM traitement_lettre
					WHERE identifiant = :identifiant')
					or die(print_r($bdd->errorInfo()));
$r2->execute(array('identifiant' => $_GET['identifiant']))
					or die(print_r($bdd->errorInfo()));		
$lettre = $r2->fetch();

$r3 = $bdd->prepare('SELECT COUNT(id) AS nombre FROM traitement_cible
					WHERE identifiant = :identifiant')
					or die(print_r($bdd->errorInfo()));
$r3->execute(array('identifiant' => $_GET['identifiant']))
					or die(print_r($bdd->errorInfo()));		
$cible = $r3->fetch();

$total = $esquive['nombre'] + $lettre['nombre'] + $cible['nombre'];

//----------------------------------------------------------------------------//

$requete = $bdd->prepare('SELECT * FROM membres 
						WHERE identifiant = :identifiant')
						or die(print_r($bdd->errorInfo()));
$requete->execute(array('identifiant' => $_GET['identifiant']))
						or die(print_r($bdd->errorInfo()));
$donnees = $requete->fetch();
if(!isset($_SESSION['envoyer_message']))
{
	echo'
	<div class="profil_autre" id="profil_autre">
		<div class="image_profil_autre" id="image_profil">';
		
		if(isset($donnees['photo_profil']) AND $donnees['photo_profil'] != '' AND $donnees['photo_profil'] != 0)
		{  
			echo'
			<div class="centre_image120">';
					
			$source = getimagesize('images_utilisateurs/'.$donnees['photo_profil']); 	// La photo est la source
			if ($source[0] <= 150 AND $source[1] <= 150)
				echo '<img src="images_utilisateurs/'.$donnees['photo_profil'].'" alt="Photo de profil" />';
			else
				echo '<img src="images_utilisateurs/mini_1_'.$donnees['photo_profil'].'" alt="Photo de profil" />';
				
			echo'
			</div>';
		}
		else
			echo '<img class="image_defaut_profil" src="images/image_defaut_grd.png" alt="Profil"/>';
?>
		</div>
		<div class="information_profil_autre">
<?php
		echo	
			'<span class="identifiant_profil">'.stripslashes(htmlspecialchars($donnees['identifiant'])).'</span>';
			
			if(isset($donnees['pays']) AND $donnees['pays'] != '')
			{
				echo'
				<p>'.stripslashes(htmlspecialchars($donnees['pays'])).'</p>';
			}
			echo '
			<p>
				'.$total.'
				<span style="font-size:medium;"> 
					partie'; if($total>1)echo's';
				echo'
				</span> 
			</p>
			
			<p>
				79
				<span style="font-size:medium;"> 
					défis
				</span> 
			</p>
			
			<p style="position:relative;bottom:5px;">
				'.$donnees['kp'].'
				<img style="position:relative;top:10px;left:5px;" src="images/kp_icone.png" alt="Kp"/>
			</p>';
?>
		</div>
		
		<div id="bloc_rang">
		
		</div>
		
		<div id="bloc_amis_autre">
<?php
			$compteur_amis = 0;
			$r_compteur = $bdd->prepare('SELECT ami_to, ami_from FROM amis 
								WHERE ami_from=:ami_from AND ami_confirm =:ami_confirm 
								OR ami_to=:ami_to AND ami_confirm =:ami_confirm
								ORDER BY RAND()')
								or die(print_r($bdd->errorInfo()));
			$r_compteur->execute(array('ami_from' => $donnees['id'],
								'ami_confirm' => 1,
								'ami_to' => $donnees['id'],))
								or die(print_r($bdd->errorInfo()));
			while ($d_compteur = $r_compteur->fetch())
			{
				$compteur_amis++;
			}
			
		echo'
		
		<div id="compteur_amis_autre">
			'.$compteur_amis.' <span style="font-size:small"> (amis) </span>
		</div>';
		
		
			$r_id = $bdd->prepare('SELECT ami_to, ami_from FROM amis 
								WHERE ami_from=:ami_from AND ami_confirm =:ami_confirm 
								OR ami_to=:ami_to AND ami_confirm =:ami_confirm
								ORDER BY RAND() LIMIT 0,10')
								or die(print_r($bdd->errorInfo()));
			$r_id->execute(array(
								// SESSION a fait la demande
								'ami_from' => $donnees['id'],
								'ami_confirm' => 1,
								// On a fait la demande à SESSION
								'ami_to' => $donnees['id'],))
								or die(print_r($bdd->errorInfo()));
			while ($d_id = $r_id->fetch())
			{
				// SESSION = AMI_TO L'INVITATION VIENT DE L'AMI
				if(isset($d_id['ami_from']) AND $d_id['ami_from'] != $donnees['id'])
				{
					$r_ami = $bdd->prepare('SELECT photo_profil, id, identifiant
											FROM membres 
											WHERE id=:id')
									or die(print_r($bdd->errorInfo()));
					$r_ami->execute(array('id' => $d_id['ami_from']))
									or die(print_r($bdd->errorInfo()));
					$d_ami = $r_ami->fetch();
				}
				// SESSION = AMI_TO L'INVITATION VIENT DE SESSION
				elseif(isset($d_id['ami_to']) AND $d_id['ami_to'] != $donnees['id'])
				{
					$r_ami = $bdd->prepare('SELECT photo_profil, id, identifiant
											FROM membres 
											WHERE id=:id')
									or die(print_r($bdd->errorInfo()));
					$r_ami->execute(array('id' => $d_id['ami_to']))
									or die(print_r($bdd->errorInfo()));
					$d_ami = $r_ami->fetch();
				}
				
				echo'
				<a href="'.urlencode(stripslashes(htmlspecialchars($d_ami['identifiant']))).'" title="'.stripslashes(htmlspecialchars($d_ami['identifiant'])).'">';
				
				if(isset($d_ami['photo_profil']) 
				AND $d_ami['photo_profil'] != ''
				AND $d_ami['photo_profil'] != 0)
				{  
					echo'
					<div class="centre_image30" style="margin-left:23px;margin-top:12px;float:left;">';
					
					$source = getimagesize('images_utilisateurs/'.$d_ami['photo_profil']); 	// La photo est la source
					if ($source[0] <= 30 AND $source[1] <= 30)
						echo '<img src="images_utilisateurs/'.$d_ami['photo_profil'].'" alt="Photo de profil" />';
					else
						echo '<img src="images_utilisateurs/mini_2_'.$d_ami['photo_profil'].'" alt="Photo de profil" />';
					
					echo'
					</div>';
				}
				else
					echo'<img style="margin-top:12px;display:block;float:left;margin-left:23px;" src="images/image_defaut.png" alt="Image"/>';
				
				echo'
				</a>';
			}
?>
		</div>
	</div>
<?php
}
	// REQUETE POUR SAVOIR SI EN ATTENTE OU PAS
	$r_attente = $bdd->prepare('SELECT ami_confirm FROM amis 
			WHERE ami_from=:ami_from1 AND ami_to=:ami_to1
			OR ami_from=:ami_from2 AND ami_to=:ami_to2')
			or die(print_r($bdd->errorInfo()));
	$r_attente->execute(array(
						// SESSION a fait la demande
						'ami_from1' => $_SESSION['id_membre'],
						'ami_to1' => $donnees['id'],
						// On a fait la demande à SESSION
						'ami_from2' => $donnees['id'],
						'ami_to2' => $_SESSION['id_membre'],))
						or die(print_r($bdd->errorInfo()));
	$d_attente = $r_attente->fetch();
	
// POUR BLOC VERT :  DEMANDE, EN ATTENTE, AMI ---------------------------------/
if(!isset($_SESSION['envoyer_message'])) // PAS DE CHANGEMENT DE CSS
{	
	if(isset($d_attente['ami_confirm']) AND $d_attente['ami_confirm'] == 0) 	// EN ATTENTE
	{
		echo'
		<div class="profil_bloc4" title="En Attente" id="bloc_vert1" ></div>';
	}
	elseif(isset($d_attente['ami_confirm']) AND $d_attente['ami_confirm'] == 1)	// AMI
	{
		echo'
		<div class="profil_bloc5" title="Ami" id="bloc_vert2" ></div>
		
		<a onclick ="var sup=confirm(\'Êtes vous sur de vouloir supprimer cet ami ?\');
		if (sup == 0)return false;"href="profil.php?id='.$donnees['id'].'&identifiant='.urlencode(stripslashes(htmlspecialchars($donnees['identifiant']))).'&supprimer">
		
			<div style="display:none" class="profil_bloc_supprimer" title="Supprimer de mes amis" id="bloc_supprimer"></div>
			
		</a>';
	}
	else
	{
		echo'
		<a href="ami_post.php?id_ami='.$donnees['id'].'&identifiant='.urlencode(stripslashes(htmlspecialchars($donnees['identifiant']))).'&ajouter">
			<div class="profil_bloc1" title="Ajouter en ami"  id="bloc_vert1"></div>
		</a>';
	}
}
else // CHANGEMENT DE CSS => MARGIN LEFT 22PX
{	
	if(isset($d_attente['ami_confirm']) AND $d_attente['ami_confirm'] == 0) 	// EN ATTENTE
	{
		echo'
		<div style="margin-left:22px;" class="profil_bloc4" title="En Attente" id="bloc_vert" ></div>';
	}
	elseif(isset($d_attente['ami_confirm']) AND $d_attente['ami_confirm'] == 1)	// AMI
	{
		echo'
		<div style="margin-left:22px;" class="profil_bloc5" title="Ami" id="bloc_vert"></div>';
	}
	else
	{
		echo'
		<a href="ami_post.php?id_ami='.$donnees['id'].'&identifiant='.urlencode(stripslashes(htmlspecialchars($donnees['identifiant']))).'&ajouter">
			<div style="margin-left:22px;" class="profil_bloc1" title="Ajouter en ami"  id="bloc_vert"></div>
		</a>';
	}
}
//-----------------------------------------------------------------------------/

if(!isset($_SESSION['envoyer_message']) OR !isset($_GET['erreur']))
{
?>
	<a href="#" id="envoyer_message">
		<div id="profil_bloc2" title="Envoyer un message"></div>
	</a>
	
	<div id="profil_bloc3" title="Défier"></div>
<?php
}

// LA SESSION SERT A LAISSER MESSAGE APPARENT SI IL Y A UNE ERREUR DANS LE MESSAGE
if(!isset($_SESSION['envoyer_message']))
{	
	// CHANGEMENT DE PLACE DES BLOCS MESSAGE ET DEFI AVEC JAVASCRIPT ///////////
	echo'
	<div id="profil_bloc6" title="Défier" style="display:none;"></div>
	
	<div id="profil_bloc7" title="Envoyer un message" style="display:none;"></div>
	
	<div id="profil_message" style="display:none;">';
}
else
{	
	echo'
	<div id="profil_bloc6" title="Défier" ></div>
	
	<div id="profil_bloc7" title="Envoyer un message" ></div>
	
	<div id="profil_message" >';
}
		echo'
		<img src="images\fleche_grise.png" alt="Flèche"/>
		<div>
			<p>
				Envoyer un message à '.stripslashes(htmlspecialchars($donnees['identifiant'])).'
			</p>
			
			<form action="message_post.php" method="post">';
			if(isset($_GET['erreur']))
				echo'
				<textarea rows="11" cols="36" name="message" placeholder=" Votre message ne peut pas être vide"></textarea>';
			else
				echo'
				<textarea rows="11" cols="36" name="message" placeholder="Écrivez votre message"></textarea>';
				
				echo'
				<br />
				<input type="hidden" name="message_to" value="'.$donnees['id'].'"/>
				<input type="hidden" name="identifiant_profil" value="'.stripslashes(htmlspecialchars($donnees['identifiant'])).'"/>
				<input type="submit"  name="envoyer" value="Envoyer"/>
			</form>
			
			
		</div>';
?>
	</div>
	
	<div  class="erreur"></div>
	