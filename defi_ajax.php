<?php
session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arrête tout
    die('Erreur : '.$e->getMessage());
}
if (isset($_GET['id'],$_GET['i']))
{
	$re = $bdd->prepare('SELECT * FROM defi 
						WHERE id=:id')
						or die(print_r($bdd->errorInfo()));
	$re->execute(array('id' => $_GET['id']))
						or die(print_r($bdd->errorInfo()));
	$donnees = $re->fetch();

	$temps_restant = (3600*48 - (time() - $donnees['debut_defi']));

	$json['h'] = floor($temps_restant / 3600);
	$json['m'] = floor(($temps_restant % 3600)/60) ;
	$json['s'] = floor((($temps_restant % 3600) % 60));
	$json['i'] = $_GET['i'];

	echo json_encode($json);
}