<?php

// ON TEST L'IMAGE
// SI L'IMAGE NE DEPASSE PAS 1 MO -> 1 000 000 d'octets
if (isset($_FILES['photo_profil']) AND $_FILES['photo_profil']['size']<=1000000  
AND isset($_FILES['photo_profil']['name']) 
AND !(empty($_FILES['photo_profil']['name'])) 
AND $_FILES['photo_profil']['error'] == 0)
{
	// TESTONS SI L'EXTENSION EST AUTORISEE
	$infosfichier = pathinfo($_FILES['photo_profil']['name']);
	$extension_upload = $infosfichier['extension'];
	$extensions_autorisees = array('jpg', 'jpeg', 'png', 'PNG', 'JPEG', 'JPG');	

	if (in_array($extension_upload, $extensions_autorisees))
	{
		// ouverture du fichier compteur
		$monfichier = fopen('compteur.txt', 'r+');				 
		$compteur = fgets($monfichier); 
		$compteur++; 
		fseek($monfichier, 0); 
		fputs($monfichier, $compteur); 
		fclose($monfichier);
		// On peut valider le fichier et le stocker définitivement
		if ($extension_upload == 'png' OR $extension_upload == 'PNG')
		{
			move_uploaded_file($_FILES['photo_profil']['tmp_name'], 'images_utilisateurs/' .$compteur.'.'.$extension_upload);
			$source = imagecreatefrompng('images_utilisateurs/'.$compteur.'.'.$extension_upload); // La photo est la source
		}
		elseif ($extension_upload == 'jpg' OR $extension_upload == 'jpeg' OR $extension_upload == 'JPG' OR $extension_upload == 'JPEG')
		{
			move_uploaded_file($_FILES['photo_profil']['tmp_name'], 'images_utilisateurs/' .$compteur.'.'.$extension_upload);
			$source = imagecreatefromjpeg('images_utilisateurs/'.$compteur.'.'.$extension_upload); // La photo est la source
		}

		include('redimension_image.php');
		$redimOK = fctredimimage(120,120,'images_utilisateurs/','mini_1_'.$compteur.'.'.$extension_upload,'images_utilisateurs/',$compteur.'.'.$extension_upload);
		$redimOK = fctredimimage(45,45,'images_utilisateurs/','mini_2_'.$compteur.'.'.$extension_upload,'images_utilisateurs/',$compteur.'.'.$extension_upload);
		$presence_photo = 'on';
	}
	else 
		$presence_photo = 'off';
	
}
else 
	$presence_photo = 'off';
	
	
if(isset($_FILES['photo_profil']) AND $_FILES['photo_profil']['name'] != '' 
AND $_FILES['photo_profil']['size']<=1000000)
{
	$monfichier = fopen('compteur.txt', 'r+');				 
	$compteur = fgets($monfichier); 
	fclose($monfichier);
}
else
$compteur = '';

if (isset($extension_upload))
{			
	$photo = $compteur.'.'.$extension_upload;
}
else // SERT QUAND AUCUNE IMAGE EST SELECTIONNEE QUAND IL N'Y A AUCUNE EXTENSION
{
	$photo = 0;		
}
?>