<?php

// OVERLAY FIN PARTIE ! ------------------------------------------------------//

if(isset($_GET['fin']) AND !isset($_GET['id_defi']))
{
	if (isset ($_SESSION['numero_partie']))
	{
		echo'
		<div id="fond_overlay" style="display:block;"></div>
		<div class="overlay_fin" id="overlay_fin">';

		if (isset($_SESSION['entrainement_esquive'])
		OR isset($_SESSION['tournoi_esquive']))
		{
			// ON CREER UNE VARIABLE $table POUR SAVOIR OU ON PREND LES INFOS
			 
			// ENTRAINEMENT
			if(isset($_SESSION['entrainement_esquive'])
			AND $_SESSION['entrainement_esquive'] == 'on' ) { $table= 'traitement_esquive'; }
			
			// TOURNOI
			elseif(isset($_SESSION['tournoi_esquive'])
			AND $_SESSION['tournoi_esquive'] == 'on') { $table= 'tournoi_esquive'; }
			
			$re_c4 = $bdd->prepare('SELECT * FROM '.$table.'
									WHERE numero_partie=:numero_partie 
									ORDER BY numero_partie DESC')
									or die(print_r($bdd->errorInfo()));
			$re_c4->execute(array('numero_partie' => $_SESSION['numero_partie']))
			or die(print_r($bdd->errorInfo()));
			$donnees_c4 = $re_c4->fetch();
			if (isset($donnees_c4['temps']))
			{
				echo' 

				Fin Partie
				<img class="main" style="top:13px;"src="images/bad.png" alt=" "/>				
				
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				
				Temps:&nbsp;
				<span>
				'.$donnees_c4['temps'].' s
				</span>
				<img class="main" style="top:5px;"src="images/good.png" alt=" "/>';
			}
		}
		// ON GERE POUR LETTRE
		elseif (isset ($_SESSION['entrainement_lettre'])
		OR isset ($_SESSION['tournoi_lettre']))
		{
			// ON CREER UNE VARIABLE $table POUR SAVOIR OU ON PREND LES INFOS
			 
			// ENTRAINEMENT
			if(isset($_SESSION['entrainement_lettre'])
			AND $_SESSION['entrainement_lettre'] == 'on' ) { $table= 'traitement_lettre'; }
			
			// TOURNOI
			elseif(isset($_SESSION['tournoi_lettre'])
			AND $_SESSION['tournoi_lettre'] == 'on') { $table= 'tournoi_lettre'; }	
			
			$re_c4 = $bdd->prepare('SELECT * FROM '.$table.'
									WHERE numero_partie=:numero_partie 
									ORDER BY numero_partie DESC')
									or die(print_r($bdd->errorInfo()));
			$re_c4->execute(array('numero_partie' => $_SESSION['numero_partie']))
			or die(print_r($bdd->errorInfo()));
			$donnees_c4 = $re_c4->fetch();
			if (isset($donnees_c4['temps'],$donnees_c4['score']))
			{
				echo' 

				Fin Partie
				<img class="main" style="top:13px;"src="images/bad.png" alt=" "/>
				
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				
				Score:&nbsp;
				<span>
				'.$donnees_c4['score'].'
				</span>
				
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				
				Temps:&nbsp;
				<span>
				'.$donnees_c4['temps'].' s
				</span>
				<img class="main" style="top:5px;"src="images/good.png" alt=" "/>';
			}
		}
		// ON GERE POUR CIBLE
		elseif (isset ($_SESSION['entrainement_cible'])
		OR isset ($_SESSION['tournoi_cible']))
		{
			// ON CREER UNE VARIABLE $table POUR SAVOIR OU ON PREND LES INFOS
			 
			// ENTRAINEMENT
			if(isset($_SESSION['entrainement_cible'])
			AND $_SESSION['entrainement_cible'] == 'on' ) { $table= 'traitement_cible'; }
			
			// TOURNOI
			elseif(isset($_SESSION['tournoi_cible'])
			AND $_SESSION['tournoi_cible'] == 'on') { $table= 'tournoi_cible'; }
			
			$re_c4 = $bdd->prepare('SELECT * FROM '.$table.' 
									WHERE numero_partie=:numero_partie 
									ORDER BY numero_partie DESC')
									or die(print_r($bdd->errorInfo()));
			$re_c4->execute(array('numero_partie' => $_SESSION['numero_partie']))
			or die(print_r($bdd->errorInfo()));
			$donnees_c4 = $re_c4->fetch();
			if (isset($donnees_c4['temps'],$donnees_c4['score']))
			{
				echo' 

				Fin Partie
				<img class="main" style="top:13px;"src="images/bad.png" alt=" "/>
				
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				
				Score:&nbsp;
				<span>
				'.$donnees_c4['score'].'
				</span>
				
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				
				Temps:&nbsp;
				<span>
				'.$donnees_c4['temps'].' s
				</span>
				<img class="main" style="top:5px;"src="images/good.png" alt=" "/>';
			}
		}

	}
		echo'
		</div>';

}
?>