<?php
include('header.php');
include("paypal.php");

unset($_SESSION['numero_partie']); 
unset($_SESSION['envoyer_message']); // SESSION POUR LE BLOC MESSAGE

//On doit le mettre dans toutes les pages accessibles depuis le header
// INDEX , JEUX , DEFI , COMPTE, MESSAGE, PROFIL, RECHERCHE (changer sur ces pages quand le tournoi change)
$_SESSION['tournoi_esquive'] = 'on';
$_SESSION['tournoi_lettre'] = 'off';
$_SESSION['tournoi_cible'] = 'off';

// traitement retour de la page paypal
if (isset($_GET['retour'],$_GET['token'],$_GET['PayerID'],$_GET['amt']))
{
	$requete = construit_url_paypal(); // Construit les options de base

	// On ajoute le reste des options
	// La fonction urlencode permet d'encoder au format URL les espaces, slash, deux points, etc.)
	$requete = $requete."&METHOD=DoExpressCheckoutPayment".
				"&TOKEN=".htmlentities($_GET['token'], ENT_QUOTES). // Ajoute le jeton qui nous a été renvoyé
				"&AMT=".htmlentities($_GET['amt'], ENT_QUOTES).
				"&CURRENCYCODE=EUR".
				"&PayerID=".htmlentities($_GET['PayerID'], ENT_QUOTES). // Ajoute l'identifiant du paiement
				"&PAYMENTACTION=sale";

	// Initialise notre session cURL. On lui donne la requête à exécuter.
	$ch = curl_init($requete);

	// Modifie l'option CURLOPT_SSL_VERIFYPEER afin d'ignorer la vérification du 
	//certificat SSL. Si cette option est à 1, une erreur affichera que la vérification 
	//du certificat SSL a échoué, et rien ne sera retourné. 
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	// Retourne directement le transfert sous forme de chaîne de la valeur retournée 
	//par curl_exec() au lieu de l'afficher directement. 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	// On lance l'exécution de la requête URL et on récupère le résultat dans une variable
	$resultat_paypal = curl_exec($ch);

	if (!$resultat_paypal) // S'il y a une erreur, on affiche "Erreur", suivi du détail de l'erreur.
		{echo "<p>Erreur</p><p>".curl_error($ch)."</p>";}
	// S'il s'est exécuté correctement, on effectue les traitements...
	else
	{
		// Lance notre fonction qui dispatche le résultat obtenu en un array
		$liste_param_paypal = recup_param_paypal($resultat_paypal); 
		
		// Si la requête a été traitée avec succès
		if ($liste_param_paypal['ACK'] == 'Success')
		{	
			
			// Mise à jour de la base de données
			$req1bis = $bdd->prepare('SELECT kp FROM membres WHERE identifiant=:identifiant')
			or die(print_r($bdd->errorInfo()));
			$req1bis->execute(array('identifiant' => $_SESSION['identifiant']))
			or die(print_r($bdd->errorInfo()));
			$donnees1bis= $req1bis->fetch();
			if (!empty($donnees1bis['kp']))
			{
				// on ajoute le(s) kap(s) qu'on vient d'acheter
				$kap_possede = $donnees1bis['kp'] + $_SESSION['kp_achete'];
								
				$req = $bdd->prepare('UPDATE membres SET kp=:kp
									  WHERE identifiant=:identifiant');
				$req->execute(array('kp' => $kap_possede,
									'identifiant' => $_SESSION['identifiant']))
									or die(print_r($bdd->errorInfo()));	
				$req->closeCursor(); // Termine le traitement de la requête	
				
				header('Location: compte.php?achat_reussi');
			}
		}
		else // En cas d'échec, affiche la première erreur trouvée.
		{echo "<p>Erreur de communication avec le serveur PayPal.<br />";}
		//".$liste_param_paypal['L_SHORTMESSAGE0']."<br />".$liste_param_paypal['L_LONGMESSAGE0']."</p>";}
	}
	// On ferme notre session cURL.
	curl_close($ch);
}

?>
<div id="wrap">
<div class="corps">
<?php
if(isset($_SESSION['identifiant']))
{
	if(isset($_GET['erreur']) AND $_GET['erreur'] != 'done')
	{
		echo'
		<div id="fond_overlay" style="display:block;"></div>
		<div id="overlay_rouge" style="display:block;">';
			if (isset($_GET['erreur']) AND $_GET['erreur'] == 'incomplet')
			{
				echo'
				Veuillez compléter entièrement le formulaire';
			}
			elseif (isset($_GET['erreur']) AND $_GET['erreur'] == 'date')
			{
				echo'
				Format de la date de naissance: jj/mm/aaaa';
			}
			elseif (isset($_GET['erreur']) AND $_GET['erreur'] == 'code')
			{
				echo'
				Le code postal est composé de 5 chiffres';
			}
			elseif (isset($_GET['erreur']) AND $_GET['erreur'] == 'reponse')
			{
				echo'
				La réponse secrète est incorrecte';
			}
			elseif (isset($_GET['erreur']) AND $_GET['erreur'] == 'actuel')
			{
				echo'
				Le mot de passe actuel est invalide';
			}
			elseif (isset($_GET['erreur']) AND $_GET['erreur'] == 'incorrect')
			{
				echo'
				Le mot de passe n\'est pas conforme (caractères (4-25) spéciaux acceptés: é & è @ à ç $ * ù)';
			}
			elseif (isset($_GET['erreur']) AND $_GET['erreur'] == 'fail')
			{
				echo'
				Les deux mots de passes ne sont pas identiques';
			}
			elseif (isset($_GET['erreur']) AND $_GET['erreur'] == 'actuelle')
			{	
				echo'
				L\'adresse email actuelle est invalide';
			}
			elseif (isset($_GET['erreur']) AND $_GET['erreur'] == 'email')
			{
				echo'
				L\'adresse e-mail n\'est pas conforme. Utilisez ce type d\'adresse: 
				azerty-123@.....';
			}
				elseif (isset($_GET['erreur']) AND $_GET['erreur'] == 'error')
			{
				echo'
				Les deux adresses email ne sont pas identiques';
			}
		echo'
		</div>';
	}
	elseif(isset($_GET['erreur']) AND $_GET['erreur'] == 'done')
	{
		echo'
		<div id="fond_overlay" style="display:block;"></div>
		<div id="overlay_vert" style="display:block;">
			Les données sont enregistrées
		</div>';
	
	}
?>		
		<div class="bloc_compte_gauche" id="bloc_compte_gauche">
			<img id="cadi_gris" class="cadi" src="images/cadi_gris.png" alt="Boutique"/>		
			<div class="bloc_kp">
			
				<a href="paypal.php?prix=0.20&kp=1"><div class="kp_choice1"> 
					<p class="p_kp">
						1 Kp
					</p>
					
					<p class="p_argent">
						0.20€
					</p>
				</div></a>
				
				<a href="paypal.php?prix=2.0&kp=10"><div class="kp_choice2">
					<p class="p_kp">
						10 Kp
					</p>
					
					<p class="p_argent">
						2€
					</p>
				</div></a>
				
				<a href="paypal.php?prix=10.0&kp=53"><div class="kp_choice3"> 
					<p class="p_kp">
						50 Kp
					</p>
					
					<p class="p_argent">
						10€
					</p>
					<p class="p_ajout">
						+ 3 Kp
					</p>
				</div></a>
				
				<a href="paypal.php?prix=20.0&kp=110"><div class="kp_choice4"> 
					<p class="p_kp">
						100 Kp
					</p>
					
					<p class="p_argent">
						20€
					</p>
					<p class="p_ajout">
						+ 10 Kp
					</p>
				</div></a>
				
				
			</div>
			

			<p id="annonce_change">
				Changer vos bénéfices en Kp
				<span style="font-size:small">
					&nbsp;&nbsp;&nbsp;&nbsp;(0.17€ / Kp)
				</span>
			</p>

			<img id="change_gris" class="carte" src="images/change_gris.png" alt="Echanger"/>
			
			<div class="bloc_argent">
				<form action="compte_post.php" method="post">
					<p>
<?php
					if(isset($_SESSION['identifiant']))
					{
						$req1 = $bdd->prepare('SELECT argent FROM membres 
											WHERE identifiant=:identifiant')
											or die(print_r($bdd->errorInfo()));
						$req1->execute(array('identifiant' => $_SESSION['identifiant']))
											or die(print_r($bdd->errorInfo()));
						$donnees1= $req1->fetch();
					}
					
					echo '<span style="color:grey">Vous gains : '.$donnees1['argent'].'€ </span><br />';
					
					if(isset($donnees1['argent']) AND $donnees1['argent'] < 0.17)
						echo'
						<input disabled="true" min="0" type="number" style="width:206px;" 
						name="kap_a_acheter" id="change" placeholder=" Nombre de Kp à acheter" />
						<input type="submit" id="soumettre" name="soumettre" value="Soumettre" class="soumettre2"/>';
					else
						echo'
						<input onKeyPress="return chiffres(event);" type="number" min="0" 
						style="width:206px;" name="kp_a_acheter" id="change" placeholder=" Nombre de Kp à acheter" />
						<input style="width:206px;" type="text" name="reponse" id="réponse1" placeholder=" Réponse secrète" />
						<input type="submit" id="soumettre" name="soumettre" value="Soumettre" class="soumettre"/>';
?>
					</p>
				</form>
				
			</div>
<?php

		if(isset($_SESSION['identifiant']))
		{
			$req = $bdd->prepare('SELECT argent FROM membres 
								WHERE identifiant=:identifiant')
								or die(print_r($bdd->errorInfo()));
			$req->execute(array('identifiant' => $_SESSION['identifiant']))
								or die(print_r($bdd->errorInfo()));
			$donnees= $req->fetch();
		}
				
			echo'
			<p id="annonce_argent">
				Vos gains : '.$donnees['argent'].'&nbsp;€ 
				<span style="font-size:small">(minimum pour effectuer un virement : 10€)</span>
			</p>';
?>
			<img id="carte_grise" class="carte" src="images/carte_grise.png" alt="Boutique"/>
			
			<div class="bloc_argent">
			
				
				<form action="compte_post.php" method="post">
					<p style="margin-top:5px;">
<?php
					if(isset($donnees['argent']) AND $donnees['argent'] < 10)
						echo'
						<input disabled="true" style="width:206px;" type="text" name="paypal" id="paypal" placeholder=" Adresse paypal" />
						<input disabled="true" type="submit" value="Virement" class="virement2"/>';
					else
						echo' 
						<input style="width:206px;" type="text" name="adresse_paypal" id="paypal" placeholder="Adresse paypal" />
						<input style="width:206px;" type="text" name="reponse1" id="réponse1" placeholder=" Réponse secrète" />
						<input type="hidden" name="argent" value="'.$donnees['argent'].'" /> 
						<input type="submit" id="virement" name="virement" value="Virement" class="virement" />';
?>
					</p>
				</form>
				
			</div>
			
		</div>
		
		
		<div class="bloc_compte_droite" id="bloc_compte_droite">
			<form action="compte_post.php" method="post">
		
			<img class="reglage" src="images/reglage_gris.png" alt="Mon compte"/>
<?php
		
		$requete = $bdd->prepare('SELECT email,reponse,nom,prenom,naissance,
								sexe,pays,ville,adresse,code FROM membres 
								WHERE identifiant = :identifiant')
								or die(print_r($bdd->errorInfo()));
		$requete->execute(array('identifiant' => $_SESSION['identifiant']))
								or die(print_r($bdd->errorInfo()));
		$donnees = $requete->fetch();

		if(isset($donnees['nom'],$donnees['prenom'],$donnees['sexe'],
		$donnees['naissance'],$donnees['pays'],$donnees['ville'],
		$donnees['code'],$donnees['adresse'])
		AND $donnees['nom'] != '' AND $donnees['prenom'] != '' 
		AND $donnees['sexe'] != '' AND $donnees['naissance'] != ''
		AND $donnees['pays'] != '' AND $donnees['ville'] != '' 
		AND $donnees['code'] != '' AND $donnees['adresse'] != '')
		{
			echo'
			<div class="bloc_info">
				<input autocomplete="off" style="width:110px;"type="text" disabled="true" value="'.stripslashes(htmlspecialchars($donnees['nom'])).'" />
				<input autocomplete="off" style="width:110px;"type="text" disabled="true" value="'.stripslashes(htmlspecialchars($donnees['prenom'])).'" />
				<input style="width:110px;"type="text" disabled="true" value="'.stripslashes(htmlspecialchars($donnees['sexe'])).'" />
				<input autocomplete="off" style="width:110px;"type="text" disabled="true" value="'.stripslashes(htmlspecialchars($donnees['naissance'])).'" />
				<input autocomplete="off" style="width:110px;"type="text" name="pays" id="pays" value="'.stripslashes(htmlspecialchars($donnees['pays'])).'" />
				<input autocomplete="off" style="width:110px;"type="text" name="ville" id="ville" value="'.stripslashes(htmlspecialchars($donnees['ville'])).'" />
				<input autocomplete="off" style="width:110px;"type="text" name="code" id="code" value="'.stripslashes(htmlspecialchars($donnees['code'])).'" />
				<input autocomplete="off" style="width:110px;"type="text" name="adresse" id="adresse" value="'.stripslashes(htmlspecialchars($donnees['adresse'])).'" />
				
				<input autocomplete="off" style="width:110px;" type="hidden" name="nom" id="nom" value="'.stripslashes(htmlspecialchars($donnees['nom'])).'" />
				<input autocomplete="off" style="width:110px;" type="hidden" name="prenom" id="prenom" value="'.stripslashes(htmlspecialchars($donnees['prenom'])).'" />
				<input autocomplete="off" style="width:110px;" type="hidden" name="sexe" id="sexe" value="'.stripslashes(htmlspecialchars($donnees['sexe'])).'" />
				<input autocomplete="off" style="width:110px;" type="hidden" name="naissance" id="naissance" value="'.stripslashes(htmlspecialchars($donnees['naissance'])).'" />
			</div>';
		}
		else
		{
			echo'
			<div class="bloc_info">
				<input autocomplete="off" style="width:110px;"type="text" name="nom" id="nom" placeholder=" Nom" />
				<input autocomplete="off" style="width:110px;"type="text" name="prenom" id="prenom" placeholder=" Prénom" />
				<select name="sexe" id="sexe" style="width:113px;" >
					<option value="sexe"> Sexe </option>
					<option value="Homme"> Homme </option>
					<option value="Femme"> Femme </option>
				</select>
				<input autocomplete="off" style="width:110px;"type="text" name="naissance" id="naissance" placeholder=" jj/mm/aaaa" />
				<input autocomplete="off" style="width:110px;"type="text" name="pays" id="pays" placeholder=" Pays" />
				<input autocomplete="off" style="width:110px;"type="text" name="ville" id="ville" placeholder=" Ville" />
				<input autocomplete="off" style="width:110px;"type="text" name="code" id="code" placeholder=" Code postal" />
				<input autocomplete="off" style="width:110px;"type="text" name="adresse" id="adresse" placeholder=" Adresse" />
			</div>';
		}
?>	
			<div class="bloc_modifier">
				<div>
					<img style="position:relative;top:5px;" src="images/clef_grise.png" alt="Mot de passe"/>
					&nbsp;&nbsp;<span id="mot_de_passe">Mot de passe</span>
					<a href="#" id="modifier_password">
						<span style="margin-left:199px;" class="modifier" id="modifier1"> 
							Modifier 
						</span>
					</a>
<?php
				if(isset($_SESSION['formulaire_password']))
					echo'
					<div class="formulaire_password" id="formulaire_password">';
				else
					echo'
					<div class="formulaire_password" id="formulaire_password" style="display:none;" >';
?>
						<input style="width:225px;"type="text" name="actuel" id="actuel" placeholder=" Actuel" onFocus="this.value='';this.type='password'"/>
						<input style="width:225px;"type="text" name="password1" id="password1" placeholder=" Nouveau" onFocus="this.value='';this.type='password'" />
						<input style="width:225px;"type="text" name="password2" id="password2" placeholder=" Saisir à nouveau" onFocus="this.value='';this.type='password'"/>
						<input style="width:225px;"type="text" name="reponse1" id="reponse1" placeholder=" Réponse secrète" autocomplete="off"/>
					</div>
				</div>
				<div style="margin-top:26px;">
					<img style="position:relative;top:7px;" src="images/email_gris.png" alt="Email"/>  
					&nbsp;&nbsp;<span id="adresse_email">Adresse email</span>
					<a href="#" id="modifier_email">
						<span class="modifier" id="modifier2"> 
							Modifier 
						</span>
					</a>
<?php
				if(isset($_SESSION['formulaire_email']))
					echo'
					<div class="formulaire_email" id="formulaire_email"" >';
				else
					echo'
					<div class="formulaire_email" id="formulaire_email" style="display:none;" >';
?>
						<input autocomplete="off" style="width:225px;"type="text" name="actuelle" id="actuelle" placeholder=" Actuelle" />
						<input autocomplete="off" style="width:225px;"type="text" name="email1" id="email1" placeholder=" Nouvelle" />
						<input autocomplete="off" style="width:225px;"type="text" name="email2" id="email2" placeholder=" Saisir à nouveau" />
						<input autocomplete="off" style="width:225px;"type="text" name="reponse2" id="reponse2" placeholder=" Réponse secrète" />
					</div>
				</div>
				<input type="submit" id="enregistrer" name="enregistrer" value="Enregistrer" class="enregistrer"/>
			</div>
			</form>
		</div>
	
	<div style="margin-top:660px;" class="erreur"></div>
<?php
}
else
	header('accueil');

?>
</div>
</div>
<?php
	
include('footer.php');
?>

	<script>
		// ON GERE OVERLAY !	
		if (document.getElementById('overlay_rouge'))
		{
			setTimeout(function() {
				document.getElementById('overlay_rouge').style.display="none";
				document.getElementById('fond_overlay').style.display="none";
			}, 2000);
		}
		// ON GERE OVERLAY !	
		if (document.getElementById('overlay_vert'))
		{
			setTimeout(function() {
				document.getElementById('overlay_vert').style.display="none";
				document.getElementById('fond_overlay').style.display="none";
			}, 2000);
		}
		document.getElementById('modifier_password').onclick = function() {
			formulaire_password.style.display = 'block';
			return false; // on bloque la redirection
			};
				
		document.getElementById('modifier_email').onclick = function() {
			formulaire_email.style.display = 'block';
			return false; // on bloque la redirection
			};
			
		// FONCTION POUR BLOQUER LES LETTRES. JUSTE LES CHIFFRES POSSIBLES
		function chiffres(event) {
			// Compatibilité IE / Firefox
			if(!event&&window.event) {
				event=window.event;
			}
			// IE
			if((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 37 || event.keyCode > 40) && event.keyCode != 46 && event.keyCode != 8 && event.keyCode != 16) {
				event.returnValue = false;
				event.cancelBubble = true;
			}
			// DOM
			if((event.which < 48 || event.which > 57) && (event.keyCode < 37 || event.keyCode > 40) && event.keyCode != 46 && event.keyCode != 8 && event.keyCode != 16) {
				event.preventDefault();
				event.stopPropagation();
			}
		}
		
		bloc_compte_gauche.addEventListener('mouseover', function () 
		{
			bloc_compte_gauche.style.backgroundColor = '#dedede';
			bloc_compte_gauche.style.borderLeft= '2px solid #bfa13a';
		},false);
		
		bloc_compte_gauche.addEventListener('mouseout', function () 
		{
			bloc_compte_gauche.style.backgroundColor = '#e8e8e8';
			bloc_compte_gauche.style.borderLeft= '2px solid #f7d354';
		},false);
		
		bloc_compte_droite.addEventListener('mouseover', function () 
		{
			bloc_compte_droite.style.backgroundColor = '#dedede';
			bloc_compte_droite.style.borderRight= '2px solid #bfa13a';
		},false);
		
		bloc_compte_droite.addEventListener('mouseout', function () 
		{
			bloc_compte_droite.style.backgroundColor = '#e8e8e8';
			bloc_compte_droite.style.borderRight= '2px solid #f7d354';
		},false);
	
	</script>

</body>

</html>
