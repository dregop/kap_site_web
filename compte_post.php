<?php
session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arrête tout
    die('Erreur : '.$e->getMessage());
}

$requete = $bdd->prepare('SELECT password,email,reponse,nom,prenom,naissance,
						sexe,pays,ville,adresse,code FROM membres 
						WHERE identifiant = :identifiant')
						or die(print_r($bdd->errorInfo()));
$requete->execute(array('identifiant' => $_SESSION['identifiant']))
						or die(print_r($bdd->errorInfo()));
$donnees = $requete->fetch();

// création réponse secrète
if(isset($_POST['question'],$_POST['reponse']) AND $_POST['question'] != 'question_secrete' 
AND $_POST['reponse'] != '')
{
	$req = $bdd->prepare('UPDATE membres SET question=:question,
						reponse=:reponse WHERE identifiant=:identifiant');
	$req->execute(array('question' => $_POST['question'],
						'reponse' => $_POST['reponse'],
						'identifiant' => $_SESSION['identifiant']))
						or die(print_r($bdd->errorInfo()));	
	$req->closeCursor(); // Termine le traitement de la requête	
	
	header('Location: accueil');
}
elseif(isset($_POST['question'],$_POST['reponse']) AND ($_POST['question'] == 'question_secrete' 
OR $_POST['reponse'] == ''))
{
	header('Location: accueil');
}
// MODIFICATION DU MOT DE PASSE
elseif (isset($_POST['reponse1'], $_POST['actuel'], $_POST['password1'], 
$_POST['password2']) AND ($_POST['actuel'] != '' OR $_POST['password1'] != ''
OR $_POST['password2'] != '' OR $_POST['reponse1'] != ''))
{
	$_SESSION['formulaire_password'] = 'on';
	if (isset($_POST['reponse1'], $_POST['actuel'], $_POST['password1'], 
	$_POST['password2']) AND $_POST['actuel'] != '' AND $_POST['password1'] != ''
	AND $_POST['password2'] != '' AND $_POST['reponse1'] != '')
	{
		if($_POST['reponse1'] == $donnees['reponse'])
		{
		
			$mdp_hache = sha1('qw' . $_POST['actuel']); // on hache le mdp	
			if ($mdp_hache == $donnees['password'])
			{
				if (!(preg_match('#^[A-Za-z0-9é&è@àç$*ù]{4,25}$#',$_POST['password1'])))
				{
					if($_POST['password1'] == $_POST['password2'])
					{
						$mdp_hache2 = sha1('qw' . $_POST['password1']); 
						
						$req = $bdd->prepare('UPDATE membres SET password=:password
											WHERE identifiant = :identifiant')
											or die(print_r($bdd->errorInfo()));
						$req->execute(array('password' => $mdp_hache2,
											'identifiant' => $_SESSION['identifiant']))
											or die(print_r($bdd->errorInfo()));	
						$req->closeCursor(); // Termine le traitement de la requête
						header('Location: compte-done');
						unset($_SESSION['formulaire_password']);
					}
					else header('Location: compte-fail'); // 2 password pas identiques
				}
				else header('Location: compte-incorrect'); // password forme incorrecte
			}
			else header('Location: compte-actuel'); // Ancien password différent
		}
		else header('Location: compte-reponse'); // Mauvaise réponse secrète
	}
	else header('Location: compte-incomplet'); // formulaire incomplet
}
// MODIFICATION DE L'ADRESSE EMAIL
elseif (isset($_POST['reponse2'], $_POST['actuelle'], $_POST['email1'], 
$_POST['email2']) AND ($_POST['actuelle'] != '' OR $_POST['email1'] != ''
OR $_POST['email2'] != '' OR $_POST['reponse2'] != ''))
{
	$_SESSION['formulaire_email'] = 'on';
	if (isset($_POST['reponse2'], $_POST['actuelle'], $_POST['email1'], 
	$_POST['email2']) AND $_POST['actuelle'] != '' AND $_POST['email1'] != ''
	AND $_POST['email2'] != '' AND $_POST['reponse2'] != '')
	{
		if($_POST['reponse2'] == $donnees['reponse'])
		{
			if ($_POST['actuelle'] == $donnees['email'])
			{
				if(preg_match('#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#i',$_POST['email1']))
				{
					if($_POST['email1'] == $_POST['email2'])
					{
						$req = $bdd->prepare('UPDATE membres SET email=:email
											WHERE identifiant = :identifiant')
											or die(print_r($bdd->errorInfo()));
						$req->execute(array('email' => $_POST['email1'],
											'identifiant' => $_SESSION['identifiant']))
											or die(print_r($bdd->errorInfo()));	
						$req->closeCursor(); // Termine le traitement de la requête
						unset($_SESSION['formulaire_email']);
						header('Location: compte-done');
					}
					else header('Location: compte-error'); // 2 password pas identiques
				}
				else header('Location: compte-email'); // email forme incorrecte
			}
			else header('Location: compte-actuelle-'.urlencode(stripslashes(htmlspecialchars($_POST['email1']))).''); // Ancienne adresse email diff
		}
		else header('Location: compte-reponse'); // Mauvaise réponse secrète
	}
	else header('Location: compte-incomplet'); // formulaire incomplet
}
// formulaire informations complémentaires sur le compte
elseif(isset($_POST['nom'],$_POST['prenom'],$_POST['sexe'],$_POST['naissance'],
$_POST['pays'],$_POST['ville'],$_POST['code'],$_POST['adresse'])
AND $_POST['nom'] != '' AND $_POST['prenom'] != '' 
AND $_POST['sexe'] != 'sexe' AND $_POST['naissance'] != ''
AND $_POST['pays'] != '' AND $_POST['ville'] != '' 
AND $_POST['code'] != '' AND $_POST['adresse'] != '')
{
	if (!(preg_match('#^[0-9]{2}/[0-9]{2}/[0-9]{4}$#',$_POST['naissance']))) 	// si la date ne correspond pas à ce format
	{
		$erreur_date = 'date';
		header('Location: compte-'.$erreur_date.'');
	}
	elseif (!(preg_match('#^[0-9]{5}$#',$_POST['code']))) 	// si la date ne correspond pas à ce format
	{
		$erreur_code = 'code';
		header('Location: compte-'.$erreur_code.'');
	}
	else
	{	
		$req = $bdd->prepare('UPDATE membres SET nom=:nom, prenom=:prenom, 
							sexe=:sexe, naissance=:naissance, pays=:pays, 
							ville=:ville, code=:code, adresse=:adresse 
							WHERE identifiant=:identifiant');
		$req->execute(array('nom' => $_POST['nom'],
							'prenom' => $_POST['prenom'],
							'sexe' => $_POST['sexe'],
							'naissance' => $_POST['naissance'],
							'pays' => $_POST['pays'],
							'ville' => $_POST['ville'],
							'code' => $_POST['code'],
							'adresse' => $_POST['adresse'],
							'identifiant' => $_SESSION['identifiant']))
							or die(print_r($bdd->errorInfo()));	
		$req->closeCursor(); // Termine le traitement de la requête	
		unset($_SESSION['formulaire_password']);
		unset($_SESSION['formulaire_email']);
		header('Location: compte-done');
	}
}
// CHANGER SES GAINS EN KP
elseif(isset($_POST['kp_a_acheter'],$_POST['reponse']))
{
	$req1 = $bdd->prepare('SELECT kp,reponse,argent FROM membres 
							WHERE identifiant=:identifiant')
							or die(print_r($bdd->errorInfo()));
	$req1->execute(array('identifiant' => $_SESSION['identifiant']))
							or die(print_r($bdd->errorInfo()));
	$donnees1= $req1->fetch();	
	
	if($_POST['reponse'] == $donnees1['reponse'] AND $_POST['kp_a_acheter'] <= ($donnees1['argent']/0.17))
	{
		$kp = $donnees1['kp'] + $_POST['kp_a_acheter'];
		$argent = $donnees1['argent'] - ($_POST['kp_a_acheter']*0.17);
		$req2 = $bdd->prepare('UPDATE membres SET kp=:kp, argent=:argent
								WHERE identifiant=:identifiant');
		$req2->execute(array('kp' => $kp,
							 'argent' => $argent,
							'identifiant' => $_SESSION['identifiant']))
							or die(print_r($bdd->errorInfo()));	
		$req2->closeCursor();
		
		header('Location: compte');
	}
	else
		header('Location: compte.php?donnees_incorrectes'); 
}
// RECUPERER SON ARGENT AVEC PAYPAL
elseif (isset($_POST['adresse_paypal'],$_POST['reponse1'],$_POST['argent']))
{
	$req = $bdd->prepare('SELECT payement_effectue FROM paypal 
							WHERE identifiant=:identifiant ORDER BY date DESC')
							or die(print_r($bdd->errorInfo()));
	$req->execute(array('identifiant' => $_SESSION['identifiant']))
							or die(print_r($bdd->errorInfo()));
	$donnees= $req->fetch();	
	
	if (!isset($donnees['payement_effectue']) OR (isset($donnees['payement_effectue']) 
		AND $donnees['payement_effectue'] != ''))
	{
		$req1 = $bdd->prepare('SELECT reponse FROM membres 
								WHERE identifiant=:identifiant')
								or die(print_r($bdd->errorInfo()));
		$req1->execute(array('identifiant' => $_SESSION['identifiant']))
								or die(print_r($bdd->errorInfo()));
		$donnees1= $req1->fetch();	
		// si adresse paypal diff demander de valider par email son identité
		if ($_POST['reponse1'] == $donnees1['reponse'])
		{
			$r1 = $bdd->prepare('INSERT INTO paypal (identifiant, somme_a_verser, 
								adresse_paypal, date) 
								VALUES(:membre, :argent, :paypal ,NOW())');
			$r1->execute(array('membre' => $_SESSION['identifiant'],
							   'argent' => $_POST['argent'],
							   'paypal' => $_POST['adresse_paypal']));
			$r1->closeCursor(); 
			
			header('Location: compte.php?recuperation_en_cours');
		}
		else
			header('Location: compte.php?reponse_incorrecte');
	}
	else
		header('Location: compte.php?une_demande_est_deja_en_cours');
}
else header('Location: compte-incomplet'); // formulaire incomplet