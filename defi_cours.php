<?php
	// POUR INCLUDE -> AFFICHAGE NORMAL - AFFICHAGE ALL	
		
	foreach($tableau_cours as $element1)
	{	
		$total_affichage= $element1['somme']*2; // LE TOTAL DU DEFI EN KP CE QU'IL PEUT GAGNER
		
		// ON A DEUX IDENTIFIANT: LA SESSION et L'AUTRE
		// ON PREND UNE FOI POUR TOUTE L'IDENTIFIANT DIFFERENT DE SESSION
		if($element1['identifiant_1'] != $_SESSION['identifiant']) 		
		{
			$r_donnees1 = $bdd->prepare('SELECT photo_profil, identifiant FROM membres 
									WHERE identifiant = :identifiant')
									or die(print_r($bdd->errorInfo()));
			$r_donnees1->execute(array('identifiant' => $element1['identifiant_1'])) 
					or die(print_r($bdd->errorInfo()));
			$donnees_from1 = $r_donnees1->fetch();
			$identifiant = 2;
		}
		elseif($element1['identifiant_2'] != $_SESSION['identifiant']) 		
		{
			$r_donnees1 = $bdd->prepare('SELECT photo_profil, identifiant FROM membres 
									WHERE identifiant = :identifiant')
									or die(print_r($bdd->errorInfo()));
			$r_donnees1->execute(array('identifiant' => $element1['identifiant_2'])) 
					or die(print_r($bdd->errorInfo()));
			$donnees_from1 = $r_donnees1->fetch();
			$identifiant = 1;
		}
		$temps_restant = (3600*48 - (time() - $element1['debut_defi']));
		if ($temps_restant <= 0 AND $element1['view'] != 'all' AND
			$element1['view'] != $_SESSION['identifiant'])
		{
			// ON RECUPERE LE MEILLEUR SCORE DU DEFI ET ON INSERT LE GAGNANT--//
			// UN IF POUR TEMPS (ESQUIVE) UN AUTRE POUR SCORE (LETTRE - CIBLE)//
			if($element1['jeu'] == 'esquive')
			{
				$meilleur_score1 = 0;
				$r_meilleur1 = $bdd->prepare('SELECT identifiant,temps 
										FROM traitement_defi 
										WHERE id_defi=:id
										ORDER BY temps DESC')
										or die(print_r($bdd->errorInfo()));
				$r_meilleur1->execute(array('id' => $element1['id']))
										or die(print_r($bdd->errorInfo()));
				while($d_meilleur1 = $r_meilleur1->fetch())
				{
					$meilleur_score1++;
					if($meilleur_score1 ==1)
					{
						break;
					}
				}
				// GRACE A LA BOUCLE ON SAIT SI ID1 ou ID2 à GAGNE ------------/
				// ON LE STOCK DANS LA VARIABLE $gagnant POUR LE REUTILISER ---/
				
				if(isset($d_meilleur1['identifiant']) AND $meilleur_score1 != 0 
				AND $d_meilleur1['identifiant'] == 'id1')
				{
					$gagnant = 'id1';
				}
				elseif(isset($d_meilleur1['identifiant']) AND $meilleur_score1 != 0 
				AND $d_meilleur1['identifiant'] == 'id2')
				{
					$gagnant = 'id2';				
				}
				else
				{
					$gagnant = 'aucun';
				}
			}
			// LE ELSE POUR LETTRE ET CIBLE, EN FONCTION DU SCORE ET NON DU TEMPS
			else
			{
				$meilleur_score2 = 0;
				$r_meilleur2 = $bdd->prepare('SELECT identifiant,score FROM traitement_defi 
										WHERE id_defi=:id
										ORDER BY score DESC')
										or die(print_r($bdd->errorInfo()));
				$r_meilleur2->execute(array('id' => $element1['id']))
										or die(print_r($bdd->errorInfo()));
				while($d_meilleur2 = $r_meilleur2->fetch())
				{
					$meilleur_score2++;
					if($meilleur_score2 ==1)
					{
						break;
					}
				}

				// GRACE A LA BOUCLE ON SAIT SI ID1 ou ID2 à GAGNE ------------/
				// ON LE STOCK DANS LA VARIABLE $gagnant POUR LE REUTILISER ---/
				// ON VERIFIE BIEN QUE $meilleur_score =! 0
				if(isset($d_meilleur2['identifiant']) AND $meilleur_score2 != 0 
				AND $d_meilleur2['identifiant'] == 'id1')
				{
					$gagnant = 'id1';
				}
				elseif(isset($d_meilleur2['identifiant']) AND $meilleur_score2 != 0
				AND $d_meilleur2['identifiant'] == 'id2')
				{
					$gagnant = 'id2';
				}
				else
				{
					$gagnant = 'aucun';
				}
			}

			// ON ENREGISTRE GAGNANT DANS LA BDD
			if($element1['gagnant'] == '') // IL FAUT QUE LE CHAMPS SOIT VIDE
			{
				if(isset($gagnant) AND $gagnant == 'id1')
				{
					$r_gagnant1= $bdd->prepare('UPDATE defi SET gagnant=:gagnant WHERE id=:id')
											or die(print_r($bdd->errorInfo()));
					$r_gagnant1->execute(array('gagnant' => $element1['identifiant_1'],
											'id' => $element1['id']))
											or die(print_r($bdd->errorInfo()));	
					$r_gagnant1->closeCursor(); // Termine le traitement de la requête 
				}
				elseif(isset($gagnant) AND $gagnant == 'id2')
				{
					$r_gagnant2= $bdd->prepare('UPDATE defi SET gagnant=:gagnant WHERE id=:id')
											or die(print_r($bdd->errorInfo()));
					$r_gagnant2->execute(array('gagnant' => $element1['identifiant_2'],
											'id' => $element1['id']))
											or die(print_r($bdd->errorInfo()));	
					$r_gagnant2->closeCursor(); // Termine le traitement de la requête 
				}
				else // ON VA INDIQUER QU'AUCUN N'A GAGNE $gagnant = aucun
				{
					$r_aucun= $bdd->prepare('UPDATE defi SET gagnant=:gagnant WHERE id=:id')
											or die(print_r($bdd->errorInfo()));
					$r_aucun->execute(array('gagnant' => $gagnant,
											'id' => $element1['id']))
											or die(print_r($bdd->errorInfo()));	
					$r_aucun->closeCursor(); // Termine le traitement de la requête 				
				}
			}
			
			// LE GAGNANT VIENT D'ETRE ENREGISTRE ! ON DISTRIBUE LES KP ------//
	
			$r_defi = $bdd->prepare('SELECT gagnant,distribution FROM defi
									WHERE id=:id')
									or die(print_r($bdd->errorInfo()));
			$r_defi->execute(array('id' => $element1['id']))
									or die(print_r($bdd->errorInfo()));
			$d_defi = $r_defi->fetch();
			
			/* 
				ON VERIFIE SI LE GAGNANT EST INSCRIT DANS LA BDD
				ET QUE LA DISTRIBUTION N'EST PAS DEJA FAITE
			*/
			if(isset($d_defi['gagnant'],$d_defi['distribution']) 
			AND $d_defi['gagnant'] != '' AND $d_defi['distribution'] == 0
			AND $d_defi['gagnant'] != 'aucun')
			{				
				// ON RECUPERE SES KP POUR FAIRE L'ADDITION
				$r_kp = $bdd->prepare('SELECT kp FROM membres
										WHERE identifiant=:identifiant')
										or die(print_r($bdd->errorInfo()));
				$r_kp->execute(array('identifiant' => $d_defi['gagnant']))
										or die(print_r($bdd->errorInfo()));
				$d_kp = $r_kp->fetch();
				
				// SES KP + LE DOUBLE DE CE QU'IL A PARIE
				$total = $d_kp['kp'] + ($element1['somme'] * 2);
				
				// ON INSERE SES NOUVEAUX KP
				$r_kp2= $bdd->prepare('UPDATE membres SET kp=:kp 
										WHERE identifiant=:identifiant')
										or die(print_r($bdd->errorInfo()));
				$r_kp2->execute(array('kp' => $total,
										'identifiant' => $d_defi['gagnant']))
										or die(print_r($bdd->errorInfo()));	
				$r_kp2->closeCursor(); // Termine le traitement de la requête
				
				// LES KP SONT DISTRIBUES ! ON MONTRE QUE LA DISTRIBUTION EST FAITE
				$r_kp3= $bdd->prepare('UPDATE defi SET distribution=:distribution WHERE id=:id')
										or die(print_r($bdd->errorInfo()));
				$r_kp3->execute(array('distribution' => 1,
										'id' => $element1['id']))
										or die(print_r($bdd->errorInfo()));	
				$r_kp3->closeCursor(); // Termine le traitement de la requête
			}
			// IL Y A AUCUN GAGNANT ON REDISTRIBUE LES KP
			elseif(isset($d_defi['gagnant'],$d_defi['distribution']) 
			AND $d_defi['gagnant'] != '' AND $d_defi['distribution'] == 0
			AND $d_defi['gagnant'] == 'aucun')
			{
				// $donnees_from1 => donnees du joueur différents de SESSION
				$r_autre = $bdd->prepare('SELECT kp FROM membres
										WHERE identifiant=:identifiant')
										or die(print_r($bdd->errorInfo()));
				$r_autre->execute(array('identifiant' => $donnees_from1['identifiant']))
										or die(print_r($bdd->errorInfo()));
				$d_autre = $r_autre->fetch();
				
				$total_autre= $d_autre['kp'] + $element1['somme'] + 1; // ON AJOUTE LE 1 QU'IL A PAYE EN PLUS
				$total_session= $_SESSION['kp'] + $element1['somme'] + 1; // ON AJOUTE LE 1 QU'IL A PAYE EN PLUS
				
				// ON ENREGISTRE SES KP (AUTRE)
				$r_autre2= $bdd->prepare('UPDATE membres SET kp=:kp WHERE identifiant=:identifiant')
										or die(print_r($bdd->errorInfo()));
				$r_autre2->execute(array('kp' => $total_autre,
										'identifiant' => $donnees_from1['identifiant']))
										or die(print_r($bdd->errorInfo()));	
				$r_autre2->closeCursor(); // Termine le traitement de la requête
				
				// ON ENREGISTRE SES KP (AUTRE)
				$r_session= $bdd->prepare('UPDATE membres SET kp=:kp WHERE identifiant=:identifiant')
										or die(print_r($bdd->errorInfo()));
				$r_session->execute(array('kp' => $total_session,
										'identifiant' => $_SESSION['identifiant']))
										or die(print_r($bdd->errorInfo()));	
				$r_session->closeCursor(); // Termine le traitement de la requête
				
				// LES KP SONT DISTRIBUES ! ON MONTRE QUE LA DISTRIBUTION EST FAITE
				$r_kp4= $bdd->prepare('UPDATE defi SET distribution=:distribution WHERE id=:id')
										or die(print_r($bdd->errorInfo()));
				$r_kp4->execute(array('distribution' => 1,
										'id' => $element1['id']))
										or die(print_r($bdd->errorInfo()));	
				$r_kp4->closeCursor(); // Termine le traitement de la requête
		
			}
			
			//----------------------------------------------------------------//			
			echo '
			<div class="contient_cours">
				<div class="argent_droite" title="Gagnez '.$total_affichage.' Kp">
					<p style="margin-top:20px;">
						'.$total_affichage.'<br/>
						Kp
					</p>
				</div>
				<div class="defi_cours">
					<div class="cours_bloc1">
						<img class="defi_eclair" src="images/eclair.png" alt="VS"/>
						<span class="centre_image30" style="float:right;">';
						
						if(isset($donnees_from1['photo_profil']) 
						AND $donnees_from1['photo_profil'] != '' 
						AND $donnees_from1['photo_profil'] != 0)
						{  
							$source = getimagesize('images_utilisateurs/'.$donnees_from1['photo_profil']); 	// La photo est la source
							if ($source[0] <= 30 AND $source[1] <= 30)
								echo '<img src="images_utilisateurs/'.$donnees_from1['photo_profil'].'" alt="Photo de profil" />';
							else
								echo '<img src="images_utilisateurs/mini_2_'.$donnees_from1['photo_profil'].'" alt="Photo de profil" />';
						}
						else
							echo'<img src="images/image_defaut.png" alt="Image"/>';
					
					
						echo'
						</span>
						<p id="identifiant">
							'.$donnees_from1['identifiant'].'
						</p>
					</div>';
				
		// on recupère les scores :	-------------------------------------------
		$r_score = $bdd->prepare('SELECT * FROM traitement_defi 
								WHERE id_defi=:id_defi 
								AND identifiant=:id
								ORDER BY score DESC,temps DESC')
								or die(print_r($bdd->errorInfo()));
		$r_score->execute(array('id_defi' => $element1['id'],
								'id' => 'id1'));
		$d_score = $r_score->fetch();
		$r_score2 = $bdd->prepare('SELECT * FROM traitement_defi 
								WHERE id_defi=:id_defi 
								AND identifiant=:id
								ORDER BY score DESC,temps DESC')
								or die(print_r($bdd->errorInfo()));
		$r_score2->execute(array('id_defi' => $element1['id'],
								'id' => 'id2'));
		$d_score2 = $r_score2->fetch();
		// --------------------------------------------------------------------
			
					// LA SESSION GAGNE LE DEFI -----------------------------------/
					if(isset($element1['gagnant']) AND $element1['gagnant'] != '' 
					AND $element1['gagnant'] == $_SESSION['identifiant'])
					{
						echo'
						<div class="cours_bloc_victoire">
							Victoire
							<a onclick ="var sup=confirm(\'Êtes vous sur de vouloir retirer ce défi de l\'affiche ?\');
							if (sup == 0)return false;"href="defi_post.php?retirer&id='.$element1['id'].'&identifiant='.$_SESSION['identifiant'].'">
								<div style="margin-top:13px;"class="message_supprimer" title="Retirer de l\'affichage" ></div>
							</a>
						</div>
		
				</div>
				<div class="defi_acceder">';
		
					if($identifiant == 1)
					{
						echo'
						<a href="defi_jeu.php?id_defi='.$element1['id'].'&id1" class="afficher_scores"
						onclick="this.nextElementSibling.style.display = \'block\';return false;">
							Afficher les scores
						</a>'; /////////////////////////////////////////////////
						echo '
						<div class="affichage_scores" style="
display:none;position:fixed;width:300px; height:300px;line-height:30px;
top:30%;margin-top:-25px; /* moitié de la hauteur pour centrer */
text-align:center;z-index: 60;background-color:#89cf9d;font-size:large;color:white;">
							Victoire !  <br /> Vous avez gagné : '.$total_affichage.' Kp. <br />
							Défi réalisé sur le jeux '.$element1['jeu'].' <br />
							Meilleures scores : <br />
						'.$element1['identifiant_1'].' : '.$d_score['score'].' pt';
						if ($d_score['temps'] != 0)
							echo ' '.$d_score['temps'].' s';
						echo '<br />';
						
						echo''.$element1['identifiant_2'].' : '.$d_score2['score'].' pt';
						if ($d_score2['temps'] != 0)
							echo ' '.$d_score2['temps'].' s';
							
						echo'<br />
						<a href="#" class="fermer_affichage_score"
						onclick="this.parentNode.style.display = \'none\';return false;">Fermer</a>
						</div>';
							
					}
					elseif($identifiant == 2)
					{
						echo'
						<a href="defi_jeu.php?id_defi='.$element1['id'].'&id2" class="afficher_scores"
						onclick="this.nextElementSibling.style.display = \'block\';return false;">
							Afficher les scores
						</a>'; /////////////////////////////////////////////////
						echo '
						<div class="affichage_scores" style="
display:none;position:fixed;width:300px; height:300px;line-height:30px;
top:30%;margin-top:-25px; /* moitié de la hauteur pour centrer */
text-align:center;z-index: 60;background-color:#89cf9d;font-size:large;color:white;">
							Victoire !  <br /> Vous avez gagné : '.$total_affichage.' Kp. <br />
							Défi réalisé sur le jeux '.$element1['jeu'].' <br />
							Meilleures scores : <br />
						'.$element1['identifiant_2'].' : '.$d_score2['score'].' pt';
						if ($d_score2['temps'] != 0)
							echo ' '.$d_score2['temps'].' s';
						echo '<br />';
						
						echo''.$element1['identifiant_1'].' : '.$d_score['score'].' pt';
						if ($d_score['temps'] != 0)
							echo ' '.$d_score['temps'].' s';
							
						echo'<br />
						<a href="#" class="fermer_affichage_score"
						onclick="this.parentNode.style.display = \'none\';return false;">Fermer</a>
						</div>';
					}
					
				echo'	
				</div>';
					}
					// LA SESSION PERD LE DEFI ------------------------------------/
					elseif(isset($element1['gagnant']) AND $element1['gagnant'] != '' 
					AND $element1['gagnant'] != $_SESSION['identifiant']
					AND $element1['gagnant'] != 'aucun')
					{
						echo'
						<div class="cours_bloc_defaite">
							Défaite
							<a onclick ="var sup=confirm(\'Êtes vous sur de vouloir retirer ce défi de l\'affiche ?\');
							if (sup == 0)return false;"href="defi_post.php?retirer&id='.$element1['id'].'&identifiant='.$_SESSION['identifiant'].'">
								<div style="margin-top:13px;"class="message_supprimer" title="Retirer de l\'affichage" ></div>
							</a>
						</div>
				</div>
				
				<div class="defi_acceder">';
		
					if($identifiant == 1)
					{
						echo'
						<a href="defi_jeu.php?id_defi='.$element1['id'].'&id1" class="afficher_scores"
						onclick="this.nextElementSibling.style.display = \'block\';return false;">
							Afficher les scores  
						</a>'; /////////////////////////////////////////////////
						echo '
						<div class="affichage_scores" style="
display:none;position:fixed;width:300px; height:300px;line-height:30px;
top:30%;margin-top:-25px; /* moitié de la hauteur pour centrer */
text-align:center;z-index: 60;background-color:#89cf9d;font-size:large;color:white;">
							Défaite !  <br /> 
							Défi réalisé sur le jeux '.$element1['jeu'].' <br />
							Meilleures scores : <br />
						'.$element1['identifiant_2'].' : '.$d_score2['score'].' pt';
						if ($d_score2['temps'] != 0)
							echo ' '.$d_score2['temps'].' s';
						echo '<br />';
						
						echo''.$element1['identifiant_1'].' : '.$d_score['score'].' pt';
						if ($d_score['temps'] != 0)
							echo ' '.$d_score['temps'].' s';
							
						echo'<br />
						<a href="#" class="fermer_affichage_score"
						onclick="this.parentNode.style.display = \'none\';return false;">Fermer</a>
						</div>';
					}
					elseif($identifiant == 2)
					{
						echo'
						<a href="defi_jeu.php?id_defi='.$element1['id'].'&id2" class="afficher_scores"
						onclick="this.nextElementSibling.style.display = \'block\';return false;">
							Afficher les score
						</a>'; /////////////////////////////////////////////////
						echo '
						<div class="affichage_scores" style="
display:none;position:fixed;width:300px; height:300px;line-height:30px;
top:30%;margin-top:-25px; /* moitié de la hauteur pour centrer */
text-align:center;z-index: 60;background-color:#89cf9d;font-size:large;color:white;">
							Défaite !  <br /> 
							Défi réalisé sur le jeux '.$element1['jeu'].' <br />
							Meilleures scores : <br />
						'.$element1['identifiant_1'].' : '.$d_score['score'].' pt';
						if ($d_score['temps'] != 0)
							echo ' '.$d_score['temps'].' s';
						echo '<br />';
						
						echo''.$element1['identifiant_2'].' : '.$d_score2['score'].' pt';
						if ($d_score2['temps'] != 0)
							echo ' '.$d_score2['temps'].' s';
							
						echo'<br />
						<a href="#" class="fermer_affichage_score"
						onclick="this.parentNode.style.display = \'none\';return false;">Fermer</a>
						</div>';
					}
						
				echo'
				</div>';
					}
					// AUCUN SCORE ENREGISTRE -------------------------------------/
					else
					{
						echo'
						<div class="cours_bloc_aucun">
							Aucun score
							<a onclick ="var sup=confirm(\'Êtes vous sur de vouloir retirer ce défi de l\'affiche ?\');
							if (sup == 0)return false;"href="defi_post.php?retirer&id='.$element1['id'].'&identifiant='.$_SESSION['identifiant'].'">
								<div style="margin-top:13px;"class="message_supprimer" title="Retirer de l\'affichage" ></div>
							</a>
						</div>
				</div>';
					}
			echo'
			</div>';
				
		}
		// NE PAS METTRE DE ELSE CAR IL Y A LE SUPPRIMER DE L'AFFICHAGE ------//
		elseif($temps_restant >= 0)
		{
			$heures = floor($temps_restant / 3600);
			$min = floor(($temps_restant % 3600)/60) ;
			$sec = floor((($temps_restant % 3600) % 60));
			
			echo '
			<div class="contient_cours">
				<div class="argent_droite" title="Gagnez '.$total_affichage.' Kp">
					<p style="margin-top:20px;">
						'.$total_affichage.'<br/>
						Kp
					</p>
				</div>
				<div class="defi_cours" id="'.$element1['id'].'">
					<div class="cours_bloc1">
						<img class="defi_eclair" src="images/eclair.png" alt="VS"/>
						<span class="centre_image30" style="float:right;">';
						
						if(isset($donnees_from1['photo_profil']) 
						AND $donnees_from1['photo_profil'] != '' 
						AND $donnees_from1['photo_profil'] != 0)
						{  
							$source = getimagesize('images_utilisateurs/'.$donnees_from1['photo_profil']); 	// La photo est la source
							if ($source[0] <= 30 AND $source[1] <= 30)
								echo '<img src="images_utilisateurs/'.$donnees_from1['photo_profil'].'" alt="Photo de profil" />';
							else
								echo '<img src="images_utilisateurs/mini_2_'.$donnees_from1['photo_profil'].'" alt="Photo de profil" />';
						}
						else
							echo'<img src="images/image_defaut.png" alt="Image"/>';
					
					
						echo'
						</span>
						<p id="identifiant" title="'.$donnees_from1['identifiant'].'">
							'.$donnees_from1['identifiant'].'
						</p>
					</div>';
					
					if($element1['jeu'] == 'esquive')
					{
						echo'
						<div class="cours_bloc2">
							<span class="compteur" title="'.$element1['id'].'"></span>
						</div>';
					}
					elseif($element1['jeu'] == 'lettre')
					{
						echo'
						<div class="cours_bloc3">						
							<span class="compteur" title="'.$element1['id'].'"></span>
						</div>';
					}				
					elseif($element1['jeu'] == 'cible')
					{
						echo'
						<div class="cours_bloc4">
							<span class="compteur" title="'.$element1['id'].'"></span>
						</div>';
					}
					
				echo'
				</div>
				
				<div class="defi_acceder">';
				
					if($identifiant == 1)
						echo'
						<a href="defi_jeu.php?id_defi='.$element1['id'].'&id1">
							Accéder
						</a>';
					elseif($identifiant == 2)
						echo'
						<a href="defi_jeu.php?id_defi='.$element1['id'].'&id2">
							Accéder
						</a>';
						
				echo'
				</div>
			</div>';
			
		}                        
	}
	
	if($compteur_defi == 0)
	{
		echo'
		<p style="color:grey; text-align:center;margin-top:33px;margin-left:10px;margin-right:10px;">
			Vous n\'avez aucun défi en cours
		</p>';

	}
	
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script>
setInterval(function() 
{
	var compteur = document.getElementsByTagName('span'),
	compteurTaille = compteur.length;
		for (var i = 0 ; i < compteurTaille ; i++) 
		{ 
			if (compteur[i] && compteur[i].className == 'compteur')
			{	
				var id_defi = compteur[i].title;
				
				$.getJSON('defi_ajax.php?id='+id_defi+'&i='+i, function(data) {				
					var h = data['h'];
					var m = data['m'];
					var s = data['s'];
					var j = data['i'];
					
					
					s = s - 1;
					if ( s == -1)
					{
						m = m - 1;
						s = 59;
					}
					if ( m == -1)
					{
						h = h - 1;
						m = 59;
					}
						
					var heures = h;
					var minutes = m;
					var secondes = s;

					var avant_heure = '0';
					var avant_minute = '0';
					var avant_seconde = '0';
						
					if (heures == 0)
					{
						heures = '00';
					}
					else if (heures < 10)
					{
						heures = avant_heure + heures;
					}
					
					if (minutes == 0)
					{
						minutes = '00';
					}
					else if (minutes < 10)
					{
						minutes = avant_minute + minutes;
					}

					if (secondes == 0)
					{
						secondes = '00';
					}
					else if (secondes < 10)
					{
						secondes = avant_seconde + secondes;
					}
					var compteur2 = document.getElementsByTagName('span'),
					compteur2Taille = compteur.length;
					for (var i = 0 ; i < compteur2Taille ; i++) 
					{ 
						if (compteur2[j] && compteur2[j].className == 'compteur')
						{	
							if (heures == '00' && minutes == '00' && secondes == '00')
								compteur[j].innerHTML =  '00 : 00 : 00';
							else
								compteur[j].innerHTML =  heures + ' : ' + minutes + ' : ' + secondes;
						}
					}

				});
						
			}		
		}
}, 1000);
</script>