<?php
  function construit_url_paypal()
  {
	$api_paypal = 'https://api-3t.sandbox.paypal.com/nvp?'; // Site de l'API PayPal. On ajoute déjà le ? afin de concaténer directement les paramètres.
	$version = 119; // Version de l'API
	
	$user = 'pierremajeur-facilitator-1_api1.gmail.com'; // Utilisateur API
	$pass = 'X5NA98AZ7VQ8J2QA'; // Mot de passe API
	$signature = 'AFcWxV21C7fd0v3bYYYRCpSSRl31A5C5wfRZ6WI6-sNkjhxdwEJ4sCLt'; // Signature de l'API

	$api_paypal = $api_paypal.'VERSION='.$version.'&USER='.$user.'&PWD='.$pass.'&SIGNATURE='.$signature; // Ajoute tous les paramètres

	return 	$api_paypal; // Renvoie la chaîne contenant tous nos paramètres.
  }
  function recup_param_paypal($resultat_paypal)
  {
	$liste_parametres = explode("&",$resultat_paypal); // Crée un tableau de paramètres
	foreach($liste_parametres as $param_paypal) // Pour chaque paramètre
	{
		list($nom, $valeur) = explode("=", $param_paypal); // Sépare le nom et la valeur
		$liste_param_paypal[$nom]=urldecode($valeur); // Crée l'array final
	}
	return $liste_param_paypal; // Retourne l'array
  }
  
if (isset($_GET['prix'],$_GET['kp']))
{
	if (($_GET['prix'] == 0.20 AND $_GET['kp'] == 1) OR
		($_GET['prix'] == 2.0 AND $_GET['kp'] == 10) OR
		($_GET['prix'] == 10.0 AND $_GET['kp'] == 53) OR
		($_GET['prix'] == 20.0 AND $_GET['kp'] == 110))
	{
		session_start();
		$_SESSION['kp_achete'] = $_GET['kp'];
		$requete = construit_url_paypal(); // Construit les options de base

		// La fonction urlencode permet d'encoder au format URL les espaces, slash, deux points, etc.)
		$requete = $requete."&METHOD=SetExpressCheckout".
					"&CANCELURL=".urlencode("http://localhost/kap%20V2/compte.php?annuler").
					"&RETURNURL=".urlencode("http://localhost/kap%20V2/compte.php?retour&amt=".$_GET['prix']."").
					"&AMT=".$_GET['prix'].
					"&CURRENCYCODE=EUR".
					"&DESC=".urlencode("Commande de ".$_GET['kp']."Kp pour : ".$_GET['prix']."€").
					"&LOCALECODE=FR".
					"&HDRIMG=".urlencode("http://www.decathelineau.com/images/kap_logo.png");

		$ch = curl_init($requete);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$resultat_paypal = curl_exec($ch);

		if (!$resultat_paypal)
			{echo "<p>Erreur</p><p>".curl_error($ch)."</p>";}
		else
		{
			$liste_param_paypal = recup_param_paypal($resultat_paypal); // Lance notre fonction qui dispatche le résultat obtenu en un array

			// Si la requête a été traitée avec succès
			if ($liste_param_paypal['ACK'] == 'Success')
			{
				// Redirige le visiteur sur le site de PayPal
				header("Location: https://www.sandbox.paypal.com/webscr&cmd=_express-checkout&token=".$liste_param_paypal['TOKEN']);
			}
			else // En cas d'échec, affiche la première erreur trouvée.
			{echo "<p>Erreur de communication avec le serveur PayPal.<br />".$liste_param_paypal['L_SHORTMESSAGE0']."<br />".$liste_param_paypal['L_LONGMESSAGE0']."</p>";}		
		}
		curl_close($ch);
	}
	else
		header('Location: compte.php?erreur_prix');
}
