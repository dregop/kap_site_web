<?php
include('header.php');

?>
<div id="wrap">
<div class="corps">

	<div style="float:left;width:980px;margin-top:30px;margin-left:22px;">
	
<?php
		$nbr_message = 0;
		$r_message = $bdd->prepare('SELECT id FROM messages
									WHERE message_to=:message_to
									ORDER BY id DESC')
									or die(print_r($bdd->errorInfo()));
		$r_message->execute(array('message_to' => $_SESSION['id_membre']))
									or die(print_r($bdd->errorInfo()));
		while ($d_message= $r_message->fetch())
		{
			$nbr_message++;
		}
		
		echo'
		<p style="text-align:center;margin-top:0px;color:grey;padding-bottom:20px;">
			Message ('.$nbr_message.')
		</p>';

	$i = 0; // POUR LE BLOC CLEAR POUR ORGANISER LES MESSAGES 
	//REQUETE POUR PRENDRE LES MESSAGES
	$r_message = $bdd->prepare('SELECT * FROM messages
								WHERE message_to=:message_to
								ORDER BY id DESC')
								or die(print_r($bdd->errorInfo()));
	$r_message->execute(array('message_to' => $_SESSION['id_membre']))
								or die(print_r($bdd->errorInfo()));
	while ($d_message= $r_message->fetch())
	{
			
		$requete2 = $bdd->prepare('SELECT id, identifiant, photo_profil,
									pays FROM membres
									WHERE id=:id')
									or die(print_r($bdd->errorInfo()));
		$requete2->execute(array('id' => $d_message['message_from']))
									or die(print_r($bdd->errorInfo()));
		$donnees2 = $requete2->fetch();
		echo'
		<div class="bloc_message" id="bloc_message" style="margin-left:40px;">
		
			<a onclick ="var sup=confirm(\'Êtes vous sur de vouloir supprimer ce message ?\');
			if (sup == 0)return false;"href="message_post.php?id='.$d_message['id'].'&supprimer">
				<div class="message_supprimer" title="Supprimer"></div>
			</a>
			
			<div class="message_bloc1">';
			
				if(isset($donnees2['photo_profil']) 
				AND $donnees2['photo_profil'] != ''
				AND $donnees2['photo_profil'] != 0)
				{  
					echo'
					<div class="centre_image30" style="float:left;">';
					
					$source = getimagesize('images_utilisateurs/'.$donnees2['photo_profil']); 	// La photo est la source
					if ($source[0] <= 30 AND $source[1] <= 30)
						echo '<img src="images_utilisateurs/'.$donnees2['photo_profil'].'" alt="Photo de profil" />';
					else
						echo '<img src="images_utilisateurs/mini_2_'.$donnees2['photo_profil'].'" alt="Photo de profil" />';
					
					echo'
					</div>';
				}
				else
					echo'<img style="float:left;"src="images/image_defaut.png" alt="Image"/>';
			
			echo'
			
				<a href="'.urlencode(stripslashes(htmlspecialchars($donnees2['identifiant']))).'">
					<p>'.stripslashes(htmlspecialchars($donnees2['identifiant'])).'</p>
				</a>
			</div>

			<div class="message_text" >
			
				<p> 
					'.stripslashes(htmlspecialchars($d_message['message'])).'
				</p>
				
			</div>
			
			<a href="#" name="'.$donnees2['id'].'"  id="repondre_message">
				<div class="message_repondre" title="Répondre"> </div>
			</a>
			

		</div>';
		$i++;
		
		if($i%3 == 0) // NBR DIVISIBLE PAR 3
		{
			echo'<div class="message_clear" style="width:980px;"></div>';
		}
	}
	echo'
	</div>';

?>
	<div class="erreur" ></div>
</div>
</div>

<?php
include('footer.php');
?>

</body>

</html>