function compte_a_rebours()
{
	var compte_a_rebours1 = document.getElementById("compte_a_rebours1");
	
	var date_actuelle = new Date();
	var date_evenement = new Date("Mar	23 22:00:00 2015");
	var total_secondes = (date_evenement - date_actuelle) / 1000;

	if (total_secondes > 0)
	{
		var jours = Math.floor(total_secondes / (60 * 60 * 24));
		var heures = Math.floor((total_secondes - (jours * 60 * 60 * 24)) / (60 * 60));
		var minutes = Math.floor((total_secondes - ((jours * 60 * 60 * 24 + heures * 60 * 60))) / 60);
		var secondes = Math.floor(total_secondes - ((jours * 60 * 60 * 24 + heures * 60 * 60 + minutes * 60)));

		var avant_jour = '0';
		var avant_heure = '0';
		var avant_minute = '0';
		var avant_seconde = '0';

		if (jours == 0)
		{
			jours = '00';
		}
		else if (jours < 10)
		{
			jours = avant_jour + jours;
		}
		
		if (heures == 0)
		{
			heures = '00';
		}
		else if (heures < 10)
		{
			heures = avant_heure + heures;
		}

		if (minutes == 0)
		{
			minutes = '00';
		}
		else if (minutes < 10)
		{
			minutes = avant_minute + minutes;
		}

		if (secondes == 0)
		{
			secondes = '00';
		}
		else if (secondes < 10)
		{
			secondes = avant_seconde + secondes;
		}


		compte_a_rebours1.innerHTML = jours + ' : ' + heures + ' : ' + minutes + ' : ' + secondes;
	}
	else
	{
		compte_a_rebours1.innerHTML = 'Mardi - 20h';
	}

	var actualisation = setTimeout("compte_a_rebours();", 1000);
}
compte_a_rebours();

function compte_a_rebours2()
{
	var compte_a_rebours2 = document.getElementById("compte_a_rebours2");
	
	var date_actuelle = new Date();
	var date_evenement = new Date("Oct 11 22:22:00 2014");
	var total_secondes = (date_evenement - date_actuelle) / 1000;


	if (total_secondes > 0)
	{
		var jours = Math.floor(total_secondes / (60 * 60 * 24));
		var heures = Math.floor((total_secondes - (jours * 60 * 60 * 24)) / (60 * 60));
		var minutes = Math.floor((total_secondes - ((jours * 60 * 60 * 24 + heures * 60 * 60))) / 60);
		var secondes = Math.floor(total_secondes - ((jours * 60 * 60 * 24 + heures * 60 * 60 + minutes * 60)));

		var avant_jour = '0';
		var avant_heure = '0';
		var avant_minute = '0';
		var avant_seconde = '0';

		if (jours == 0)
		{
			jours = '00';
		}
		else if (jours < 10)
		{
			jours = avant_jour + jours;
		}

		if (heures == 0)
		{
			heures = '00';
		}
		else if (heures < 10)
		{
			heures = avant_heure + heures;
		}

		if (minutes == 0)
		{
			minutes = '00';
		}
		else if (minutes < 10)
		{
			minutes = avant_minute + minutes;
		}

		if (secondes == 0)
		{
			secondes = '00';
		}
		else if (secondes < 10)
		{
			secondes = avant_seconde + secondes;
		}


		compte_a_rebours2.innerHTML = jours + ' : ' + heures + ' : ' + minutes + ' : ' + secondes;
	}
	else
	{
		compte_a_rebours2.innerHTML = '	Mardi - 20h';
	}

	var actualisation = setTimeout("compte_a_rebours2();", 1000);
}
compte_a_rebours2();