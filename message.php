<?php
include('header.php');

unset($_SESSION['formulaire_password']); // SESSION POUR LE BLOC MDP
unset($_SESSION['formulaire_email']); // SESSION POUR LE BLOC EMAIL
unset($_SESSION['numero_partie']); 
unset($_SESSION['envoyer_message']); // SESSION POUR LE BLOC MESSAGE

//On doit le mettre dans toutes les pages accessibles depuis le header
// INDEX , JEUX , DEFI , COMPTE, MESSAGE, PROFIL, RECHERCHE (changer sur ces pages quand le tournoi change)
$_SESSION['tournoi_esquive'] = 'on';
$_SESSION['tournoi_lettre'] = 'off';
$_SESSION['tournoi_cible'] = 'off';

if (isset($_GET['done']))
{
	echo '
	<div id="fond_overlay" style="display:block;"></div>
	<div id="overlay_vert" style="display:block;">
			Message Envoyé
	</div>';
}
?>
<div id="fond_overlay2"></div>

<div id="wrap">
<div class="corps">
	<div id="message_gauche">
<?php
		$nbr_demande = 0;
		$r_demande = $bdd->prepare('SELECT * FROM amis 
									WHERE ami_to=:ami_to 
									AND ami_confirm=:ami_confirm
									ORDER BY ami_date DESC LIMIT 0,15')
									or die(print_r($bdd->errorInfo()));
		$r_demande->execute(array('ami_to' => $_SESSION['id_membre'],
									'ami_confirm' => 0))
									or die(print_r($bdd->errorInfo()));
		while ($d_demande= $r_demande->fetch())
		{
			$nbr_demande++;
		}
		echo'
		<p style="margin-left:120px;margin-top:0px;color:grey;">
			Demande d\'ami ('.$nbr_demande.')
		</p>';
		
	//REQUETE POUR PRENDRE LES DEMANDE D'AJOUT (ami_to : on lui fait la demande)
	$nbr = 0;
	$r_demande = $bdd->prepare('SELECT * FROM amis 
								WHERE ami_to=:ami_to 
								AND ami_confirm=:ami_confirm
								ORDER BY ami_date DESC LIMIT 0,15')
								or die(print_r($bdd->errorInfo()));
	$r_demande->execute(array('ami_to' => $_SESSION['id_membre'],
								'ami_confirm' => 0))
								or die(print_r($bdd->errorInfo()));
	while ($d_demande= $r_demande->fetch())
	{
		$requete1 = $bdd->prepare('SELECT id, identifiant, photo_profil,
									pays FROM membres
									WHERE id=:id')
									or die(print_r($bdd->errorInfo()));
		$requete1->execute(array('id' => $d_demande['ami_from']))
									or die(print_r($bdd->errorInfo()));
		$donnees1 = $requete1->fetch();
		
		echo'
		<div class="bloc_demande">
			<div class="demande_bloc1">';
			
				if(isset($donnees1['photo_profil']) 
				AND $donnees1['photo_profil'] != ''
				AND $donnees1['photo_profil'] != 0)
				{  
					echo'
					<span class="centre_image120">';
					
					$source = getimagesize('images_utilisateurs/'.$donnees1['photo_profil']); 	// La photo est la source
					if ($source[0] <= 30 AND $source[1] <= 30)
						echo '<img src="images_utilisateurs/'.$donnees1['photo_profil'].'" alt="Photo de profil" />';
					else
						echo '<img src="images_utilisateurs/mini_1_'.$donnees1['photo_profil'].'" alt="Photo de profil" />';
					
					echo'
					</span>';
				}
				else
					echo'<img style="opacity:0.6;"src="images/image_defaut_grd.png" alt="Image"/>';
			
			echo'
			</div>
			
			<div class="demande_bloc2">
				<a href="'.urlencode(stripslashes(htmlspecialchars($donnees1['identifiant']))).'">
					<p>'.stripslashes(htmlspecialchars($donnees1['identifiant'])).'</p>
				</a>
			</div>

			<a href="ami_post.php?id_ami='.$donnees1['id'].'&identifiant='.urlencode(stripslashes(htmlspecialchars($donnees1['identifiant']))).'&accepter">
				<div class="demande_bloc3" title="Accepter"></div>
			</a>
			<a href="ami_post.php?id_ami='.$donnees1['id'].'&refuser">
				<div class="demande_bloc4" title="Refuser"></div>
			</a>';
			
		echo'
		</div>';
		
		$nbr++;
	
	}
	
	if($nbr == 0)
	{
		echo'
		<p style="margin-left:50px;padding-top:40px;color:#528661">
			Vous n\'avez pas de demande d\'ami
		</p>';
	}
	if($nbr > 15)
	{
		echo'
		<div class="message_clear"></div>
		<a href="all-requests">
			<p class="lien_message" style="width:225px;margin-left:55px;border:1px dashed grey;padding:10px;">
				Afficher toutes les demandes +
			</p>
		</a>';
	}
	
	echo'
	</div>';
	
	$nbr_message = 0;
	$r_message = $bdd->prepare('SELECT id FROM messages
								WHERE message_to=:message_to
								ORDER BY id DESC')
								or die(print_r($bdd->errorInfo()));
	$r_message->execute(array('message_to' => $_SESSION['id_membre']))
								or die(print_r($bdd->errorInfo()));
	while ($d_message= $r_message->fetch())
	{
		$nbr_message++;
	}
	
	echo'
	<div id="message_droite">
		<p style="text-align:center;margin-top:0px;color:grey;padding-bottom:20px;">
			Message ('.$nbr_message.')
		</p>';
		
	$i = 1; // POUR LE BLOC CLEAR POUR ORGANISER LES MESSAGES 
	//REQUETE POUR PRENDRE LES MESSAGES
	$r_message = $bdd->prepare('SELECT * FROM messages
								WHERE message_to=:message_to
								ORDER BY id DESC LIMIT 0,20')
								or die(print_r($bdd->errorInfo()));
	$r_message->execute(array('message_to' => $_SESSION['id_membre']))
								or die(print_r($bdd->errorInfo()));
	while ($d_message= $r_message->fetch())
	{
			
		$requete2 = $bdd->prepare('SELECT id, identifiant, photo_profil,
									pays FROM membres
									WHERE id=:id')
									or die(print_r($bdd->errorInfo()));
		$requete2->execute(array('id' => $d_message['message_from']))
									or die(print_r($bdd->errorInfo()));
		$donnees2 = $requete2->fetch();
		echo'
		<div class="bloc_message" id="bloc_message">
		
			<a onclick ="var sup=confirm(\'Êtes vous sur de vouloir supprimer ce message ?\');
			if (sup == 0)return false;"href="message_post.php?id='.$d_message['id'].'&supprimer">
				<div class="message_supprimer" title="Supprimer" ></div>
			</a>
			
			<div class="message_bloc1">';
			
				if(isset($donnees2['photo_profil']) 
				AND $donnees2['photo_profil'] != ''
				AND $donnees2['photo_profil'] != 0)
				{  
					echo'
					<div class="centre_image30" style="float:left;">';
					
					$source = getimagesize('images_utilisateurs/'.$donnees2['photo_profil']); 	// La photo est la source
					if ($source[0] <= 30 AND $source[1] <= 30)
						echo '<img src="images_utilisateurs/'.$donnees2['photo_profil'].'" alt="Photo de profil" />';
					else
						echo '<img src="images_utilisateurs/mini_2_'.$donnees2['photo_profil'].'" alt="Photo de profil" />';
					
					echo'
					</div>';
				}
				else
					echo'<img style="float:left;"src="images/image_defaut.png" alt="Image"/>';
			
			echo'
			
				<a href="'.urlencode(stripslashes(htmlspecialchars($donnees2['identifiant']))).'">
					<p>'.stripslashes(htmlspecialchars($donnees2['identifiant'])).'</p>
				</a>
			</div>

			<div class="message_text" >
			
				<p> 
					'.stripslashes(htmlspecialchars($d_message['message'])).'
				</p>
				
			</div>
			
			<a href="#" class="repondre_message">
				<div class="message_repondre" title="Répondre"> </div>
			</a>
			

		</div>';
		
		echo'
		<div class="overlay_repondre_message" id="overlay_repondre_message">
			
			<div id="centre_element">
				<p>
					Réponde à 
					<a class="message_to" href="'.urlencode(stripslashes(htmlspecialchars($donnees2['identifiant']))).'">
						'.stripslashes(htmlspecialchars($donnees2['identifiant'])).'					
					</a>
				</p>
				<form action="message_post.php" method="post">
					<textarea rows="3" cols="30" name="message" ></textarea>
					<input type="hidden" name="message_to" value="'.$donnees2['id'].'"/>
					<input type="hidden" name="reponse" value="0"/>
					<input type="submit" name="envoyer" value="Envoyer"/>
				</form>
				<a href="#" class="fermer_reponse">
					<div class="message_supprimer" style="position:relative;bottom:175px;left:13px;" title="Supprimer" ></div>
				</a>
			</div>
		</div>';

		$i++;
		
		if($i%2) // NBR DIVISIBLE PAR 2
		{
			echo'<div class="message_clear"></div>';
		}
	}

	if($i == 1)
	{
		echo'
		<p style="text-align:center;padding-top:20px;color:#448593;">
			Vous n\'avez pas de message
		</p>';
	}
	if($i > 21) // 21 parce que le $i commence à 1
	{
		echo'
		<a href="all-messages">
			<p class="lien_message" style="width:205px;margin:auto;border:1px dashed grey;padding:10px;">
				Afficher tous les messages +
			</p>
		</a>';
	}
	echo'
	</div>';

?>
	<div class="erreur" style="clear:right;height:2px;"></div>
	<div class="erreur" style="height:2px;"></div>
</div>
</div>

<?php
include('footer.php');
?>
	<script>
	
	var a = document.getElementsByTagName('a'),
	aTaille = a.length;
	for (var i = 0 ; i < aTaille ; i++) 
	{
		if (a[i] && a[i].className == 'repondre_message')
		{	
			a[i].onclick = function() 
			{
				this.parentNode.nextElementSibling.style.display = 'block';
				document.getElementById('fond_overlay2').style.display= 'block';
				return false; // on bloque la redirection
			}; 
		}
	}
	var fermer = document.getElementsByTagName('a'),
		fermerTaille = fermer.length;
	for (var i =0; i < fermerTaille; i++) {
		if (fermer[i] && fermer[i].className == 'fermer_reponse')
		{
			fermer[i].onclick = function() {
				this.parentNode.parentNode.style.display = 'none';
				document.getElementById('fond_overlay2').style.display= 'none';
				return false;
			};
		}
	}	
	if (document.getElementById('overlay_vert'))
	{
		setTimeout(function() {
			document.getElementById('overlay_vert').style.display="none";
			document.getElementById('fond_overlay').style.display="none";
		}, 2000);
	}
					
	</script>
</body>

</html>