<?php
// ON FAIT LES DIFFERENTES SESSIONS EN FONCTION ENTRAINEMENT ET TOURNOI
if(isset($_SESSION['tournoi_esquive'])
AND $_SESSION['tournoi_esquive'] == 'on')
{
	// C'EST LE TOURNOI
	$table = 'meilleur_tournoi_esquive';
	echo'
	<table class="affichage_classement_tournoi" id="affichage_classement">';
}
else
{
	// C'EST L'ENTRAINEMENT
	$table = 'meilleur_esquive';
	echo'
	<table class="affichage_classement" id="affichage_classement" >';
}

	$a = 0;
	$aucun_score = $bdd->prepare('SELECT id FROM '.$table.'
							WHERE identifiant=:identifiant')
							or die(print_r($bdd->errorInfo()));
	$aucun_score->execute(array('identifiant' => $_SESSION['identifiant']))
							or die(print_r($bdd->errorInfo()));
	while ($aucune_donnees = $aucun_score->fetch())
	{
		$a++;
	}

	if($a > 0)
	{
		echo'
		<tr>
			<td style="color:#448593">Place</td>
			<td style="color:#448593">Pseudo</td>
			<td style="color:#448593">Temps</td>
		</tr>';
	}
	
	$i = 0;
	$j = 1;
	$nbre = 0;
	$score = $bdd->query('SELECT * FROM '.$table.'
							ORDER BY temps DESC,temps DESC')
							or die(print_r($bdd->errorInfo()));
	while ($donnees = $score->fetch())
	{	
		if ($donnees['identifiant'] == $_SESSION['identifiant'])
		{
			$classement = $j;
			$nbre = $classement - 4;
		}
			
		$j++;
	}
	if (isset($classement))
	{
		if ($classement < 4)
			$nbre = 0;
		else if ($j - $classement == 3 AND $classement > 4)
			$nbre = $nbre - 1;
		else if ($j - $classement == 2 AND $classement > 5)
			$nbre = $nbre - 2;
		else if ($j - $classement == 1 AND $classement > 6)
			$nbre = $nbre - 3;

		$r_score = $bdd->query('SELECT * FROM '.$table.'  
								ORDER BY temps DESC,temps DESC
								LIMIT '.$nbre.',7')
							or die(print_r($bdd->errorInfo()));
		$c = $classement - 3;
		if ($classement == 1)
			$c = $classement;
		else if ($classement == 2)
			$c = $classement - 1;
		else if ($classement == 3)
			$c = $classement - 2;
		else if ($j - $classement == 3 AND $classement > 4)
			$c = $c - 1;
		else if ($j - $classement == 2 AND $classement > 5)
			$c = $c - 2;
		else if ($j - $classement == 1 AND $classement > 6)
			$c = $c - 3;
		while($d_score = $r_score->fetch())
		{
			if($d_score['identifiant'] == $_SESSION['identifiant'])
			{
				echo'
				<tr  class="fonce">';
					echo '
					<td>
						'.$c.'
					</td>
					<td>
						'.stripslashes(htmlspecialchars($d_score['identifiant'])).' 
					</td>
					<td>
						'.$d_score['temps'].' s
					</td>';
				echo'			
				</tr>';
			}
			else
			{
				echo'
				<tr>
					<td>
						'.$c.'
					</td>
					<td>
						'.stripslashes(htmlspecialchars($d_score['identifiant'])).' 
					</td>
					<td>
						'.$d_score['temps'].' s
					</td>
				</tr>';
			}
			$i++;	
			$c++;
		}
	}
	else
	{
		if(!isset($_SESSION['tournoi_esquive']))
		{
			echo '
			<tr>
				<td style="border:none;text-align:left;padding-left:15px;">
					Aucun score enregistré. 
				</td>
			</tr>';
		}
		echo'
		<tr>
			<td style="border:none;text-align:left;padding-left:15px;">
				Vous ne pouvez pas être classé...
			</td>
		</tr>';
		if(!isset($_SESSION['tournoi_esquive']))
		{
			echo'
			<tr>
				<td style="border:none;padding-top:30px;">
					Lancez une partie.
				</td>
			</tr>';
		}
	}				
?>
</table>