<?php
include('header.php');

unset($_SESSION['formulaire_password']); // SESSION POUR LE BLOC MDP
unset($_SESSION['formulaire_email']); // SESSION POUR LE BLOC EMAIL
unset($_SESSION['numero_partie']); 

//On doit le mettre dans toutes les pages accessibles depuis le header
// INDEX , JEUX , DEFI , COMPTE, MESSAGE, PROFIL, RECHERCHE (changer sur ces pages quand le tournoi change)
$_SESSION['tournoi_esquive'] = 'on';
$_SESSION['tournoi_lettre'] = 'off';
$_SESSION['tournoi_cible'] = 'off';

if(isset($_GET['id'],$_GET['supprimer']))
{
	// On supprime liens d'amitié dans la BDD
	$r2 = $bdd->prepare('DELETE FROM amis
						WHERE (ami_to=:id1 AND ami_from=:id2)
						OR (ami_to=:id2 AND ami_from=:id1)') 
						or die(print_r($bdd->errorInfo()));
	$r2->execute(array('id1' => $_GET['id'], 
						'id2' => $_SESSION['id_membre'])) 
						or die(print_r($bdd->errorInfo()));
	$r2->closeCursor(); // Termine le traitement de la requête
	
	header('Location: '.$_GET['identifiant'].'');



}
?>
<div id="wrap">
<div class="corps">
<?php

		include('profil_autre.php');

?>
</div>
</div>
<?php
include('footer.php');
?>

	<script>
	
		document.getElementById('envoyer_message').onclick = function() {
			profil_autre.style.display = 'none';
			profil_bloc2.style.display = 'none';
			profil_bloc3.style.display = 'none';
			profil_bloc6.style.display = 'block';
			profil_bloc7.style.display = 'block';
			profil_message.style.display = 'block';
			if (bloc_vert1) {
				bloc_vert1.style.marginLeft = '22px';
			}
			if (bloc_vert2) {
				bloc_vert2.style.marginLeft = '22px';
			}
			if (bloc_supprimer) {
				bloc_supprimer.style.marginLeft = '22px';
			}
			return false; // on bloque la redirection
			};
			
		bloc_vert2.addEventListener('mouseover', function () 
		{
			bloc_vert2.style.display = 'none';
			bloc_supprimer.style.display = 'block';
		},false);
		
		bloc_supprimer.addEventListener('mouseout', function () 
		{
			bloc_vert2.style.display = 'block';
			bloc_supprimer.style.display = 'none';
		},false);
	
	</script>

</body>

</html>