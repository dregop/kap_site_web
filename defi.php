<?php
include('header.php');

unset($_SESSION['formulaire_password']); // SESSION POUR LE BLOC MDP
unset($_SESSION['formulaire_email']); // SESSION POUR LE BLOC EMAIL
unset($_SESSION['numero_partie']); 
unset($_SESSION['envoyer_message']); // SESSION POUR LE BLOC MESSAGE

//On doit le mettre dans toutes les pages accessibles depuis le header
// INDEX , JEUX , DEFI , COMPTE, MESSAGE, PROFIL, RECHERCHE (changer sur ces pages quand le tournoi change)
$_SESSION['tournoi_esquive'] = 'on';
$_SESSION['tournoi_lettre'] = 'off';
$_SESSION['tournoi_cible'] = 'off';

// OVERLAY -------------------------------------------------------------------//
	echo'
	<div id="fond_overlay2" style="display:none;"></div>
	<div id="overlay_rouge2" style="display:none;">
		Vous n\'avez pas assez de Kp pour pouvoir defier quelqu\'un || 
		<a href="#" id="fermer_overlay" style="color:black;"> OK </a>
	</div>';
if(isset($_GET['envoye']))
{
	echo'
	<div id="fond_overlay" style="display:block;"></div>
	<div id="overlay_vert" style="display:block;">
		Votre défi est envoyé
	</div>';
}
elseif(isset($_GET['ready']))
{
	echo'
	<div id="fond_overlay" style="display:block;"></div>
	<div id="overlay_vert" style="display:block;">
		Votre défi a commencé !
	</div>';
}
elseif(isset($_GET['lui']))
{
	echo'
	<div id="fond_overlay" style="display:none;"></div>
	<div id="overlay_bleu" style="display:none;">
		Il vous a déjà envoyé un défi
	</div>';
}
elseif(isset($_GET['vous']))
{
	echo'
	<div id="fond_overlay" style="display:block;"></div>
	<div id="overlay_rouge" style="display:block;">
		Vous lui avez déjà envoyé un défi
	</div>';
}
?> 
<div id="fond_overlay"></div>

<div id="wrap">
<div class="corps">
<?php 
if(isset($_GET['all'])) // AFFICHAGE DE TOUTES LES DEMANDES ET EN COURS
{	
	echo'
	<p id="intro_all"> 
		Voici toutes vos demandes et tous vos défi en cours. 
		&nbsp;&nbsp;&nbsp; <a href="defi">Retour</a>
	</p>';
	
	// ON AFFICHE TOUTES LES DEMANDES
	echo'
	<div id="defi_all_demandes">';
		
		$compteur_demande =0;
		$re_c5 = $bdd->prepare('SELECT * FROM demande_defi 
								WHERE identifiant_to=:id_to')
								or die(print_r($bdd->errorInfo()));
		$re_c5->execute(array('id_to' => $_SESSION['identifiant']))
								or die(print_r($bdd->errorInfo()));
		
		// LA BOUCLE EST DANS LE INCLUDE
		include('defi_demandes.php'); //--------------------------------------//
	
	echo'
	</div>';
	
	echo'
	<div id="defi_all_cours">';
	
		// ON AFFICHE TOUTES LES DEMANDES EN COURS
		$tableau_cours = array(); // ON OUVRE UN TABLEAU POUR STOCKER
		$en_cours = 0;
		$compteur_defi = 0;
		$re_c6 = $bdd->prepare('SELECT * FROM defi 
								WHERE (identifiant_1=:id
								OR identifiant_2=:id)
								AND view != :id
								AND view != :all')
								or die(print_r($bdd->errorInfo()));
		$re_c6->execute(array('id' => $_SESSION['identifiant'],
							'all' => 'all'))
								or die(print_r($bdd->errorInfo()));
		while($donnees_c6 = $re_c6->fetch())
		{
			$tableau_cours[$en_cours] = $donnees_c6;
			$en_cours++;
			$compteur_defi++;
		}
		
		// LE FOREACH EST DANS LE INCLUDE AVEC L'AFFICHAGE 
		include('defi_cours.php'); //-----------------------------------------//
		
	echo'
	</div>
	
	<div class="erreur" style="clear:right;height:10px;"></div>
	<div class="erreur" style="height:10px;"></div>';


}
else
{
	if (isset($_GET['defier'],$_GET['identifiant']))
	{
		$req1 = $bdd->prepare('SELECT photo_profil FROM membres
							WHERE identifiant = :identifiant')
							or die(print_r($bdd->errorInfo()));
		$req1 ->execute(array('identifiant' => $_GET['identifiant'])) 
							or die(print_r($bdd->errorInfo()));
		$d_to = $req1 ->fetch();
		
		echo '
		<div id="bloc_gauche2" class="defi_complete"> 
			<p style="text-align:center;margin-top:8px;padding-bottom:22px;color:#474747">
				Vous voulez défier 
				&nbsp;&nbsp;&nbsp;&nbsp;'.$_GET['identifiant'].'&nbsp;&nbsp;
				<span class="centre_image30">';
			
					if(isset($d_to['photo_profil']) 
					AND $d_to['photo_profil'] != '' 
					AND $d_to['photo_profil'] != 0)
					{  
						$source = getimagesize('images_utilisateurs/'.$d_to['photo_profil']); 	// La photo est la source
						if ($source[0] <= 30 AND $source[1] <= 30)
							echo '<img src="images_utilisateurs/'.$d_to['photo_profil'].'" alt="Photo de profil" />';
						else
							echo '<img src="images_utilisateurs/mini_2_'.$d_to['photo_profil'].'" alt="Photo de profil" />';
					}
					else
						echo'<img src="images/image_defaut.png" alt="Image"/>';
				
			
				echo'
				</span>
			</p>
			<form action="defi_post.php" method="post">
				<div>
					<p>
						Un défi dure au maximum 24 h 00 m 00 s. Vous avez 20 
						parties pour faire le meilleur score et remporter le 
						double de votre mise. <br />
						
						Dès que la personne concernée aura accepté le défi, vous 
						serez avertis par mail et le défi commencera. <br />
						
						Que le meilleur gagne !<br />
						
					</p>
					<p id="game_choice">
						<span> Choix du jeu : </span>  <br /><br />
						
						
						<label for="esquive"><img src="images/icone_esquive.png"/></label>
						 
						<label for="lettre"><img src="images/icone_lettre.png"/></label>
						 
						<label for="cible"><img src="images/icone_cible.png"/></label><br />
						
						<input type="radio" name="jeu" value="esquive" checked="checked"  /> 
						<input type="radio" style="margin-left:130px;" name="jeu" value="lettre" />
						<input type="radio" style="margin-left:130px;" name="jeu" value="cible" />
						<br /><br />
					</p>
					<p id="money_choice">
						<span> Kp mis en jeu : (Vous avez '.$_SESSION['kp'].' Kp)</span>	<br /><br />
						
						<input type="radio" name="somme" value="2" checked="checked" /> 
						<label for="2">2 Kp</label>';
						
						if($_SESSION['kp'] < 11)
							echo'
							<input disabled disabled="true" type="radio" name="somme" value="10" /> ';
						else
							echo'
							<input type="radio" name="somme" value="10" /> ';
						echo'
						<label for="10">10 Kp</label>';
						
						if($_SESSION['kp'] < 26)
							echo'
							<input disabled disabled="true" type="radio" name="somme" value="25" />';
						else
							echo'
							<input type="radio" name="somme" value="25" />';
						echo'
						<label for="25">25 Kp</label>';
						
						if($_SESSION['kp'] < 51)
							echo'
							<input disabled disabled="true" type="radio" name="somme" value="50" /> ';
						else
							echo'
							<input type="radio" name="somme" value="50" /> ';
						echo'
						<label for="50">50 Kp</label>';
						
						if($_SESSION['kp'] < 101)
							echo'
							<input disabled disabled="true" type="radio"  name="somme" value="100" /> ';
						else	
							echo'
							<input type="radio" name="somme" value="100" /> ';
						echo'
						<label for="100">100 Kp</label>
					</p>
				
					<input type="hidden" name="identifiant" value="'.$_GET['identifiant'].'" />
					<input type="submit" name="Valider" value="Défier" id="défier" />
				</div>
			</form>	
		</div>';
	}
	else
	{
?>		
		<div class="defi_bloc1" id="bloc_gauche">
			<p style="text-align:center;margin-top:0px;color:grey;">
				<img style="position:relative;top:5px;" src="images/icone_eclair.png" alt=" "/>
				Défiez un joueur&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				
				<img style="position:relative;top:5px;" src="images/icone_meilleur.png" alt=" "/>
				Faîtes le meilleur score&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				
				<img style="position:relative;top:5px;" src="images/icone_kp.png" alt=" "/>
				Remportez la mise 	
			</p>
<?php
			echo'
			<form action="defi.php" method="post">
				<input autocomplete="off" id="defi_recherche" name="rechercher_joueur" 
				type="text" placeholder=" Chercher un jouer à défier" />
			</form>';
			
			// TOUTES LES REQUETES POUR RECUPERER LES IDENTIFIANTS DANS L'ORDRE
			include('affichage_defi.php');
			
				
			if (isset($_GET['page']))
			{
				$numero_page = $_GET['page']-1;
			}
			else
				$numero_page = 0;

			$tableau_global = array_slice ($tableau_global, $numero_page*15, 15, true);
/*
			AFFICHAGE DU TABLEAU GLOBAL TOUT EST DANS L'ORDRE 
			AMIS CONNECTES - AMIS DECONNECTES - MEMBRES CONNECTES - MEMBRES DECONNECTES
*/
			$i= 1; // LE $i DEMARRE A 1 DANS LE TABLEAU
			foreach($tableau_global as $affichage) 
			{
				// ON A DEJA FAIT TOUTES LES VERIFICATIONS, DONNEES CLASSEES
				// VERIFICATION DES CONNECTES POUR GERER AFFICHAGE DES BLOC
				$r1 = $bdd->prepare('SELECT * FROM connecte 
									WHERE identifiant = :identifiant')
									or die(print_r($bdd->errorInfo()));
				$r1->execute(array('identifiant' => $affichage['identifiant'])) 
									or die(print_r($bdd->errorInfo()));
				$donnees = $r1->fetch();			
				echo'
				<div class="bloc_joueur" id="'.$affichage['id'].'">';
					// L'IDENTIFIANT RECUPERER EST CONNECTE
					if(isset($donnees['timestamp']) AND $donnees['timestamp'] != '')
					{
						echo'
						<div class="defi_connecte" title="Connecté">
							<img src="images/defi_connecte.png" alt="Connecté"/>
						</div>';
					}
					// L'IDENTIFIANT RECUPERE EST DECONNECTE
					else
					{
						echo'
						<div class="defi_connecte" title="Déconnecté">
							<img src="images/defi_deconnecte.png" alt="Déconnecté"/>
						</div>';
					}
					echo'
					<div class="defi_information1">';
						
						echo'
						<span class="centre_image30">';
						if(isset($affichage['photo_profil']) 
						AND $affichage['photo_profil'] != '' 
						AND $affichage['photo_profil'] != 0)
						{  
							$source = getimagesize('images_utilisateurs/'.$affichage['photo_profil']); 	// La photo est la source
							if ($source[0] <= 30 AND $source[1] <= 30)
								echo '<img src="images_utilisateurs/'.$affichage['photo_profil'].'" alt="Photo de profil" />';
							else
								echo '<img src="images_utilisateurs/mini_2_'.$affichage['photo_profil'].'" alt="Photo de profil" />';
						}
						else
							echo'<img src="images/image_defaut.png" alt="Image"/>';
					
					
					echo'
						</span>
						<p>
							'.$affichage['identifiant'].' </br >	
							<span style="color:#528661;font-weight:bolder;">'.$affichage['kp'].' kp</span>
						</p>
					</div>';
					
					if(isset($_SESSION['kp']) AND $_SESSION['kp'] >= 3)
					{
						echo'
						<a href="defi.php?defier&identifiant='.$affichage['identifiant'].'">
							<div class="defi_defier">
								<img src="images/defi_defier.png" alt="Défier" title="Défier"/>
							</div>
						</a>';
					}
					else
					{
						echo'
						<a href="#" class="defier" id="defier" >
							<div class="defi_defier">
								<img src="images/defi_defier.png" alt="Défier" title="Défier"/>
							</div>
						</a>';
					}
				echo'
				</div>';
				$i++;
			
			}
			/* 
				On compte tous les joueurs du site puisque dans défi ils 
				s'affichent tous (AVANT C'ETAIT JUSTE UN CLASSEMENT)
			*/
			$nbre_page = 1;
			$p = $bdd->prepare('SELECT COUNT(id) 
								AS nbr_joueur
								FROM membres 
								WHERE identifiant!=:identifiant')
								or die(print_r($bdd->errorInfo()));
			$p->execute(array('identifiant' => $_SESSION['identifiant']))
								or die(print_r($bdd->errorInfo()));
			while($do = $p->fetch())
			{
				$compteur = $do['nbr_joueur'];
			}

			
			$nbr_entrees = $compteur; //LE RESULTAT DE LA BOUCLE (AVEC ADDITION)
		
			if (isset ($_GET['page']))
				$current_page = $_GET['page'];
			else
				$current_page = 1;
			
			$nbr_affichage = 15;
			$nom_page = 'defi';
			
			echo'
			<div class="contient_pagination">';
				include('pagination.php');
			echo'
			</div>';
?>		
		</div>
<?php
	

	}
// --------------------  DEFI EN COURS ----------------------------------------/
	$tableau_cours = array(); // ON OUVRE UN TABLEAU POUR STOCKER
	$en_cours = 0;
	$compteur_defi = 0;
	$re_c6 = $bdd->prepare('SELECT * FROM defi 
							WHERE (identifiant_1=:id
							OR identifiant_2=:id)
							AND view != :id
							AND view != :all')
							or die(print_r($bdd->errorInfo()));
	$re_c6->execute(array('id' => $_SESSION['identifiant'],
						'all' => 'all'))
							or die(print_r($bdd->errorInfo()));
	while($donnees_c6 = $re_c6->fetch())
	{
		$tableau_cours[$en_cours] = $donnees_c6;
		$en_cours++;
		$compteur_defi++;
	}
?>	
	<div class="defi_bloc2">
		<p style="text-align:center;margin-top:8px;color:#528661;padding-bottom:21px;">
			Défi en cours <?php echo'('.$compteur_defi.')';?>
		</p>
<?php
	
	// ON EN AFFICHE QUE 5 array_slice pour couper
	$tableau_cours = array_slice ($tableau_cours, 0 , 4 , true);
	
	// LE FOREACH EST DANS LE INCLUDE AVEC L'AFFICHAGE 
	include('defi_cours.php'); //---------------------------------------------//
		
	if($compteur_defi >= 4)
	{
		echo'
		<a href="defi-all">
			<p id="defi_afficher1">
				Tout afficher
			</p>
		</a>';

	}
	echo'
	</div>';
	
// ---------- DEMANDE DE DEFI -------------------------------------------------/

	$compteur_demande=0;
	$r_compteur2 = $bdd->prepare('SELECT id FROM demande_defi 
							WHERE identifiant_to=:id_to')
							or die(print_r($bdd->errorInfo()));
	$r_compteur2->execute(array('id_to' => $_SESSION['identifiant']))
							or die(print_r($bdd->errorInfo()));
	while($d_compteur2 = $r_compteur2->fetch())
	{
		$compteur_demande++;
	}
?>
	
	<div class="defi_bloc3">
		<p style="text-align:center;margin-top:8px;color:#448593;padding-bottom:30px;">
			Demande de défi <?php echo'('.$compteur_demande.')';?>
		</p>
<?php
		$re_c5 = $bdd->prepare('SELECT * FROM demande_defi 
								WHERE identifiant_to=:id_to
								LIMIT 0,4')
								or die(print_r($bdd->errorInfo()));
		$re_c5->execute(array('id_to' => $_SESSION['identifiant']))
								or die(print_r($bdd->errorInfo()));
		
		// LA BOUCLE EST DANS LE INCLUDE
		include('defi_demandes.php'); //--------------------------------------//
	
		if($compteur_demande >= 4)
		{
			echo'
			<a href="defi-all">
				<p id="defi_afficher2">
					Tout afficher
				</p>
			</a>';

		}

?>		
	
	
	
	</div>

	<div class="erreur" style="clear:right;height:10px;"></div>
	<div class="erreur" style="height:10px;"></div>
<?php
}
?>
</div>
</div>
<?php
include('footer.php');

// POUR VOIR CE QU'IL Y A DANS LES DIFFERENTS TABLEAUX ----------------------------

/*
echo '<pre style="color:black"> CONNECTE';
print_r($tableau_co);
echo '</pre>';

echo '<pre style="color:black"> DECONNECTE';
print_r($tableau_deco);
echo '</pre>';

/*
echo '<pre style="color:black"> ALL IDENTIFIANT';
print_r($tableau_identifiant);
echo '</pre>';

echo '<pre style="color:black"> EN COURS';
print_r($tableau_cours);
echo '</pre>';
/*
*/

?>
<script>
	var a = document.getElementsByTagName('a'),
	aTaille = a.length;

			for (var i = 0 ; i < aTaille ; i++) { 
				if (a[i] && a[i].className == 'defier')
				{	
					a[i].onclick = function() {
						document.getElementById('fond_overlay2').style.display = "block";
						document.getElementById('overlay_rouge2').style.display = "block";
						return false; // on bloque la redirection
					}; 
				}		
			}
			
	document.getElementById('fermer_overlay').onclick = function() {
		document.getElementById('fond_overlay2').style.display = "none";
		document.getElementById('overlay_rouge2').style.display = "none";
		return false; // on bloque la redirection
	};

var recherche = document.getElementById('defi_recherche'),
	selectedResult = -1, // Permet de savoir quel résultat est sélectionné : -1 signifie « aucune sélection »
    previousValue = recherche.value; // On fait de même avec la précédente valeur
var resultat = document.createElement('div');
resultat.id = "resultat_recherche";
var bloc = document.getElementById('bloc_gauche');
bloc.appendChild(resultat);

	recherche.onkeyup = function(e) { 
	
		e = e || window.event; // On n'oublie pas la compatibilité pour IE
		
		function getResults(keywords)
		{
			// On lance la requête ajax
			$.getJSON('recherche_defi_ajax.php?recherche='+keywords, function(data) {
					
				$("#resultat_recherche").empty();
				$("#resultat_recherche").html(data['messages']);
			});		
		}

        if (recherche.value != previousValue) { // Si le contenu du champ de recherche a changé

            previousValue = recherche.value;
			previousValue = previousValue.replace(/^\s*|\s*$/g,"");
			if (previousValue != '')
				getResults(previousValue); // On stocke la nouvelle requête

            selectedResult = -1; // On remet la sélection à zéro à chaque caractère écrit

        }
		
		var div = document.getElementsByTagName('div'),
		divTaille = div.length;
		previousValue = previousValue.replace(/^\s*|\s*$/g,"");
		for (var i = 0 ; i < divTaille ; i++) {
			if ((div[i] && div[i].className == 'bloc_joueur' 
			|| div[i].className =='contient_pagination') && previousValue != '')
				div[i].style.display = 'none';
			else if ((div[i] && div[i].className == 'bloc_joueur' 
			|| div[i].className =='contient_pagination') && !previousValue)
			{
				div[i].style.display = 'block';	
				$("#resultat_recherche").empty();
			}
		}
			
		

	};			



</script>
</body>

</html>