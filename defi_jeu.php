<?php
$acces = 'on';

include('header.php');


if ((isset($_GET['id_defi']) AND (isset($_GET['id1']) OR isset($_GET['id2']))))
{
	$re = $bdd->prepare('SELECT * FROM defi 
						WHERE id=:id')
						or die(print_r($bdd->errorInfo()));
	$re->execute(array('id' => $_GET['id_defi']))
						or die(print_r($bdd->errorInfo()));
	$donnees = $re->fetch();
	if (isset($_GET['id1']) AND $donnees['identifiant_1'] == $_SESSION['identifiant'])
	{
		$_SESSION['id_defi'] = $_GET['id_defi'];
		$_SESSION['identifiant_defi'] = 'id1';
	}
		// on créer session pour pouvoir enregister ds traitement_defi
	elseif (isset($_GET['id2']) AND $donnees['identifiant_2'] == $_SESSION['identifiant'])
	{
		$_SESSION['id_defi'] = $_GET['id_defi'];
		$_SESSION['identifiant_defi'] = 'id2';
	}
	else
		header('Location: defi.php');
}
	
	
if (isset($_GET['onglet']))
{
	$req1 = $bdd->prepare('DELETE FROM traitement_defi 
						WHERE numero_partie=:num')
						or die(print_r($bdd->errorInfo()));
	$req1->execute(array('num' => $_SESSION['numero_partie'])) 
						or die(print_r($bdd->errorInfo()));
	$req1->closeCursor(); // Termine le traitement de la requête
	header('Location: defi.php');
}

if (isset($_GET['debut_partie'],$_SESSION['id_defi'],$_SESSION['identifiant_defi']))
{
	// ouverture du fichier numero_partie
	$monfichier = fopen('numero_partie.txt', 'r+');				 
	$numero = fgets($monfichier); 
	$numero++; 
	fseek($monfichier, 0); 
	fputs($monfichier,$numero); 
	fclose($monfichier);
	$_SESSION['numero_partie'] = $numero;
	
		
    $milliseconds = round(microtime(true) * 1000);
	$re_c2 = $bdd->prepare('INSERT INTO traitement_defi 
						(id_defi,identifiant,temps_debut_jeu,numero_partie) 
						VALUES(:id_defi,:identifiant,:debut_jeu,:numero_partie)')
						or die(print_r($bdd->errorInfo()));
	$re_c2->execute(array('id_defi' => $_SESSION['id_defi'],
						'identifiant' => $_SESSION['identifiant_defi'], 
						'debut_jeu' => $milliseconds,
						'numero_partie' => $_SESSION['numero_partie']))
						or die(print_r($bdd->errorInfo()));
}
if (isset($_GET['fin_partie'],$_GET['temps'],
	$_SESSION['numero_partie'],$_SESSION['id_defi'],$_SESSION['identifiant_defi']))
{
	$re_c3 = $bdd->prepare('SELECT temps_debut_jeu FROM traitement_defi 
							WHERE numero_partie=:num')
							or die(print_r($bdd->errorInfo()));
	$re_c3->execute(array('num' => $_SESSION['numero_partie']))
							or die(print_r($bdd->errorInfo()));
	$donnees_c3 = $re_c3->fetch();
	if (isset($donnees_c3['temps_debut_jeu']))
	{
		$milliseconds = round(microtime(true) * 1000);
		$temps = ($milliseconds - $donnees_c3['temps_debut_jeu'])/1000;
		if ($temps < $_GET['temps'] + 2 AND $temps > $_GET['temps'] - 2)
		{
			if (!isset($_GET['score']))
				$score = 0;
			else
				$score = $_GET['score'];
				
			$re_c2 = $bdd->prepare('UPDATE traitement_defi SET 
									temps_fin_jeu=:fin_jeu,
									temps=:temps,score=:score 
									WHERE numero_partie=:num')
									or die(print_r($bdd->errorInfo()));
			$re_c2->execute(array('num' => $_SESSION['numero_partie'], 
									'temps' => $temps,
									'fin_jeu' => $milliseconds,
									'score' => $score))
									or die(print_r($bdd->errorInfo()));
			
			header('Location: defi_jeu.php?fin&id_defi='.$_SESSION['id_defi'].'&'.$_SESSION['identifiant_defi']);		
		}
		else
		{	
			$req1 = $bdd->prepare('DELETE FROM traitement_defi 
						WHERE numero_partie=:num')
						or die(print_r($bdd->errorInfo()));
			$req1->execute(array('num' => $_SESSION['numero_partie'])) 
						or die(print_r($bdd->errorInfo()));
			$req1->closeCursor(); // Termine le traitement de la requête
			header('Location: defi_jeu.php?chargement
			&id_defi='.$_SESSION['id_defi'].'&'.$_SESSION['identifiant_defi']); // chargement trop long
		}
	}
}
if(isset($_GET['fin']))
{
	if (isset ($_SESSION['numero_partie']))
	{
		echo'
		<div id="fond_overlay" style="display:block;"></div>
		<div class="overlay_fin">';
			$re_c4 = $bdd->prepare('SELECT * FROM traitement_defi 
									WHERE numero_partie=:numero_partie')
									or die(print_r($bdd->errorInfo()));
			$re_c4->execute(array('numero_partie' => $_SESSION['numero_partie']))
									or die(print_r($bdd->errorInfo()));
			$donnees_c4 = $re_c4->fetch();
			if (isset($donnees_c4['temps'],$donnees_c4['score']))
			{
				
				if(isset($donnees['jeu']) AND $donnees['jeu'] == 'esquive')
				{
					echo' 

					Fin Partie
					<img class="main" style="top:13px;"src="images/bad.png" alt=" "/>				
					
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					
					Temps:&nbsp;
					<span>
					'.$donnees_c4['temps'].' s
					</span>
					<img class="main" style="top:5px;"src="images/good.png" alt=" "/>';
				}
				elseif((isset($donnees['jeu']) AND $donnees['jeu'] == 'lettre')
				OR (isset($donnees['jeu']) AND $donnees['jeu'] == 'cible'))
				{
					echo' 

					Fin Partie
					<img class="main" style="top:13px;"src="images/bad.png" alt=" "/>
					
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					
					Score:&nbsp;
					<span>
					'.$donnees_c4['score'].'
					</span>
					
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					
					Temps:&nbsp;
					<span>
					'.$donnees_c4['temps'].' s
					</span>
					<img class="main" style="top:5px;"src="images/good.png" alt=" "/>';
				}
			}
		
		echo'
		</div>';
	}
}
?>
<div id="wrap">
<div class="corps">
<?php
// On récupere les informations de participant au défi ------------------------/

	if (isset($_GET['id1']))
	{
		$rq_autre = $bdd->prepare('SELECT photo_profil, identifiant FROM membres 
								WHERE identifiant=:identifiant')
								or die(print_r($bdd->errorInfo()));
		$rq_autre->execute(array('identifiant' => $donnees['identifiant_2']))
		or die(print_r($bdd->errorInfo()));
		$donnees_autre = $rq_autre->fetch();
		
		$_SESSION['identifiant_defi_autre'] = 'id2';	
	}
	elseif (isset($_GET['id2']))
	{
		$rq_autre = $bdd->prepare('SELECT photo_profil, identifiant FROM membres 
								WHERE identifiant=:identifiant')
								or die(print_r($bdd->errorInfo()));
		$rq_autre->execute(array('identifiant' => $donnees['identifiant_1']))
		or die(print_r($bdd->errorInfo()));
		$donnees_autre = $rq_autre->fetch();
		
		$_SESSION['identifiant_defi_autre'] = 'id1';
	}



//-----------------------------------------------------------------------------/
	echo'
	<div class="jeu_'.$donnees['jeu'].'" style="clear:left;">';
	
	$resu = $bdd->prepare('SELECT COUNT(*) AS nbr_partie FROM traitement_defi
						WHERE id_defi LIKE :id_defi AND identifiant LIKE :identifiant_defi')
						or die(print_r($bdd->errorInfo()));
	$resu->execute(array('id_defi' => $_SESSION['id_defi'].'%',
						'identifiant_defi' => $_SESSION['identifiant_defi'].'%'))
						or die(print_r($bdd->errorInfo()));		
	$resultat = $resu->fetch();	
	if(isset($resultat['nbr_partie']))
	{
		$restant = 20 - $resultat['nbr_partie'];
	}
	else
	{
		$restant = 20;
	}
	if($restant > 0)
	{
		echo'
		<button class="haut_jeu_'.$donnees['jeu'].'" id="clique" >
			Jouer <span style="font-size:small;">('.$restant.')</span>
		</button>';
	}
	else
	{
		echo'
		<div class="haut_jeu_'.$donnees['jeu'].'" style="cursor:default;">
			Parties épuisées
		</div>';
	}
		
?> 
<?php
		echo'
		<div id="defi_jeu_identifiant1">
			<span class="centre_image30" style="float:left;">';
			
			if(isset($_SESSION['photo_profil']) 
			AND $_SESSION['photo_profil'] != '' 
			AND $_SESSION['photo_profil'] != 0)
			{  
				$source = getimagesize('images_utilisateurs/'.$_SESSION['photo_profil']); 	// La photo est la source
				if ($source[0] <= 30 AND $source[1] <= 30)
					echo '<img src="images_utilisateurs/'.$_SESSION['photo_profil'].'" alt="Photo de profil" />';
				else
					echo '<img src="images_utilisateurs/mini_2_'.$_SESSION['photo_profil'].'" alt="Photo de profil" />';
			}
			else
				echo'<img src="images/image_defaut.png" alt="Image"/>';
		
		
			echo'
			</span>
			
			<span class="defi_limitation_identifiant1">'.$_SESSION['identifiant'].'</span>
		</div>
		
		<img class="jeu_defi_eclair" src="images/eclair.png" alt="VS"/>
		
		<div id="defi_jeu_identifiant2">
			<span class="centre_image30" style="float:right;">';
			
			if(isset($donnees_autre['photo_profil']) 
			AND $donnees_autre['photo_profil'] != '' 
			AND $donnees_autre['photo_profil'] != 0)
			{  
				$source = getimagesize('images_utilisateurs/'.$donnees_autre['photo_profil']); 	// La photo est la source
				if ($source[0] <= 30 AND $source[1] <= 30)
					echo '<img src="images_utilisateurs/'.$donnees_autre['photo_profil'].'" alt="Photo de profil" />';
				else
					echo '<img src="images_utilisateurs/mini_2_'.$donnees_autre['photo_profil'].'" alt="Photo de profil" />';
			}
			else
				echo'<img src="images/image_defaut.png" alt="Image"/>';
		
		
			echo'
			</span>
			
			<span class="defi_limitation_identifiant2">'.$donnees_autre['identifiant'].'</span>
		</div>';
								
?> 
				
			<div class="defi_contient_affichage">
				<table class="defi_affichage_score1">
<?php
				$i1 = 1;
				$aucun1 = 0;
				$r_score = $bdd->prepare('SELECT * FROM traitement_defi 
									WHERE id_defi=:id_defi 
									AND identifiant =:identifiant
									ORDER BY score DESC,temps DESC LIMIT 0,7')
									or die(print_r($bdd->errorInfo()));
				$r_score->execute(array('id_defi' => $_SESSION['id_defi'],
										'identifiant' => $_SESSION['identifiant_defi']))
									or die(print_r($bdd->errorInfo()));
				while($d_score = $r_score->fetch())
				{
					// CONCERNANT AFFICHAGE SCORE JEU ESQUIVE AVEC SESSION
					if ($donnees['jeu'] == 'esquive')
					{
						if ($d_score['identifiant'] == $_SESSION['identifiant_defi'])
						{
							echo '
							<tr>';
							if($i1==1)
							{
								echo'
								<td>
									<img src="images/decompte_bleu.png" alt="Temps"/>
								</td>
								<td style="text-align:right;color:#448593;">
									'.$d_score['temps'].' s
								</td>
							</tr>';
							}
							else
							{
								echo'
								<td>
									<img src="images/decompte_jeu.png" alt="Temps"/>
								</td>
								<td style="text-align:right;">
									'.$d_score['temps'].' s
								</td>
							</tr>';
							}
						}
						$i1++;
						$aucun1++;
					}
					// AFFICHAGE DES AUTRE JEUX : CIBLE ET LETTRE
					else
					{
						if ($d_score['identifiant'] == $_SESSION['identifiant_defi'])
						{
							echo '
							<tr>';
							if($i1==1)
							{	
								if($donnees['jeu'] == 'lettre')
								{
									echo'
									<td style="text-align:left;width:100px;color:#ca5151">
										'.$d_score['score'].'
									</td>
									<td style="text-align:right;color:#ca5151;font-size:x-small;font-weight:bolder;float:right;width:55px;">
										'.$d_score['temps'].' s
									</td>';
								}
								elseif($donnees['jeu'] == 'cible')
								{
									echo'
									<td style="text-align:left;width:100px;color:#528661">
										'.$d_score['score'].'
									</td>
									<td style="text-align:right;color:#528661;font-size:x-small;font-weight:bolder;float:right;width:55px;">
										'.$d_score['temps'].' s
									</td>';
								}
							echo'
							</tr>';
							}
							else
							{
								echo'
								<td style="text-align:left;width:100px;1">
									'.$d_score['score'].'
								</td>
								<td style="float:right;width:55px;text-align:right;font-size:x-small;font-weight:bolder;">
									'.$d_score['temps'].' s
								</td>
							</tr>';
							}
						}
						$i1++;
						$aucun1++;
					}
				}

				if($aucun1 == 0)
				{
					echo '
					<tr>
						<td style="text-align:center;">
							Aucun score enregistré. 
						</td>
					</tr>';
				}			
?>
				</table> 
				
				<table class="defi_affichage_score2">
<?php
				$i2 = 1;
				$aucun2 = 0;
				$r_score = $bdd->prepare('SELECT * FROM traitement_defi 
									WHERE id_defi=:id_defi 
									AND identifiant =:identifiant
									ORDER BY score DESC,temps DESC LIMIT 0,7')
									or die(print_r($bdd->errorInfo()));
				$r_score->execute(array('id_defi' => $_SESSION['id_defi'],
										'identifiant' => $_SESSION['identifiant_defi_autre']))
									or die(print_r($bdd->errorInfo()));
				while($d_score = $r_score->fetch())
				{
					// CONCERNANT AFFICHAGE SCORE JEU ESQUIVE AVEC L'AUTRE
					if ($donnees['jeu'] == 'esquive')
					{
						if ($d_score['identifiant'] ==  $_SESSION['identifiant_defi_autre'])
						{
							echo '
							<tr>';
							if($i2==1)
							{
								echo'
								<td>
									<img src="images/decompte_bleu.png" alt="Temps"/>
								</td>
								<td style="text-align:right;color:#448593;">
									'.$d_score['temps'].' s
								</td>
							</tr>';
							}
							else
							{
								echo'
								<td>
									<img src="images/decompte_jeu.png" alt="Temps"/>
								</td>
								<td style="text-align:right;">
									'.$d_score['temps'].' s
								</td>
							</tr>';
							}
						}
						$i2++;
						$aucun2++;
					}
					// AFFICHAGE DES AUTRE JEUX : CIBLE ET LETTRE
					else
					{
						if ($d_score['identifiant'] ==  $_SESSION['identifiant_defi_autre'])
						{
							echo '
							<tr>';
							if($i2==1)
							{	
								if($donnees['jeu'] == 'lettre')
								{
									echo'
									<td style="text-align:left;width:100px;color:#ca5151">
										'.$d_score['score'].'
									</td>
									<td style="text-align:right;color:#ca5151;font-size:x-small;font-weight:bolder;float:right;width:55px;">
										'.$d_score['temps'].' s
									</td>';
								}
								elseif($donnees['jeu'] == 'cible')
								{
									echo'
									<td style="text-align:left;width:100px;color:#528661">
										'.$d_score['score'].'
									</td>
									<td style="text-align:right;color:#528661;font-size:x-small;font-weight:bolder;float:right;width:55px;">
										'.$d_score['temps'].' s
									</td>';
								}
							echo'
							</tr>';
							}
							else
							{
								echo'
								<td style="text-align:left;width:100px;1">
									'.$d_score['score'].'
								</td>
								<td style="float:right;width:55px;text-align:right;font-size:x-small;font-weight:bolder;">
									'.$d_score['temps'].' s
								</td>
							</tr>';
							}
						}
						$i2++;
						$aucun2++;
					}
				}

				if($aucun2 == 0)
				{
					echo '
					<tr>
						<td style="text-align:center;">
							Aucun score enregistré. 
						</td>
					</tr>';
				}		
?>
				</table> 
			</div>  
<?php
			echo'
			<div id="jeu_argent">
				Votre mise - &nbsp;&nbsp;&nbsp; '.$donnees['somme'].' Kp
			</div>';

	if ($donnees['jeu'] == 'esquive')
		echo'
		<div class="bas_score" style="border:none;" id="bas_score">
			<img src="images/information_image1.png" alt="Météorites"/>
			<p> Esquivez les météorites le plus longtemps possible</p>
		</div>';
	elseif ($donnees['jeu'] == 'lettre')
		echo'
		<div class="bas_score" style="border:none;" id="bas_score">
			<img src="images/information_image3.png" alt="Météorites"/>
			<p> A vos claviers. N\'en laissez pas passer plus de 3</p>
		</div>';
	elseif ($donnees['jeu'] == 'cible')
		echo'
		<div class="bas_score" style="border:none;" id="bas_score">
			<img src="images/information_image2.png" alt="Cercles"/>
			<p> Cliquez au centre pour faire disparaitre les cercles </p>
		</div>';
?>
	</div>
<?php
	
	if(isset($_GET['fin']))
	{
		echo'
		<canvas style="opacity:0.3;" class="canvas_defi_'.$donnees['jeu'].'" id="canvas" height="450px" width="640px">';
	}
	else
	{
		echo'
		<canvas class="canvas_defi_'.$donnees['jeu'].'" id="canvas" height="450px" width="640px">';
	}
?>
			<p>	
				Désolé, votre navigateur ne supporte pas Canvas. 
				Mettez-vous à jour
			</p>
		</canvas>
			
<?php
	if(!isset($_GET['fin']))
	{
		echo'
		<div id="jeu_defi_intro">
			<p style="float:left;margin-top:3px;margin-left:15px;">
				Vous êtes en défi contre &nbsp;&nbsp;&nbsp;
				<span class="centre_image30">';
				if(isset($donnees_autre['photo_profil']) 
				AND $donnees_autre['photo_profil'] != '' 
				AND $donnees_autre['photo_profil'] != 0)
				{  
					$source = getimagesize('images_utilisateurs/'.$donnees_autre['photo_profil']); 	// La photo est la source
					if ($source[0] <= 30 AND $source[1] <= 30)
						echo '<img src="images_utilisateurs/'.$donnees_autre['photo_profil'].'" alt="Photo de profil" />';
					else
						echo '<img src="images_utilisateurs/mini_2_'.$donnees_autre['photo_profil'].'" alt="Photo de profil" />';
				}
				else
					echo'<img src="images/image_defaut.png" alt="Image"/>';
			
			
				echo'
				</span>
				<span style="color:#4f4f4f;">
					'.$donnees_autre['identifiant'].'
				</span>
			</p>';
			
			
			$temps_restant = (3600*48 - (time() - $donnees['debut_defi']));
			$heures = ceil($temps_restant / 3600);
			$min = ($temps_restant % 3600)/60 ;
			$sec = (($temps_restant % 3600) % 60);
			
			$total= $donnees['somme'] * 2;
			echo'
			<p id="fin_defi">
				Fin du défi <br />
					'.(ceil($heures) - 1).' : '.(ceil($min) - 1).' : '.(ceil($sec) - 1).'
			</p>
			<p id="somme_defi">
				'.$total.' <br />
				Kp
				
			</p>';
		
		echo'
		</div>';
	}
		echo'
		<div id="jeu_defi_pied" style="height:62px;clear:right;clear:left;"></div>';
?>
</div>
</div>
<?php
include('footer.php');
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script> var defi = 'on'; </script> <!-- pour que l'on sache qu'on est sur defi -->
<?php
echo '<script type="text/javascript" src="javascript/jeu_'.$donnees['jeu'].'.js"></script>';
?>
</body>
</html>