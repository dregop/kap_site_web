	var lettre = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']; //65-90
	var cercleCouleur = [
	'rgba(134, 196, 209, 0.8)',
	'rgba(137, 207, 157, 0.8)',
	'rgba(247, 211, 84, 0.8)',
	'rgba(222, 151, 111, 0.8)',
	'rgba(255, 126, 126, 0.8)'];
	var tailleCercle2 = Math.floor((Math.random() * 30) + 20);
	var tailleCercle3 = Math.floor((Math.random() * 30) + 20);
	var tailleCercle4 = Math.floor((Math.random() * 30) + 20);
	var lettreGeometrique = ['a','k','m','o','y'];
	var numeroLettreGeometrique = ['65','75','77','79','89'];
	
	
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
      window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
      window.cancelAnimationFrame = window[vendors[x] + 'CancelAnimationFrame']
      || window[vendors[x] + 'CancelRequestAnimationFrame'];
    }
     
	var chance = 3;	 
	var score = 0;
	var compteur = 0;
    var requestAnimId;
	var lettreBonus = 0;
	
    var canvasTerrainContext;
	var canvas  = document.querySelector('#canvas');
	var canvasTerrainContext = canvas.getContext('2d');
	
    var terrainLongueur = 640;
    var terrainLargeur = 450;
    var couleurTerrain = "#000000";
	var dessinerTerrain = function() {
		canvasTerrainContext.font = "18px Arial,Calibri,Geneva";
		canvasTerrainContext.fillStyle = "white";
		canvasTerrainContext.fillText(chance + ' vies', terrainLongueur - 60, 25);
		canvasTerrainContext.fillText('score : ' + score, terrainLongueur - 355, 25);
		canvasTerrainContext.fillText(compteur, 75, 25);
    } 
	var dessinerDecompte = function() {
		var decompte= new Image();
		decompte.src = 'images/decompte_jeu.png';
		decompte.onload = function() {
					dessinerCercle2();
					dessinerCercle3();
					dessinerCercle4();
					dessinerLettreAlea();
					dessinerLettreAlea2();
					dessinerLettreAlea3();
					dessinerCercle();
			canvasTerrainContext.fillStyle = "#ca5151";
			canvasTerrainContext.fillRect(0, 0,terrainLongueur, 35);
			canvasTerrainContext.drawImage(decompte, 40, 5); 
		}
	}
	var couleurCercle = "rgba(134, 196, 209, 0)";
    var dessinerCercle = function() {
	canvasTerrainContext.strokeStyle = couleurCercle;
	canvasTerrainContext.beginPath(); // La bouche, un arc de cercle
	canvasTerrainContext.arc(positionXlettreGeo + 12, positionYlettreGeo + 12 , 30, 0, Math.PI*2); // Ici aussi
	canvasTerrainContext.stroke();
		canvasTerrainContext.font = "20px Arial,Calibri,Geneva";
		canvasTerrainContext.fillStyle = couleurCercle;
		canvasTerrainContext.fillText('+50', positionXlettreGeo + 50, positionYlettreGeo);
    } 
         
	var positionXLettre = Math.floor((Math.random() * 600) + 1);
	var positionYLettre = -50;	
	var i = Math.floor((Math.random() * 25) + 1);
	var couleur1 = Math.floor((Math.random() * 5));
	var positionXLettre2 = Math.floor((Math.random() * 600) + 1);
	var positionYLettre2 = -50;	
	var j = Math.floor((Math.random() * 25) + 1);
	var couleur2 = Math.floor((Math.random() * 5));
	var positionXLettre3 = Math.floor((Math.random() * 600) + 1);
	var positionYLettre3 = -50;	
	var k = Math.floor((Math.random() * 25) + 1);
	var couleur3 = Math.floor((Math.random() * 5));
	
	//faire tomber les caisses alÃ©atoires
	var positionXlettreGeo = Math.floor((Math.random() * 600) + 1);
	var positionYlettreGeo = -50;	
	var h = Math.floor((Math.random() * 4) + 1);
	
	var dessinerForme = function() {
		var lettreGeo = new Image();
			lettreGeo.src = 'images/lettre_'+lettreGeometrique[h]+'.png';
			lettreGeo.onload = function() {
				canvasTerrainContext.clearRect ( 0, 0 , terrainLongueur , terrainLargeur ); 
				canvasTerrainContext.drawImage(lettreGeo, positionXlettreGeo, positionYlettreGeo); 
			}     
    }	
    var dessinerCercle2 = function() {
		canvasTerrainContext.fillStyle = cercleCouleur[couleur1];;
		canvasTerrainContext.beginPath(); // La bouche, un arc de cercle
		canvasTerrainContext.arc(positionXLettre + 10, positionYLettre - 8, tailleCercle2, 0, Math.PI*2); // Ici aussi
		canvasTerrainContext.fill();
    } 
    var dessinerLettreAlea = function() {
				canvasTerrainContext.font = "25px Arial,Calibri,Geneva";
				canvasTerrainContext.fillStyle = "white";
				canvasTerrainContext.fillText(lettre[i], positionXLettre, positionYLettre);       
    }
    var dessinerCercle3 = function() {
		canvasTerrainContext.fillStyle = cercleCouleur[couleur2];
		canvasTerrainContext.beginPath(); // La bouche, un arc de cercle
		canvasTerrainContext.arc(positionXLettre2 + 10, positionYLettre2 - 8, tailleCercle3, 0, Math.PI*2); // Ici aussi
		canvasTerrainContext.fill();
    } 	
	var dessinerLettreAlea2 = function() {
					canvasTerrainContext.font = "25px Arial,Calibri,Geneva";
					canvasTerrainContext.fillStyle = "#white";
					canvasTerrainContext.fillText(lettre[j], positionXLettre2, positionYLettre2);       
	}
    var dessinerCercle4 = function() {
		canvasTerrainContext.fillStyle = cercleCouleur[couleur3];
		canvasTerrainContext.beginPath(); // La bouche, un arc de cercle
		canvasTerrainContext.arc(positionXLettre3 + 10, positionYLettre3 - 8, tailleCercle4, 0, Math.PI*2); // Ici aussi
		canvasTerrainContext.fill();
    } 
	var dessinerLettreAlea3 = function() {
					canvasTerrainContext.font = "25px Arial,Calibri,Geneva";
					canvasTerrainContext.fillStyle = "#white";
					canvasTerrainContext.fillText(lettre[k], positionXLettre3, positionYLettre3);       
	}
	
		
    var animerLettre = function() {
		if (positionYlettreGeo > terrainLargeur && compteur % 10 == 0)
		{
			positionYlettreGeo = -20;
			positionXlettreGeo = Math.floor((Math.random() * 600) + 1);
			h = Math.floor((Math.random() * 4) + 1);
			lettreBonus = 0;
		}
		if (positionYLettre > terrainLargeur)
		{
			positionYLettre = 0;
			positionXLettre = Math.floor((Math.random() * 600) + 1);
			i = Math.floor((Math.random() * 25) + 1);
			chance--;
			tailleCercle2 = Math.floor((Math.random() * 30) + 20);
			couleur1 = Math.floor((Math.random() * 5));
		}
		if (positionYLettre2 > terrainLargeur)
		{
			positionYLettre2 = 0;
			positionXLettre2 = Math.floor((Math.random() * 600) + 1);
			j = Math.floor((Math.random() * 25) + 1);
			chance--;
			tailleCercle3 = Math.floor((Math.random() * 30) + 20);
			couleur2 = Math.floor((Math.random() * 5));
		}
		if (positionYLettre2 > terrainLargeur)
		{
			positionYLettre3 = 0;
			positionXLettre3 = Math.floor((Math.random() * 600) + 1);
			k = Math.floor((Math.random() * 25) + 1);
			chance--;
			tailleCercle4 = Math.floor((Math.random() * 30) + 20);
			couleur3 = Math.floor((Math.random() * 5));
		}		

// Vitesse de déplacement des lettres		
		positionYLettre+=1;			
		
		if (compteur > 3)
			positionYLettre+=1;
			positionYLettre2+=2;
		
		if (compteur > 10)
			positionYlettreGeo+=4;
			
		if (compteur > 13)
			positionYLettre3+=2;
			
		if (compteur > 18) {
			positionYLettre+=0.5;
			positionYLettre2+=0.5;
			positionYLettre3+=0.5;
		}
		
		if (compteur > 25) {
			positionYLettre+=0.5;
			positionYLettre2+=0.5;
			positionYLettre3+=0.5;
			positionYlettreGeo+=1;
		}
		
		if (compteur > 35) {
			positionYLettre+=1;
			positionYLettre2+=1;
			positionYLettre3+=1;
			positionYlettreGeo+=1;
		}
		
		if (compteur > 45) {
			positionYLettre+=1.5;
			positionYLettre2+=1.5;
			positionYLettre3+=1.5;
			positionYlettreGeo+=1;
		}	
			
    }	
 
  
    var initialisation = function() {
	compteur = 0;
	score = 0;
	chance = 3;
setInterval(function() { 
compteur++;
}
,1000);
      // le code de l'initialisation  
	  dessinerForme();	  
	  dessinerTerrain();
	  dessinerDecompte();
           
      requestAnimId = window.requestAnimationFrame(principale); // premier appel de principale au rafraichissement de la page
    }
	
var principale = function() {

(function() { // si l'onglet est changé pendant le jeu, on quitte le jeu
    var hidden = "hidden";

    // Standards:
    if (hidden in document)
        document.addEventListener("visibilitychange", onchange);
    else if ((hidden = "mozHidden") in document)
        document.addEventListener("mozvisibilitychange", onchange);
    else if ((hidden = "webkitHidden") in document)
        document.addEventListener("webkitvisibilitychange", onchange);
    else if ((hidden = "msHidden") in document)
        document.addEventListener("msvisibilitychange", onchange);
    // IE 9 and lower:
    else if ('onfocusin' in document)
        document.onfocusin = document.onfocusout = onchange;
    // All others:
    else
        window.onpageshow = window.onpagehide 
            = window.onfocus = window.onblur = onchange;

    function onchange (evt) {
        var v = 'visible', h = 'hidden',
            evtMap = { 
                focus:v, focusin:v, pageshow:v, blur:h, focusout:h, pagehide:h 
            };

        evt = evt || window.event;
        if (evt.type in evtMap)
            document.body.className = evtMap[evt.type];
        else
		{
			alert('Merci de na pas changer d\'onglet pendant une partie');
			if (defi == 'off')
			{
			$.get('traitement_lettre.php?onglet', function() { });
			document.location.href="lettre-onglet";
			}
			else
				document.location.href="defi_jeu.php?onglet";	
		}
    }
})();

	// le code du jeu
	  animerLettre();
	  dessinerForme();	
	  dessinerTerrain();
	  dessinerDecompte();
	  
	// IL Y A SESSION TOURNOI OU SESSION ENTRAINEMENT DONC PAS BESOIN DE GERER LETTRE
	if (chance != 0)
		requestAnimId = window.requestAnimationFrame(principale); // rappel de principale au prochain rafraichissement de la page
	else  
	{
		if (defi == 'off')
		{
			document.location.href="lettre.php?fin_partie&score="+score+"&temps="+compteur;
		}
		else
			document.location.href="defi_jeu.php?fin_partie&score="+score+"&temps="+compteur;
	}
}
	
	var onKeyDown = function(event) {	
	if (event.keyCode == numeroLettreGeometrique[h] && positionYlettreGeo > 0 && positionYlettreGeo < terrainLargeur && lettreBonus == 0)
	{
			lettreBonus = 1;
			score+= 50;	
		couleurCercle = "#528661";
		setTimeout(function() {
			couleurCercle = "#c8e3cc";
		}, 1000);	
	}
        if (event.keyCode - 65 == i) 
		{
			positionYLettre = 0;
			positionXLettre = Math.floor((Math.random() * 600) + 1);
			i = Math.floor((Math.random() * 25) + 1);	
			score+= 10;
			tailleCercle2 = Math.floor((Math.random() * 30) + 20);
			couleur1 = Math.floor((Math.random() * 5));
		}
        else if (event.keyCode - 65 == j) 
		{
			positionYLettre2 = 0;
			positionXLettre2 = Math.floor((Math.random() * 600) + 1);
			j = Math.floor((Math.random() * 25) + 1);	
			score+= 10;
			tailleCercle3 = Math.floor((Math.random() * 30) + 20);
			couleur2 = Math.floor((Math.random() * 5));
		}
        else if (event.keyCode - 65 == k) 
		{
			positionYLettre3 = 0;
			positionXLettre3 = Math.floor((Math.random() * 600) + 1);
			k = Math.floor((Math.random() * 25) + 1);	
			score+= 10;
			tailleCercle4 = Math.floor((Math.random() * 30) + 20);
			couleur3 = Math.floor((Math.random() * 5));
		}
		else
			score-= 5;
	}
	
	// association des mÃ©thodes aux Ã©vÃ¨nements :
    // onKeyDown = Ã  l'appui de la touche
    // onKeyUp = au relÃ¨vement de la touche
    window.onkeydown = onKeyDown;
	
	var clique = document.getElementById('clique');
	clique.onclick = function() {
		initialisation(); // appel de la fonction initialisation au chargement de la page
		var remplace = document.createElement('div');
			remplace.id = "en_cours";
		var remplaceText = document.createTextNode("En cours");
			remplace.appendChild(remplaceText);
		clique.parentNode.replaceChild(remplace, clique);
		if(document.getElementById('jeu_defi_intro'))
		{
			jeu_defi_intro.style.display = "none";
		}
		if(document.getElementById('tournoi_kp'))
		{
			tournoi_kp.style.display = "none";
		}
		remplace.style.backgroundColor = "#ca5151";
		remplace.style.paddingTop = "7px";
		remplace.style.paddingBottom = "7px";
		remplace.style.textAlign = "center";
		remplace.style.color = "white";
		remplace.style.fontSize = "large";
		var canvas  = document.querySelector('#canvas');
		canvas.style.backgroundColor = "#dedede";
		canvas.style.backgroundImage = "none";	
		
		if (tournoi == 'on' && defi == 'off' && entrainement == 'off') // C'est tournoi
			$.get('traitement_lettre.php?debut_partie&tournoi', function() { });
		else if (defi == 'on' && tournoi == 'off' && entrainement == 'off' ) // C'est defi
			$.get('defi_jeu.php?debut_partie', function() { });	
		else if (entrainement == 'on' && tournoi == 'off' && defi == 'off' ) // C'est entrainement
			$.get('traitement_lettre.php?debut_partie', function() { });
		
		canvas.style.opacity = "1";
		fin_partie.style.display = "none";
	};