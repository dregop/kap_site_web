<?php
// REQUETES POUR AVOIR SON NOMBRE TOTAL DE PARTIES ---------------------------//

$r1 = $bdd->prepare('SELECT COUNT(id) AS nombre FROM traitement_esquive
					WHERE identifiant = :identifiant')
					or die(print_r($bdd->errorInfo()));
$r1->execute(array('identifiant' => $_SESSION['identifiant']))
					or die(print_r($bdd->errorInfo()));		
$esquive = $r1->fetch();	

$r2 = $bdd->prepare('SELECT COUNT(id) AS nombre FROM traitement_lettre
					WHERE identifiant = :identifiant')
					or die(print_r($bdd->errorInfo()));
$r2->execute(array('identifiant' => $_SESSION['identifiant']))
					or die(print_r($bdd->errorInfo()));		
$lettre = $r2->fetch();

$r3 = $bdd->prepare('SELECT COUNT(id) AS nombre FROM traitement_cible
					WHERE identifiant = :identifiant')
					or die(print_r($bdd->errorInfo()));
$r3->execute(array('identifiant' => $_SESSION['identifiant']))
					or die(print_r($bdd->errorInfo()));		
$cible = $r3->fetch();

$total = $esquive['nombre'] + $lettre['nombre'] + $cible['nombre'];

//----------------------------------------------------------------------------//

$requete = $bdd->prepare('SELECT * FROM membres 
						WHERE identifiant = :identifiant')
						or die(print_r($bdd->errorInfo()));
$requete->execute(array('identifiant' => $_SESSION['identifiant']))
						or die(print_r($bdd->errorInfo()));
$donnees = $requete->fetch();

// TEMPS TOTALE PASSE SUR LES JEUX -------------------------------------------//
$temps_esquive = 0;
$temps_lettre = 0;
$temps_cible = 0;
$r_temps1 = $bdd->prepare('SELECT temps FROM traitement_esquive
						WHERE identifiant = :identifiant')
						or die(print_r($bdd->errorInfo()));
$r_temps1->execute(array('identifiant' => $_SESSION['identifiant']))
						or die(print_r($bdd->errorInfo()));
while($d_temps1 = $r_temps1->fetch())
{
	$temps_esquive = $temps_esquive + $d_temps1['temps'];
}
$r_temps2 = $bdd->prepare('SELECT temps FROM traitement_lettre
						WHERE identifiant = :identifiant')
						or die(print_r($bdd->errorInfo()));
$r_temps2->execute(array('identifiant' => $_SESSION['identifiant']))
						or die(print_r($bdd->errorInfo()));
while($d_temps2 = $r_temps2->fetch())
{
	$temps_lettre = $temps_lettre + $d_temps2['temps'];
}
$r_temps3 = $bdd->prepare('SELECT temps FROM traitement_cible
						WHERE identifiant = :identifiant')
						or die(print_r($bdd->errorInfo()));
$r_temps3->execute(array('identifiant' => $_SESSION['identifiant']))
						or die(print_r($bdd->errorInfo()));
while($d_temps3 = $r_temps3->fetch())
{
	$temps_cible = $temps_cible + $d_temps3['temps'];
}

$temps_total = $temps_esquive + $temps_lettre + $temps_cible;	

// NOMBRE DE POINTS TOTAUX ---------------------------------------------------//
$point_lettre = 0;
$r_point1 = $bdd->prepare('SELECT score FROM traitement_lettre
						WHERE identifiant = :identifiant')
						or die(print_r($bdd->errorInfo()));
$r_point1->execute(array('identifiant' => $_SESSION['identifiant']))
						or die(print_r($bdd->errorInfo()));
while($d_point1 = $r_point1->fetch())
{
	$point_lettre = $point_lettre + $d_point1['score'];
}
$point_cible = 0;
$r_point2 = $bdd->prepare('SELECT score FROM traitement_cible
						WHERE identifiant = :identifiant')
						or die(print_r($bdd->errorInfo()));
$r_point2->execute(array('identifiant' => $_SESSION['identifiant']))
						or die(print_r($bdd->errorInfo()));
while($d_point2 = $r_point2->fetch())
{
	$point_cible = $point_cible + $d_point2['score'];
}

$point_total = $point_lettre + $point_cible;	


echo'
<div class="fond_container" style="padding:0px;">
	<div id="index_profil_top">
		<div id="index_bloc1"> 

			<form action="profil_post.php" method="post" enctype="multipart/form-data">';	
				if(isset($donnees['photo_profil']) AND $donnees['photo_profil'] != '' AND $donnees['photo_profil'] != 0)
				{  
					echo'
					<div class="centre_image120">';
							
					$source = getimagesize('images_utilisateurs/'.$donnees['photo_profil']); 	// La photo est la source
					if ($source[0] <= 150 AND $source[1] <= 150)
						echo '<img src="images_utilisateurs/'.$donnees['photo_profil'].'" alt="Photo de profil" />';
					else
						echo '<img src="images_utilisateurs/mini_1_'.$donnees['photo_profil'].'" alt="Photo de profil" />';
						
					echo'
					</div>';
				}
				else
					echo '<img class="image_defaut_profil" src="images/image_defaut_grd.png" alt="Profil"/>';
			echo'
			</form>
			<div id="index_information_profil">
				<span title="'.$_SESSION['identifiant'].'">'.$_SESSION['identifiant'].'</span>
			</div>
			
		</div>
		
		<div id="index_bloc2">
			<div class="sous_bloc2 fond_bleu">
				&nbsp;&nbsp;&nbsp;&nbsp;Classement :';
				echo'
				<span>';
				
					if(isset($classement_esquive)){echo $classement_esquive;}
					else echo' 0 '; // SI AUCUN CLASSEMENT ON MET 0
						
				echo'
				</span>
					<img src="images/icone_esquive.png" alt="Esquive"/>

				<span>';
				
					if(isset($classement_lettre)){echo $classement_lettre;}
					else echo' 0 '; // SI AUCUN CLASSEMENT ON MET 0
						
				echo'
				</span>
					<img src="images/icone_lettre.png" alt="Lettre"/>

				<span>';
				
					if(isset($classement_cible)){echo $classement_cible;}
					else echo' 0 '; // SI AUCUN CLASSEMENT ON MET 0
						
				echo'
				</span>
					<img src="images/icone_cible.png" alt="Cible"/>

			</div>
			<div class="sous_bloc2" id="border_top">';
				
				// BLOC AFFICHAGE DES AMIS
				$compteur_amis = 0;
				$r_compteur = $bdd->prepare('SELECT ami_to, ami_from FROM amis 
									WHERE ami_from=:ami_from AND ami_confirm =:ami_confirm 
									OR ami_to=:ami_to AND ami_confirm =:ami_confirm
									ORDER BY RAND()')
									or die(print_r($bdd->errorInfo()));
				$r_compteur->execute(array('ami_from' => $_SESSION['id_membre'],
									'ami_confirm' => 1,
									'ami_to' => $_SESSION['id_membre'],))
									or die(print_r($bdd->errorInfo()));
				while ($d_compteur = $r_compteur->fetch())
				{
					$compteur_amis++;
				}
				
				$r_id = $bdd->prepare('SELECT ami_to, ami_from FROM amis 
								WHERE ami_from=:ami_from AND ami_confirm =:ami_confirm 
								OR ami_to=:ami_to AND ami_confirm =:ami_confirm
								ORDER BY RAND() LIMIT 0,7')
								or die(print_r($bdd->errorInfo()));
				$r_id->execute(array(
									// SESSION a fait la demande
									'ami_from' => $_SESSION['id_membre'],
									'ami_confirm' => 1,
									// On a fait la demande à SESSION
									'ami_to' => $_SESSION['id_membre'],))
									or die(print_r($bdd->errorInfo()));
				while ($d_id = $r_id->fetch())
				{
					// SESSION = AMI_TO L'INVITATION VIENT DE L'AMI
					if(isset($d_id['ami_from']) AND $d_id['ami_from'] != $_SESSION['id_membre'])
					{
						$r_ami = $bdd->prepare('SELECT photo_profil, id, identifiant
												FROM membres 
												WHERE id=:id')
										or die(print_r($bdd->errorInfo()));
						$r_ami->execute(array('id' => $d_id['ami_from']))
										or die(print_r($bdd->errorInfo()));
						$d_ami = $r_ami->fetch();
					}
					// SESSION = AMI_TO L'INVITATION VIENT DE SESSION
					elseif(isset($d_id['ami_to']) AND $d_id['ami_to'] != $_SESSION['id_membre'])
					{
						$r_ami = $bdd->prepare('SELECT photo_profil, id, identifiant
												FROM membres 
												WHERE id=:id')
										or die(print_r($bdd->errorInfo()));
						$r_ami->execute(array('id' => $d_id['ami_to']))
										or die(print_r($bdd->errorInfo()));
						$d_ami = $r_ami->fetch();
					}
					
					echo'
					<a href="'.$d_ami['identifiant'].'" title="'.$d_ami['identifiant'].'">';
					
					if(isset($d_ami['photo_profil']) 
					AND $d_ami['photo_profil'] != ''
					AND $d_ami['photo_profil'] != 0)
					{  
						echo'
						<div class="centre_image45 index_img_amis">';
						
						$source = getimagesize('images_utilisateurs/'.$d_ami['photo_profil']); 	// La photo est la source
						if ($source[0] <= 30 AND $source[1] <= 30)
							echo '<img src="images_utilisateurs/'.$d_ami['photo_profil'].'" alt="Photo de profil" />';
						else
							echo '<img src="images_utilisateurs/mini_2_'.$d_ami['photo_profil'].'" alt="Photo de profil" />';
						
						echo'
						</div>';
					}
					else
						echo'<img class="index_img_amis" src="images/image_defaut.png" alt="Image"/>';
					
					echo'
					</a>';
				
				}
				
				if($compteur_amis == 0)
				{
					echo'
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Invitez vos amis et ajoutez les';
				}
			
			echo'
			</div>			
		</div>		
		<div id="index_bloc3" class="fond_bleu">
			<span>'.$total.'</span> partie'; if($total > 1){echo's';}	
		echo'
		</div>		
		<div id="index_bloc4">
			<span>'.$compteur_amis.'</span> ami'; if($compteur_amis > 1){echo's';}	
		echo'
		</div>
		<div id="index_bloc5">
			<span style="margin-left:40px;">Crédits: <span class="bolder"> '.$_SESSION['kp'].' Kp </span></span>
			
			<span style="margin-left:50px;">Gains: <span class="bolder"> 206 € </span></span>
			
			<span style="margin-left:50px;">Demandes Défi: <span class="bolder"> 4 </span></span>
		
		</div>
	</div>
</div>

<div class="contener_980">

	<div class="session_jeux" style="clear:left;" id="session_esquive">
		<p class="nom_jeux">Esquive <span>('.$esquive['nombre'].')</span></p>
		<img class="menu_img" src="images/index_score_esquive.png" alt="Esquive"/>';
		
		// AFFICHAGE DES SCORE POUR ESQUIVE
		echo'
		<div class="index_menu_score">
			<div class="profil_score">
				<table>';

				$e = 1;
				$r_esquive = $bdd->prepare('SELECT * FROM traitement_esquive 
									WHERE identifiant=:identifiant 
									ORDER BY temps DESC,temps DESC LIMIT 0,10')
									or die(print_r($bdd->errorInfo()));
				$r_esquive->execute(array('identifiant' => $_SESSION['identifiant']))
									or die(print_r($bdd->errorInfo()));
				while($d_esquive = $r_esquive->fetch())
				{
					echo '
					<tr>';
					if($e==1)
					{
						echo'
						<td class="grand">
							<img src="images/sablier_marron.png" alt="Temps"/>
							<span>
								'.$d_esquive['temps'].' s
							</span>
						</td>
					</tr>
					<tr>
						<td class="padding">

						</td>
					</tr>';
					}
					else
					{
						echo'
						<td class="petit">
							<span>
								'.$d_esquive['temps'].' s
							</span>
						</td>
					</tr>';
					}
				   $e++;
			   }
			   if ($e == 1)
				echo '<span class="aucun_index">Aucun temps enregistré</span>';
			
			echo'
				</table> 
			</div>';
			
			// Affichage des classements pour ESQUIVE
			echo'
			<p>Classement</p>';
			include('classement_profil_temps.php');

		echo'
		</div>
			
	</div>
	
	<div class="session_jeux" id="session_lettre">
		<p class="nom_jeux">Lettre <span>('.$lettre['nombre'].')</span></p>
		<img class="menu_img" src="images/index_score_lettre.png" alt="Lettre"/>';
		
		// AFFICHAGE DES SCORE POUR LETTRE
		echo'	
		<div class="index_menu_score">
			<div class="profil_score">
				<table>';

				$l = 1;
				$r_lettre = $bdd->prepare('SELECT * FROM traitement_lettre
									WHERE identifiant=:identifiant 
									ORDER BY score DESC,score DESC LIMIT 0,10')
									or die(print_r($bdd->errorInfo()));
				$r_lettre->execute(array('identifiant' => $_SESSION['identifiant']))
									or die(print_r($bdd->errorInfo()));
				while($d_lettre = $r_lettre->fetch())
				{
					echo '
					<tr>';
					if($l==1)
					{
						echo'
						<td class="grand">
							<img src="images/score_marron.png" alt="Temps"/>
							<span>
								'.$d_lettre['score'].' pts
							</span>
						</td>
					</tr>
					<tr>
						<td class="padding">

						</td>
					</tr>';
					}
					else
					{
						echo'
						<td class="petit">
							<span>
								'.$d_lettre['score'].' pts
							</span>
						</td>
					</tr>';
					}
				   $l++;
			   }
			   if ($l == 1)
				echo '<span class="aucun_index">Aucun score enregistré</span>';
			
			echo'
				</table> 
			</div>';
			
			// Affichage des classements pour LETTRE
			echo'
			<p>Classement</p>
			<table class="affichage_classement_profil">';

			$classement_jeu = 'meilleur_lettre';
			$i = 0;
			$tableau_score = '';
			$j = 1;
			$nbre = 0;
			$score = $bdd->query('SELECT * FROM '.$classement_jeu.' 
									ORDER BY score DESC')
									or die(print_r($bdd->errorInfo()));
			while ($donnees = $score->fetch())
			{	
				if ($donnees['identifiant'] == $_SESSION['identifiant'])
				{
					// On a 2 cas pour une page. 
					// On ne peut pas verifier avec $classement 
					$verification1 = 1;
					$classement = $j;
					$nbre = $classement - 4;
				}
					
				$j++;
			}
			if(isset($verification1) AND $verification1 == 1)
			{
				include('classement_profil_score.php');
			}
			else
			{
				echo '
				<tr>
					<td style="border:none;text-align:center;">
						Aucun score enregistré. 
					</td>
				</tr>
				<tr>
					<td style="border:none;text-align:center;">
						Vous ne pouvez pas être classé...
					</td>
				</tr>
				<tr>
					<td style="border:none;padding-top:30px;">
						Lancez une partie.
					</td>
				</tr>';
			}
			echo'
			</table>
			
		</div>	
	</div>
	
	<div class="session_jeux" id="session_cible">
		<p class="nom_jeux">Cible <span>('.$cible['nombre'].')</span></p>
		<img class="menu_img" src="images/index_score_cible.png" alt="Cible"/>';
		
		// AFFICHAGE DES SCORES POUR CIBLE
		echo'
		<div class="index_menu_score" id="index_score_cible">
			<div class="profil_score">
				<table>';

				$c = 1;
				$r_cible = $bdd->prepare('SELECT * FROM traitement_cible
									WHERE identifiant=:identifiant 
									ORDER BY score DESC,score DESC LIMIT 0,10')
									or die(print_r($bdd->errorInfo()));
				$r_cible->execute(array('identifiant' => $_SESSION['identifiant']))
									or die(print_r($bdd->errorInfo()));
				while($d_cible = $r_cible->fetch())
				{
					echo '
					<tr>';
					if($c==1)
					{
						echo'
						<td class="grand">
							<img src="images/score_marron.png" alt="Temps"/>
							<span>
								'.$d_cible['score'].' pts
							</span>
						</td>
					</tr>
					<tr>
						<td class="padding">

						</td>
					</tr>';
					}
					else
					{
						echo'
						<td class="petit">
							<span>
								'.$d_cible['score'].' pts
							</span>
						</td>
					</tr>';
					}
				   $c++;
			   }
			   if ($c == 1)
				echo '<span class="aucun_index">Aucun score enregistré</span>';
			
			echo'
				</table> 
			</div>';
			
			// Affichage des classements pour CIBLE
			echo'
			<p>Classement</p>
			<table class="affichage_classement_profil">';
			
				$classement_jeu = 'meilleur_cible';
				$i = 0;
				$tableau_score = '';
				$j = 1;
				$nbre = 0;
				$score = $bdd->query('SELECT * FROM '.$classement_jeu.' 
										ORDER BY score DESC')
										or die(print_r($bdd->errorInfo()));
				while ($donnees = $score->fetch())
				{	
					if ($donnees['identifiant'] == $_SESSION['identifiant'])
					{
						// On a 2 cas pour une page. 
						// On ne peut pas verifier avec $classement 
						$verification2 = 1;
						$classement = $j;
						$nbre = $classement - 4;
					}
						
					$j++;
				}
				if(isset($verification2) AND $verification2 == 1)
				{
					include('classement_profil_score.php');
				}
				else
				{
					echo '
					<tr>
						<td style="border:none;text-align:center;">
							Aucun score enregistré. 
						</td>
					</tr>
					<tr>
						<td style="border:none;text-align:center;">
							Vous ne pouvez pas être classé...
						</td>
					</tr>
					<tr>
						<td style="border:none;padding-top:30px;">
							Lancez une partie.
						</td>
					</tr>';
				}
			echo'
			</table>
			
		</div>
		
		
		
		
		
		
		
		
		
		
		
		
		
	</div>

	<div style="width:980px;height:1px;clear:left;"></div>

</div>



';