<?php
$acces = 'on';

include('header.php');
// ON INCLUDE L'OVERLAY POUR LES FIN DE PARTIES
include('fin_partie.php');

if(isset($_GET['entrainement']))
{
	$_SESSION['entrainement_lettre'] = 'on';
}
elseif(isset($_GET['tournoi']))
{
	$_SESSION['tournoi_lettre'] = 'on';
}

if (isset($_GET['fin_partie'],$_GET['temps'],$_GET['score'],$_SESSION['numero_partie']))
{
	// ON CREE UNE VARIABLE TABLE POUR SAVOIR DANS QUEL TABLE ON FAIT LA REQUETE
	if(isset($_SESSION['tournoi_lettre'])
	AND $_SESSION['tournoi_lettre'] == 'on')
	{
		// C'EST LE TOURNOI
		$table = 'tournoi_lettre';
		$table_meilleur = 'meilleur_tournoi_lettre';
	}
	else
	{
		// C'EST L'ENTRAINEMENT
		$table = 'traitement_lettre';
		$table_meilleur = 'meilleur_lettre';
	}
	
	$re_c3 = $bdd->prepare('SELECT temps_debut_jeu FROM '.$table.' 
							WHERE numero_partie=:num')
							or die(print_r($bdd->errorInfo()));
	$re_c3->execute(array('num' => $_SESSION['numero_partie']))
							or die(print_r($bdd->errorInfo()));
	$donnees_c3 = $re_c3->fetch();
	if (isset($donnees_c3['temps_debut_jeu']))
	{
		$milliseconds = round(microtime(true) * 1000);
		$temps = ($milliseconds - $donnees_c3['temps_debut_jeu'])/1000;
		if ($temps < $_GET['temps'] + 3 AND $temps > $_GET['temps'] - 3)
		{
			$re_c2 = $bdd->prepare('UPDATE '.$table.' 
									SET temps_fin_jeu=:fin_jeu,temps=:temps,
									score=:score WHERE numero_partie=:num')
									or die(print_r($bdd->errorInfo()));
			$re_c2->execute(array('num' => $_SESSION['numero_partie'], 'temps' => $temps,
									'fin_jeu' => $milliseconds,'score' => $_GET['score']))
									or die(print_r($bdd->errorInfo()));
									
			if(isset($_SESSION['identifiant']))
			{
				$re_c4 = $bdd->prepare('SELECT score,temps FROM '.$table_meilleur.'
										WHERE identifiant=:identifiant')
										or die(print_r($bdd->errorInfo()));
				$re_c4->execute(array('identifiant' => $_SESSION['identifiant']))
										or die(print_r($bdd->errorInfo()));
				$donnees_c4 = $re_c4->fetch();
				
				if (!isset($donnees_c4['score']))
				{
					$re_c5 = $bdd->prepare('INSERT INTO '.$table_meilleur.' 
											(identifiant,temps,score,date) 
											VALUES(:identifiant,:temps,
											:score,NOW())')
											or die(print_r($bdd->errorInfo()));
					$re_c5->execute(array('identifiant' => $_SESSION['identifiant'], 
											'temps' => $temps,
											'score' => $_GET['score']))
											or die(print_r($bdd->errorInfo()));
				}
				else if (isset($donnees_c4['score'],$donnees_c4['temps']) 
				AND $donnees_c4['score'] != ''
				AND $donnees_c4['temps'] != '' AND $_GET['score'] > $donnees_c4['score'])
				{

					$re_c6 = $bdd->prepare('UPDATE '.$table_meilleur.' SET 
											temps=:temps,score=:score 
											WHERE identifiant=:identifiant')
											or die(print_r($bdd->errorInfo()));
					$re_c6->execute(array('identifiant' => $_SESSION['identifiant'], 
											'temps' => $temps,
											'score' => $_GET['score']))
											or die(print_r($bdd->errorInfo()));
				}
			}
			header('Location: lettre-fin');		
		}


	}
}

// ON DEFINI LES VARIABLE POUR classement_jeu.php
if(isset($_SESSION['tournoi_lettre']))
{
	// C'EST LE TOURNOI
	$classement_jeu = 'meilleur_tournoi_lettre';
}
else
{
	// C'EST L'ENTRAINEMENT
	$classement_jeu = 'meilleur_lettre';
}	
?>

<div id="fond_overlay"></div>
<div id="overlay_rouge">
	Il vous faut 1Kp pour participer au tournoi
</div>

<div id="wrap">
<div class="corps">
	<div class="jeu_lettre"  style="clear:left;">
<?php
	// C'EST LE TOURNOI ------------------------------------------------------//
	if(isset($_SESSION['tournoi_lettre'],$_SESSION['identifiant'])
	AND $_SESSION['tournoi_lettre'] =='on')
	{
		if(isset($_SESSION['kp']) AND $_SESSION['kp'] >=1)
		{
			echo'
			<button class="haut_jeu_lettre" id="clique"> 
				<img style="position:relative;top:4px;"src="images/play_blanc.png" alt=" "/>
				<span style="position:relative;bottom:2px;">
					Jouer <span style="font-size:small;">(1Kp)</span>
				</span>
			</button>';
		}
		else
		{
			echo'
			<button class="haut_jeu_lettre" id="insuffisant"> 
				<img style="position:relative;top:4px;"src="images/play_blanc.png" alt=" "/>
				<span style="position:relative;bottom:2px;">
					Jouer <span style="font-size:small;">(1Kp)</span>
				</span>
			</button>';
		}
		$i=0;
		$r_tournoi1 = $bdd->prepare('SELECT score, temps FROM tournoi_lettre 
								WHERE identifiant=:identifiant
								ORDER BY temps DESC')
								or die(print_r($bdd->errorInfo()));
		$r_tournoi1->execute(array('identifiant' => $_SESSION['identifiant']))
								or die(print_r($bdd->errorInfo()));
		while($d_tournoi1 = $r_tournoi1->fetch())
		{
			$i++;
			if($i ==1){ break; }
		}
		
		
		echo'
		<div id="tournoi_lettre_bloc1">';
		
			if(isset($d_tournoi1['score']))
			{
				echo'
				<p> 
					Mon score: 
					<span>'.$d_tournoi1['score'].'  </span>
					&nbsp;&nbsp;&nbsp;
					<span>'.$d_tournoi1['temps'].' s </span>
				</p>';
			}
			else
			{
				echo'
				<p> 
					Aucun score
				</p>';
			}
		
echo'
		</div>
		
		<div id="tournoi_lettre_bloc2">';
		
			include('classement_jeu.php');
		
		echo'
		</div>';
	}
	// C'EST L'ENTRAINEMENT --------------------------------------------------//
	else
	{
		echo'
		<button class="haut_jeu_lettre" id="clique" >
			<img style="position:relative;top:4px;"src="images/play_blanc.png" alt=" "/>
			<span style="position:relative;bottom:2px;">Jouer</span>
		</button>';
		if (isset ($_SESSION['identifiant']))
		{
?>
			<div class="score_classement1" id="score_classement1">
				<p style="float:left;font-weight:bolder;">
					<a id="score1" href="#">
						<span style="color:#ca5151;">Score</span> 
					</a>
				</p>  
				<p style="float:left;">
					<a id="classement1" href="#">
						&nbsp;&nbsp;&nbsp;&nbsp;Classement
					</a>
				</p>  
			</div>
			<div class="score_classement2" id="score_classement2" style="display:none;">
				<p style="float:left;font-weight:bolder;color:#528661;">
					<a id="score2" href="#">
						Score
					</a>
				</p>  
				<p style="float:left;">
					<a id="classement2" href="#">
						&nbsp;&nbsp;&nbsp;&nbsp;
						<span style="color:#ca5151;">
							Classement
						</span>
					</a>
				</p>   
			</div>
			<div class="contient_affichage" id="affichage_score">
				<table class="affichage_score">
	<?php
				$i = 1;
				$aucun = 0;
				$r_score= $bdd->prepare('SELECT * FROM traitement_lettre
									WHERE identifiant=:identifiant 
									ORDER BY score DESC,temps DESC LIMIT 0,6')
									or die(print_r($bdd->errorInfo()));
				$r_score->execute(array('identifiant' => $_SESSION['identifiant']))
									or die(print_r($bdd->errorInfo()));
				while($d_score = $r_score->fetch())
				{ 
					echo '
					<tr>';
					if($i==1)
					{
						echo'
						<td style="text-align:left;width:100px;color:#ca5151;">
							'.$d_score['score'].'
						</td>
						<td>
							<img src="images/decompte_rouge.png" alt="Temps"/>
						</td>
						<td style="color:#ca5151;">
							'.$d_score['temps'].' s
						</td>
					</tr>';
					}
					else
					{
						echo'
						<td style="text-align:left;">
							'.$d_score['score'].'
						</td>
						<td >
							<img src="images/decompte_jeu.png" alt="Temps"/>
						</td>
						<td>
							'.$d_score['temps'].' s
						</td>
					</tr>';
					}
					$i++;
					$aucun++;
			    }
				if($aucun == 0)
				{
					echo '
					<tr>
						<td style="text-align:left;padding-left:0px;">
							Aucun score enregistré. 
						</td>
					</tr>';
				}
?>
				</table> 
			</div>
<?php
			
			include('classement_jeu.php');
		}
		else
		{
			echo'
			<h4> Inscrivez-vous </h4>
			<div class="texte_inscription" id="texte_inscription">';
			
			include('inscription_all.php');
			
			echo'
					<p style="font-size:x-small;">
						</br >
						En validant, vous acceptez les
						<a style="color:#ca5151;" href="charte.html" id="conditions" >
							conditions générales d\'utilisation
						</a>
					</p>
					<input style="background-color:#ca5151;" type="submit" id="valider" name="valider" value="Valider" class="valider"/>
				</form>
			</div>';
			
		}
	}
	if(isset($_SESSION['identifiant']))
	{
		echo'
		<div class="bas_score" id="bas_score">
			<img src="images/information_image3.png" alt="Météorites"/>
			<p> A vos claviers. N\'en laissez pas passer plus de 3</p>
		</div>';
	}
?>
	</div>

<?php 
	if(isset($_SESSION['tournoi_lettre'],$_SESSION['identifiant'])
	AND $_SESSION['tournoi_lettre'] =='on')
	{
		echo'
		<canvas class="canvas_tournoi_lettre" id="canvas" height="450" width="640">';
	}
	else
	{
		echo'
		<canvas class="canvas_lettre" id="canvas" height="450" width="640">';
	}
?>
			<p>	
				Désolé, votre navigateur ne supporte pas Canvas. 
				Mettez-vous à jour
			</p>
		</canvas>
<?php
	if(isset($_SESSION['tournoi_lettre'],$_SESSION['identifiant'])
	AND $_SESSION['tournoi_lettre'] =='on')
	{
		// ON COMPTE LE NOMBRE DE KP DEPENSE DANS HEADER
		

		
		$total_kp = $mise_depart + $d_kp2['nombre_lettre']; // ON A LE TOTAL DE KP
		
		echo'
		<div id="tournoi_kp">
			<p>
				'.$total_kp.' <br />
				Kp
			</p>
		</div>';
	}
?>
</div>
</div>
<?php
//include('inscription_erreur.php');
include('footer.php');
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>

<?php
if(isset($_SESSION['tournoi_lettre']) AND $_SESSION['tournoi_lettre'] == 'on')
{
?>
	<script type="text/javascript">
	// C'EST TOURNOI
	// Avant toute chose on met la variable defi OFF pour être tranquil
		var defi = 'off';
		var tournoi = 'on';
		var entrainement ='off'
	</script>
<?php
}
else
{
?>
	<script type="text/javascript">
	// C'EST TOURNOI
	// Avant toute chose on met la variable defi OFF pour être tranquil
		var defi = 'off';
		var tournoi = 'off'; // C'est ENTRAINEMENT
		var entrainement ='on'
	</script>
<?php
}		
?>	
<script type="text/javascript" src="javascript/jeu_lettre.js"></script>
<script type="text/javascript" src="javascript/score_classement.js"></script>
<script>

	// ON GERE OVERLAY !	
	if (document.getElementById('overlay_fin'))
	{
		setTimeout(function() {
			document.getElementById('overlay_fin').style.display="none";
			document.getElementById('fond_overlay').style.display="none";
		}, 1500);
	}
	
	document.getElementById('insuffisant').onclick = function() 
	{
		overlay_rouge.style.display = 'block';
		fond_overlay.style.display = 'block';
		setTimeout(function() {
			document.getElementById('overlay_rouge').style.display="none";
			document.getElementById('fond_overlay').style.display="none";
		}, 1000);
		return false; // on bloque la redirection
	};	

</script>
</body>
</html>