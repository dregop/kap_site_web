<?php
session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arrête tout
    die('Erreur : '.$e->getMessage());
}

$req_identifiant = $bdd->prepare('SELECT identifiant FROM membres 
					WHERE identifiant=:identifiant')
					or die(print_r($bdd->errorInfo()));
$req_identifiant->execute(array('identifiant' => $_POST['identifiant']))
					or die(print_r($bdd->errorInfo()));
$donnees_identifiant = $req_identifiant->fetch();

$req_email = $bdd->prepare('SELECT email FROM membres 
					WHERE email=:email')
					or die(print_r($bdd->errorInfo()));
$req_email->execute(array('email' => $_POST['email']))
					or die(print_r($bdd->errorInfo()));
$donnees_email = $req_email->fetch();

if (isset($_POST['identifiant'],$_POST['password1'],$_POST['password2']
,$_POST['email']) AND $_POST['identifiant'] != '' 
AND $_POST['password1'] != '' AND $_POST['password2'] != '' 
AND $_POST['email'] != '')
{
	$_SESSION['inscription'] = 1;
	
	/*
		PAS BESOIN DE PROTEGER PAR L'URL, ELLE SERONT RENVOYEES EN POST
		DONC PROTEGEES JUSTE AU DESSUS ET LES GET SONT PROTEGES POUR AFFICHAGE
		
		PROTEGE POUR AFFICHER DES GET ET INSERTION DANS LA BDD
	*/
	if (!(preg_match('#^[A-Za-z0-9]{1,1000}$#',$_POST['identifiant'])))
	{
		$erreur2_nom_de_compte = 'e1';
		header('Location: accueil-'.$erreur2_nom_de_compte.','.urlencode(stripslashes(htmlspecialchars($_POST['email']))).'');
	}
	elseif ($donnees_email['email'] == $_POST['email']) 						// s'il y a déjà cette adresse mail ds la bdd 
	{
		$erreur_email1 = 'email1';
		header('Location: accueil-'.$erreur_email1.'-'.urlencode(stripslashes(htmlspecialchars($_POST['identifiant']))).'');
	}
	elseif (
	!(preg_match('#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#i',$_POST['email']
	))) // si l'email ne correspond pas à ce format
	{
		$erreur_email2 = 'email2';
		header('Location: accueil-'.$erreur_email2.'-'.urlencode(stripslashes(htmlspecialchars($_POST['identifiant']))).'');
	}
    elseif ($donnees_identifiant['identifiant'] == $_POST['identifiant']) 		// s'il y a déjà ce nom_de_compte ds la bdd 
	{
		$erreur_nom_de_compte = 'same';
		header('Location: accueil-'.$erreur_nom_de_compte.','.urlencode(stripslashes(htmlspecialchars($_POST['email']))).','.urlencode(stripslashes(htmlspecialchars($_POST['identifiant']))).'');
	}	
	elseif (strlen($_POST['identifiant']) <4) 									// si le nom de compte est inférieur à 4 caractères
	{
		$erreur3_nom_de_compte = 'e2';
		header('Location: accueil-'.$erreur3_nom_de_compte.','.urlencode(stripslashes(htmlspecialchars($_POST['email']))).','.urlencode(stripslashes(htmlspecialchars($_POST['identifiant']))).'');
	}
	elseif (strlen($_POST['identifiant']) >12) 									// si le nom de compte est supérieur à 30 caractères
	{
		$erreur4_nom_de_compte = 'e3';
		header('Location: accueil-'.$erreur4_nom_de_compte.','.urlencode(stripslashes(htmlspecialchars($_POST['email']))).','.urlencode(stripslashes(htmlspecialchars($_POST['identifiant']))).'');
	}
	elseif ($_POST['password1'] != $_POST['password2']) 						// si les deux mot de passe entrées sont différents
	{
		$erreur_mdp2 = 'diff';
		header('Location: accueil-'.$erreur_mdp2.','.urlencode(stripslashes(htmlspecialchars($_POST['email']))).','.urlencode(stripslashes(htmlspecialchars($_POST['identifiant']))).'');
	}
	elseif (!(preg_match('#^[A-Za-z0-9é&è@àç$*ù]{4,25}$#',$_POST['password1'])))	// si le mdp n'est pas conforme
	{
		$erreur_mdp = 'fail';
		header('Location: accueil-'.$erreur_mdp.','.urlencode(stripslashes(htmlspecialchars($_POST['email']))).','.urlencode(stripslashes(htmlspecialchars($_POST['identifiant']))).'');
	}
	elseif (!(isset($erreur_mdp2)) AND !(isset($erreur_email1)) AND !(isset($erreur_email2)) 
	AND !(isset($erreur2_nom_de_compte)) AND !(isset($erreur_nom_de_compte)) 
	AND !(isset($erreur_mdp)) AND !(isset($erreur3_nom_de_compte)) 
	AND !(isset($erreur4_nom_de_compte)))	
	{
		// si tout c'est bien passé
		$mdp_hache = sha1('qw' . $_POST['password1']); // on hache le mdp
		
		$req = $bdd->prepare('INSERT INTO membres(identifiant, password, 
							email,date) 
							VALUES(:identifiant, :password, :email,NOW())');
		$req->execute(array('identifiant' => $_POST['identifiant'],
							'password' => $mdp_hache,
							'email' => $_POST['email']));
		$req->closeCursor(); // Termine le traitement de la requète	
		
		$_SESSION['identifiant'] = $_POST['identifiant'];
		
		$r2 = $bdd->prepare('SELECT id FROM membres 
							WHERE identifiant=:identifiant')
							or die(print_r($bdd->errorInfo()));
		$r2->execute(array('identifiant' => $_SESSION['identifiant']))
							or die(print_r($bdd->errorInfo()));
		$donnees = $r2->fetch();

		$_SESSION['id_membre'] = $donnees['id'];
		
		unset($_SESSION['inscription']);
		header('Location: accueil'); 
	}
}
else
{
	$_SESSION['inscription'] = 1;
	
	$erreur5 = 'ncomplet';
	if(isset($_POST['identifiant'],$_POST['email']) AND
	(preg_match('#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#i',$_POST['email']))
	AND $_POST['identifiant'] == '' AND $_POST['email'] != '')
	{
		header('Location: accueil-i'.$erreur5.','.urlencode(stripslashes(htmlspecialchars($_POST['email']))).'');
	}
	elseif(isset($_POST['email'],$_POST['identifiant']) 
	AND $_POST['email'] != '' AND $_POST['identifiant'] != ''
	AND
	(preg_match('#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#i',$_POST['email'])))
	{
		header('Location: accueil-i'.$erreur5.','.urlencode(stripslashes(htmlspecialchars($_POST['email']))).','.urlencode(stripslashes(htmlspecialchars($_POST['identifiant']))).'');
	}
	elseif(isset($_POST['identifiant'])
	AND $_POST['identifiant'] != '')
	{
		header('Location: accueil-i'.$erreur5.'-'.urlencode(stripslashes(htmlspecialchars($_POST['identifiant']))).'');
	}
	else
	{
		header('Location: accueil-i'.$erreur5.'');
	}
}
?>