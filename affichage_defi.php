<?php

	// ON RECUPERE SES AMIS, ON STOCK DANS $tableau_amis ------//
	$a = 0;
	$tableau_amis = array();
	$tableau_identifiant = array();
	$r_id = $bdd->prepare('SELECT ami_to, ami_from, ami_date FROM amis 
						WHERE ami_from=:ami_from AND ami_confirm =:ami_confirm 
						OR ami_to=:ami_to AND ami_confirm =:ami_confirm')
						or die(print_r($bdd->errorInfo()));
	$r_id->execute(array(
						// SESSION a fait la demande
						'ami_from' => $_SESSION['id_membre'],
						'ami_confirm' => 1,
						// On a fait la demande à SESSION
						'ami_to' => $_SESSION['id_membre'],))
						or die(print_r($bdd->errorInfo()));
	while ($d_id = $r_id->fetch())
	{
		// SESSION = AMI_TO L'INVITATION VIENT DE L'AMI (INFOS AMIS)
		if(isset($d_id['ami_from']) AND $d_id['ami_from'] != $_SESSION['id_membre'])
		{
			$r_ami = $bdd->prepare('SELECT identifiant, kp
									FROM membres 
									WHERE id=:id')
							or die(print_r($bdd->errorInfo()));
			$r_ami->execute(array('id' => $d_id['ami_from']))
							or die(print_r($bdd->errorInfo()));
			$d_ami = $r_ami->fetch();

			// ON STOCK L'ID DE SES AMIS ET LES KP POUR CLASSER
			$tableau_amis[$a] = $d_ami;
			
			$tableau_identifiant[$a] = $d_ami['identifiant'];
			$a++;

		}
		// SESSION = AMI_TO L'INVITATION VIENT DE SESSION (INFOS AMIS)
		elseif(isset($d_id['ami_to']) AND $d_id['ami_to'] != $_SESSION['id_membre'])
		{
			$r_ami = $bdd->prepare('SELECT identifiant, kp
									FROM membres 
									WHERE id=:id')
							or die(print_r($bdd->errorInfo()));
			$r_ami->execute(array('id' => $d_id['ami_to']))
							or die(print_r($bdd->errorInfo()));
			$d_ami = $r_ami->fetch();
			
			// ON STOCK L'ID DE SES AMIS
			$tableau_amis[$a] = $d_ami;
			
			$tableau_identifiant[$a] = $d_ami['identifiant'];
			$a++;
			
		}
	}
	
	if(!empty($tableau_amis)) // IL FAUT QU'IL AIT DES AMIS
	{
		// MAINTENANT ON VA CLASSER LES ID AMIS EN FONCTION CO OU PAS CO
		$classement1=0;
		$classement2=0;
		$tableau_co = $tableau_deco = $tri_kp = array();

		foreach ($tableau_amis as $donnees){

		  $tri_kp[] = $donnees['kp'];
		  
		}
		// ON CLASSE EN FONCTION DES KP le tableau_amis ! ON EST TRANQUILE
		array_multisort($tri_kp, SORT_DESC, $tableau_amis);

		/*
		echo '<pre style="color:black"> AMIS';
		print_r($tableau_amis);
		echo '</pre>';
		*/
		
		foreach($tableau_amis as $element1) 
		{

			$r_1 = $bdd->prepare('SELECT * FROM connecte 
								WHERE identifiant = :identifiant')
								or die(print_r($bdd->errorInfo()));
			$r_1->execute(array('identifiant' => $element1['identifiant'])) 
								or die(print_r($bdd->errorInfo()));
			$donnees = $r_1 -> fetch();
			
			$temps1 = time() - 60*10;

			if(isset($donnees['identifiant']))
			// CET AMI EST CONNECTE ON LE STOCK DANS LES PREMIERS
			{
				if ($donnees['timestamp'] < $temps1)
				// VERIFICATION DU TEMPS DANS LA BDD (si inf�rieur)
				{
					$req_c = $bdd->prepare('DELETE FROM connecte 
											WHERE timestamp=:timestamp')
											or die(print_r($bdd->errorInfo()));
					$req_c->execute(array('timestamp' => $donnees['timestamp'])) 
											or die(print_r($bdd->errorInfo()));
				}
				else
				// AUCUN PROBLEME ON LE MET DANS LE TABLEAU
				{
					$tableau_co[$classement1] = $donnees['identifiant'];
					$classement1++;
				}
			}
			else
			// SI IL NE RENTRE PAS DANS LA BOUCLE -> AMI NON CO
			{
				$tableau_deco[$classement2] = $element1;
				$classement2++;
			}
			
			
		}
	}

	$i = 0;
	// MAINTENANT ON LES MET DANS UN TABLEAU GLOBAL DANS L'ORDRE -----//
	//----------------------------------------------------------------//
	$tableau_global = array();
	// EN PREMIER ON GERE LES AMIS CONNECTES -------------------------//
	if(!empty($tableau_co))
	{
		foreach($tableau_co as $element2) 
		{
			$r_global = $bdd->prepare('SELECT id,photo_profil,identifiant,kp 
									FROM membres 
									WHERE identifiant = :identifiant')
									or die(print_r($bdd->errorInfo()));
			$r_global->execute(array('identifiant' => $element2)) 
									or die(print_r($bdd->errorInfo()));
			while($information_ami = $r_global->fetch())
			{
				$tableau_global[$i]= $information_ami;
			}
			$i++; // LE $i NUMEROTTE LE JOUEUR EN PARTANT DE 0
		}
	}
	// EN DEUXIEME ON GERE LES AMIS DECONNECTES ----------------------//
	if(!empty($tableau_deco))
	{
		foreach($tableau_deco as $element3) 
		{
			$r_global = $bdd->prepare('SELECT id,photo_profil,identifiant,kp 
									FROM membres 
									WHERE identifiant = :identifiant')
									or die(print_r($bdd->errorInfo()));
			$r_global->execute(array('identifiant' => $element3['identifiant'])) 
									or die(print_r($bdd->errorInfo()));
			while($information_ami = $r_global->fetch())
			{
			
				$tableau_global[$i]= $information_ami;

			}
			$i++; // LE $i NUMEROTTE LE JOUEUR EN PARTANT DE 0
		}
	}
	
//----------------------------------------------------------------------------//
	// MAINTENANT ON STOCK LES MEMBRES CONNECTES DIFFERENTS DE CES AMIS ------//
	if(!empty($tableau_co)) 
	// !empty => DIFFERENT DE VIDE (le tableau existe toujours vu qu'il est cr��)
	{
		$fubar1 = 'identifiant != "'.$tableau_co[0].'" ';

		$limite_boucle1 = count($tableau_co)-1;
		// compter le nombre d'�l�ments dans le tableau pour obtenir l'index maximal
		// (soustraire 1 si l'index commence � 0)

		$i1 = 0; // si l'index commence � 0

		while ($i1 <= $limite_boucle1)
		{
			 $fubar1 = $fubar1.' AND identifiant != "'.$tableau_co[$i1].'" ';
			 
			 $i1++;
		}
		
		$r_co = $bdd->prepare('SELECT * FROM connecte 
							WHERE identifiant != :identifiant1
							AND '.$fubar1.'')
							or die(print_r($bdd->errorInfo()));
		$r_co->execute(array('identifiant1' => $_SESSION['identifiant'])) 
							or die(print_r($bdd->errorInfo()));

	}
	else
	{
		// TABLEAU CO N'EXISTE PAS ON NE VERIFIE PAS, ON PREND TOUS MEMBRES CO
		$r_co = $bdd->prepare('SELECT * FROM connecte 
							WHERE identifiant != :identifiant1')
							or die(print_r($bdd->errorInfo()));
		$r_co->execute(array('identifiant1' => $_SESSION['identifiant'])) 
							or die(print_r($bdd->errorInfo()));
	}
	
	$temps = time() - 60*10;	
	while($autre_co = $r_co->fetch())
	{
		if (isset($autre_co['timestamp']) AND $autre_co['timestamp'] < $temps)
		{
			$req_c = $bdd->prepare('DELETE FROM connecte 
									WHERE timestamp=:timestamp')
									or die(print_r($bdd->errorInfo()));
			$req_c->execute(array('timestamp' => $autre_co['timestamp'])) 
									or die(print_r($bdd->errorInfo()));
		}
		else
		{
			$r_global = $bdd->prepare('SELECT id,photo_profil,identifiant,kp 
									FROM membres 
									WHERE identifiant = :identifiant
									ORDER BY kp DESC') // CLASSEMENT PAR KP
									or die(print_r($bdd->errorInfo()));
			$r_global->execute(array('identifiant' => $autre_co['identifiant'])) 
									or die(print_r($bdd->errorInfo()));
			while($information_autre = $r_global->fetch())
			{
				$tableau_identifiant[$i] = $information_autre['identifiant'];
				$tableau_global[$i]= $information_autre;
			}
			$i++; // LE $i NUMEROTTE LE JOUEUR EN PARTANT DE 1
		}
		
		
	}

//----------------------------------------------------------------------------//
	// MAINTENANT ON STOCK LES MEMBRES DECONNECTES DIFFERENTS DE CES AMIS -----/
	// CLASSE PAR KP DECROISSANT ON AFFICHE CEUX QUI EN ONT LE PLUS
	if(!empty($tableau_identifiant))
	{
			$fubar2 = 'identifiant != "'.$tableau_identifiant[0].'" ';

			$limite_boucle2 = count($tableau_identifiant)-1;
			// compter le nombre d'�l�ments dans le tableau pour obtenir l'index maximal
			// (soustraire 1 si l'index commence � 0)

			$i2 = 0; // si l'index commence � 0

			while ($i2 <= $limite_boucle2)
			{
				 $fubar2 = $fubar2.' AND identifiant != "'.$tableau_identifiant[$i2].'" ';
				 
				 $i2++;
			}
			
			// ON PARCOURT LE TABLEAU DES AMIS DECONNECTES POUR NE PAS AFFICHER LES MEMES
			$r_deco = $bdd->prepare('SELECT id,photo_profil,identifiant,kp 
									FROM membres
									WHERE identifiant!=:identifiant1 AND '.$fubar2.'
									ORDER BY kp DESC') // CLASSEMENT PAR KP
									or die(print_r($bdd->errorInfo()));
			$r_deco->execute(array('identifiant1' => $_SESSION['identifiant']))
									or die(print_r($bdd->errorInfo()));
		
	}
	else
	{
		// TABLEAU DECO N'EXISTE PAS ON NE VERIFIE PAS - TOUS LES MEMBRES DECO - ET IL N A PAS D AMIS
		$r_deco = $bdd->prepare('SELECT id,photo_profil,identifiant,kp  
							FROM membres 
							WHERE identifiant != :identifiant1
							ORDER BY kp DESC') // CLASSEMENT PAR KP
							or die(print_r($bdd->errorInfo()));
		$r_deco->execute(array('identifiant1' => $_SESSION['identifiant'])) 
							or die(print_r($bdd->errorInfo()));

	}
	
	while($autre_deco = $r_deco->fetch())
	{
		$temps = time() - 60*10;	
		// ON VERIFIE TOUJOURS LE TEMPS DE CONNEXION
		$r_co2 = $bdd->prepare('SELECT * FROM connecte 
							WHERE identifiant = :identifiant1')
							or die(print_r($bdd->errorInfo()));
		$r_co2->execute(array('identifiant1' => $autre_deco['identifiant'])) 
							or die(print_r($bdd->errorInfo()));
		while($autre_co2 = $r_co2->fetch())
		{					
			if (isset($autre_co2['timestamp']) AND $autre_co2['timestamp'] < $temps)
			{
				$req_c2 = $bdd->prepare('DELETE FROM connecte 
										WHERE timestamp=:timestamp')
										or die(print_r($bdd->errorInfo()));
				$req_c2->execute(array('timestamp' => $autre_co2['timestamp'])) 
										or die(print_r($bdd->errorInfo()));
			}
			else
			{
				break;
			}
		}					
		$tableau_global[$i]= $autre_deco;
		$i++; // LE $i NUMEROTTE LE JOUEUR EN PARTANT DE 0
	}
	

?>