<?php
session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arrête tout
    die('Erreur : '.$e->getMessage());
}

// ON INSERT DANS LA BDD QUAND IL CLIQUE SUR AJOUTER
if(isset($_GET['ajouter']))
{
	$r1 = $bdd->prepare('INSERT INTO amis (ami_from, ami_to, 
						ami_confirm, ami_date) 
						VALUES(:ami_from, :ami_to, :ami_confirm ,NOW())');
	$r1->execute(array('ami_from' => $_SESSION['id_membre'],
						'ami_to' => $_GET['id_ami'],
						'ami_confirm' => 0));
	$r1->closeCursor(); // Termine le traitement de la requète	

	header('Location: '.urlencode(stripslashes(htmlspecialchars($_GET['identifiant']))).''); 
}

if(isset($_GET['accepter']))
{
	$r2 = $bdd->prepare('UPDATE amis SET ami_confirm=:ami_confirm
						WHERE ami_from=:ami_from AND ami_to=:ami_to');
	$r2->execute(array('ami_confirm' => 1,
						'ami_from' => $_GET['id_ami'],
						'ami_to' => $_SESSION['id_membre']))
						or die(print_r($bdd->errorInfo()));	
	$r2->closeCursor(); // Termine le traitement de la requête	
	
	header('Location: '.urlencode(stripslashes(htmlspecialchars($_GET['identifiant']))).''); 
}

if(isset($_GET['refuser']))
{
	$r3 = $bdd->prepare('DELETE FROM amis 
						WHERE ami_from=:ami_from AND ami_to=:ami_to');
	$r3->execute(array('ami_from' => $_GET['id_ami'],
						'ami_to' => $_SESSION['id_membre']))
						or die(print_r($bdd->errorInfo()));	
	$r3->closeCursor(); // Termine le traitement de la requête	
	
	header('Location: message'); 
}

