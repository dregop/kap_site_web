<?php
session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arrête tout
    die('Erreur : '.$e->getMessage());
}

if(isset($_GET['recherche']))
{
	$requete = $bdd->prepare('SELECT * FROM membres WHERE identifiant LIKE :recherche 
						ORDER BY identifiant LIMIT 0,20')
						or die(print_r($bdd->errorInfo()));
	$requete->execute(array('recherche' => $_GET['recherche'].'%'))
						or die(print_r($bdd->errorInfo()));
	$json['messages'] = '';	
	while ($donnees = $requete->fetch()) 
	{					
		// VERIFICATION DES CONNECTES POUR GERER AFFICHAGE DES BLOC
		$r1 = $bdd->prepare('SELECT * FROM connecte 
							WHERE identifiant = :identifiant')
							or die(print_r($bdd->errorInfo()));
		$r1->execute(array('identifiant' => $donnees['identifiant'])) 
		or die(print_r($bdd->errorInfo()));
		$donnees_c = $r1->fetch();	
		
		$json['messages'] .= '<div class="bloc_joueur" id="'.$donnees['id'].'">';
		
		// L'IDENTIFIANT RECUPERER EST CONNECTE
		if(isset($donnees_c['timestamp']) AND $donnees_c['timestamp'] != '')
		{
			$json['messages'] .= '
				<div class="defi_connecte" title="Connecté">
					<img src="images/defi_connecte.png" alt="Connecté"/>
				</div>';
		}
		// L'IDENTIFIANT RECUPERE EST DECONNECTE
		else
		{
			$json['messages'] .= '
				<div class="defi_connecte" title="Déconnecté">
					<img src="images/defi_deconnecte.png" alt="Déconnecté"/>
				</div>';
		}
		
		$json['messages'] .= '
			<div class="defi_information1">';
						
						$json['messages'] .= '
						<span class="centre_image30">';
						if(isset($donnees['photo_profil']) 
						AND $donnees['photo_profil'] != '' 
						AND $donnees['photo_profil'] != 0)
						{  
							$source = getimagesize('images_utilisateurs/'.$donnees['photo_profil']); 	// La photo est la source
							if ($source[0] <= 30 AND $source[1] <= 30)
								$json['messages'] .='<img src="images_utilisateurs/'.$donnees['photo_profil'].'" alt="Photo de profil" />';
							else
								$json['messages'] .='<img src="images_utilisateurs/mini_2_'.$donnees['photo_profil'].'" alt="Photo de profil" />';
						}
						else
							$json['messages'] .='<img src="images/image_defaut.png" alt="Image"/>';
					
					
					$json['messages'] .= 
						'</span>
						<p>
							'.stripslashes(htmlspecialchars($donnees['identifiant'])).' </br >	
							<span style="color:#528661;font-weight:bolder;">'.$donnees['kp'].' kp</span>
						</p>
					</div>';
					
					if(isset($_SESSION['kp']) AND $_SESSION['kp'] >= 3)
					{
						$json['messages'] .= 
						'<a href="defi.php?defier&identifiant='.urlencode(stripslashes(htmlspecialchars($donnees['identifiant']))).'">
							<div class="defi_defier">
								<img src="images/defi_defier.png" alt="Défier" title="Défier"/>
							</div>
						</a>';
					}
					else
					{
						$json['messages'] .= '
						<a href="#" class="defier" id="defier" >
							<div class="defi_defier">
								<img src="images/defi_defier.png" alt="Défier" title="Défier"/>
							</div>
						</a>';
					}
				$json['messages'] .= '
				</div>';
	}
	
	// Encodage de la variable tableau json et affichage
	echo json_encode($json);
}