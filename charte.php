<?php
$acces = 'on';
include('header.php');
?>
<div id="wrap">
    <div class="corps">
       <div id="corps_charte">	
           <p>
		        <strong>
					<span class="titre">
						Conditions générales d'utilisation
					</span>
				</strong>
				<br /><br />
				Une fois inscrit sur Gather Games, vous avez la possibilité de 
				vous exprimer sur différentes pages.
				<br />
				<ul>
				    <li>Vous êtes responsable et propriétaire de tout ce que 
						vous publiez sur celles-ci. 
						<br/>
                    <li>Gather Games ne pourra être tenu responsable des propos 
						publiés.
						<br/>
                    <li>Les propos tenus doivent être modérés, aussi il vous est 
						demandé de rester correct et courtois dans vos propos 
						vis-à-vis des autres utilisateurs.
						<br/>
                </ul>
		        <span class="titre">Sur notre site vous pouvez:</span><br />
                <ul>  
					<li>Partagez des photos, des vidéos ou des textes.<br />
                    <li>Ajouter des amis et discuter avec eux.<br />
                    <li>Voir l'actualité des jeux vidéos et celle de vos amis.
						<br />
                    <li>Créer la page de votre Team.<br />
                    <li>Faire partie d'un forum pour être membre d'une Team.
						<br />
                </ul>
                Ce site est évolutif, les services qui y sont proposés peuvent 
				changer à tout moment. 
				<br /><br />
				
                <strong>
					<span class="titre">
						Mot de passe et nom d'utilisateur
					</span></strong>
					<br /><br />
                <ul>
				    <li>Lors de la création de votre compte, vous avez choisi un
						mot de passe et un nom d'utilisateur dont vous serez 
						entièrement responsable. Votre compte et votre mot de 
						passe vous sont strictement personnel, ils ne doivent 
						pas être communiqués et doivent rester secrets...<br />
                    <li>Gather Games n'est pas responsable de la perte de votre 
						mot de passe ou de votre nom d'utilisateur ni de 
						l’utilisation qui pourrait être faite de votre compte.
						<br /><br /> 
                </ul>
                <strong>
					<span class="titre">
						Les droits de Gather Games
					</span>
				</strong>
				<br /><br />
                <ul>
				    <li>Gather Games avertira l'utilisateur d'un compte s'il ne 
						respecte pas les conditions générales d'utilisation.
						<br />
                    <li>Gather Games pourra supprimer un compte qui après avoir 
						reçu un avertissement ne respecte toujours pas les 
						conditions générales d'utilisation.
						<br />
                    <li>Gather Games stocke les données des comptes ainsi que 
						les informations personnelles s’y référant.
						<br />
                    <li>Il peut modifier et compléter la charte à tout moment.
						<br /><br />
                </ul>
				<strong>
					<span class="titre">
						Important: concernant vos données personnelles
					</span>
				</strong>
				<br /><br />
                <ul>
					<li>Pour vous proposer un meilleur service, Gather Games ne
					vous demande que vos données de localisation, c'est à dire 
					ville et pays, pour vous mettre en relation avec des joueurs
					près de chez vous.<br />
				    <li>Conformément à la loi n° 78-17 du 6 janvier 1978 
					relative à l'informatique, aux fichiers et aux libertés, 
					chaque membre de Gather Games dispose sur les données 
					personnelles le concernant d'un droit d'accès (art. 34 à 38 
					de la loi), de rectification (art. 36 de la loi) et 
					d'opposition (art. 26 de la loi), qu'il peut exercer auprès
					de Gather Games.<br /><br />
                </ul>
                 <span class="fin_charte">				 
					Vous êtes maintenant informé du fonctionnement du site, de ses 
					règles et de ce qu'il propose.
				 </span>
        
		</div>
   </div>
   <div class="erreur"></div>
</div>

<?php
include('footer.php');
?>

</body>

</html>