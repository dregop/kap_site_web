
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
      window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
      window.cancelAnimationFrame = window[vendors[x] + 'CancelAnimationFrame']
      || window[vendors[x] + 'CancelRequestAnimationFrame'];
    }	
  
    var requestAnimId;
    var canvasTerrainContext;
	var canvas  = document.querySelector('#canvas');
	var canvasTerrainContext = canvas.getContext('2d');
	var fin = 0;
	var compteur = 0;
	
    var terrainLongueur = 640;
    var terrainLargeur = 450;
    var filetLargeur = 6;
    var couleurTerrain = "#000000";
    var couleurFilet = "#000000";
	var dessinerTerrain = function() { 
			canvasTerrainContext.font = "18px Arial,Calibri,Geneva";
			canvasTerrainContext.fillStyle = "white";
			canvasTerrainContext.fillText('score : ' + score, terrainLongueur - 115, 25);
			canvasTerrainContext.fillText(compteur, 75, 25);
    }
	var dessinerDecompte = function() {
		var decompte= new Image();
		decompte.src = 'images/decompte_jeu.png';
		decompte.onload = function() {
			dessinerCercle();
			dessinerCercle2();
			dessinerCercle3();
			dessinerCercle4();
			dessinerCercle5();
			dessinerCercle6();
			dessinerCible3();
			dessinerCible4();
			dessinerCible5();
			dessinerCible6();
			dessinerCible();
			dessinerCible2();
			canvasTerrainContext.fillStyle = "#528661";
			canvasTerrainContext.fillRect(0, 0,terrainLongueur, 35);
			canvasTerrainContext.drawImage(decompte, 40, 5); 
		}
	}
  
	var rayonCible = 20;
	var rayonCible2 = 20;
	var rayonCible3 = 15;
	var rayonCible4 = 15;
	var rayonCible5 = 15;
	var rayonCible6 = 8;
	
	var decompte = 5;
	var decompte2 = 5;
	var decompte3 = 5;
	var decompte4 = 5;
	var decompte5 = 5;
	var decompte6 = 5;
	
	var activePosCercle3 = 0;
	var activePosCercle4 = 0;
	var activePosCercle5 = 0;
	var activePosCercle6 = 0;
	
	positionXCercle = Math.floor((Math.random() * 550) + 30);
	var positionYCercle = Math.floor((Math.random() * 310) + 60);
	var rayon = 0;
	
	positionXCercle2 = Math.floor((Math.random() * 550) + 1);	
	var positionYCercle2 = Math.floor((Math.random() * 310) + 60);
	var rayon2 = 0;
	
	positionXCercle3 = Math.floor((Math.random() * 550) + 30);	
	var positionYCercle3 = -100;
	var rayon3 = 0;
	
	positionXCercle4 = Math.floor((Math.random() * 550) + 30);	
	var positionYCercle4 = -100;
	var rayon4 = 0;
	
	positionXCercle5 = Math.floor((Math.random() * 550) + 30);	
	var positionYCercle5 = -100;
	var rayon5 = 0;
	
	positionXCercle6 = Math.floor((Math.random() * 550) + 30);	
	var positionYCercle6 = -100;
	var rayon6 = 0;

	
   var dessinerCercle = function() {
	canvasTerrainContext.fillStyle = "rgba(134, 196, 209, 0.6)";
	canvasTerrainContext.beginPath(); 
	canvasTerrainContext.arc(positionXCercle, positionYCercle, rayonCible+rayon, 0, Math.PI*2);
	canvasTerrainContext.fill();	
	}
	var dessinerCible = function() {
	canvasTerrainContext.fillStyle = "rgba(68, 133, 147, 1)";
	canvasTerrainContext.beginPath();
	canvasTerrainContext.arc(positionXCercle, positionYCercle, rayonCible, 0, Math.PI*2); 
	canvasTerrainContext.fill();
	canvasTerrainContext.font = "11px Arial,Calibri,Geneva";
	canvasTerrainContext.fillStyle = "white";
	canvasTerrainContext.fillText(decompte, positionXCercle-3, positionYCercle+3);
	}
	
    var dessinerCercle2 = function() {
	canvasTerrainContext.fillStyle = "rgba(137, 207, 157, 0.6)";
	canvasTerrainContext.beginPath();
	canvasTerrainContext.arc(positionXCercle2, positionYCercle2, rayonCible2+rayon2, 0, Math.PI*2); 
	canvasTerrainContext.fill();
	}
	var dessinerCible2 = function() {
	canvasTerrainContext.fillStyle = "rgba(82, 134, 97, 1)";
	canvasTerrainContext.beginPath();
	canvasTerrainContext.arc(positionXCercle2, positionYCercle2, rayonCible2, 0, Math.PI*2);
	canvasTerrainContext.fill();
	canvasTerrainContext.font = "11px Arial,Calibri,Geneva";
	canvasTerrainContext.fillStyle = "white";
	canvasTerrainContext.fillText(decompte2, positionXCercle2-3, positionYCercle2+3);	
	}	
    var dessinerCercle3 = function() {
	canvasTerrainContext.fillStyle = "rgba(247, 211, 84, 0.6)";
	canvasTerrainContext.beginPath(); 
	canvasTerrainContext.arc(positionXCercle3, positionYCercle3, rayonCible3+rayon3, 0, Math.PI*2); 
	canvasTerrainContext.fill();
	}
	var dessinerCible3 = function() {
	canvasTerrainContext.fillStyle = "rgba(191, 161, 58, 1)";
	canvasTerrainContext.beginPath(); 
	canvasTerrainContext.arc(positionXCercle3, positionYCercle3, rayonCible3, 0, Math.PI*2); 
	canvasTerrainContext.fill();	
	canvasTerrainContext.font = "11px Arial,Calibri,Geneva";
	canvasTerrainContext.fillStyle = "white";
	canvasTerrainContext.fillText(decompte3, positionXCercle3-3, positionYCercle3+3);
	}
    var dessinerCercle4 = function() {
	canvasTerrainContext.fillStyle = "rgba(222, 151, 111, 0.6)";
	canvasTerrainContext.beginPath(); 
	canvasTerrainContext.arc(positionXCercle4, positionYCercle4, rayonCible4+rayon4, 0, Math.PI*2); 
	canvasTerrainContext.fill();
	}
	var dessinerCible4 = function() {
	canvasTerrainContext.fillStyle = "rgba(163, 107, 76, 1)";
	canvasTerrainContext.beginPath(); 
	canvasTerrainContext.arc(positionXCercle4, positionYCercle4, rayonCible4, 0, Math.PI*2); 
	canvasTerrainContext.fill();
	canvasTerrainContext.font = "11px Arial,Calibri,Geneva";
	canvasTerrainContext.fillStyle = "white";
	canvasTerrainContext.fillText(decompte4, positionXCercle4-3, positionYCercle4+3);
	}
    var dessinerCercle5 = function() {
	canvasTerrainContext.fillStyle = "rgba(255, 126, 126, 0.6)";
	canvasTerrainContext.beginPath(); 
	canvasTerrainContext.arc(positionXCercle5, positionYCercle5, rayonCible5+rayon5, 0, Math.PI*2);
	canvasTerrainContext.fill();
	}
	var dessinerCible5 = function() {
	canvasTerrainContext.fillStyle = "rgba(202, 81, 81, 1)";
	canvasTerrainContext.beginPath(); 
	canvasTerrainContext.arc(positionXCercle5, positionYCercle5, rayonCible5, 0, Math.PI*2);
	canvasTerrainContext.fill();	
	canvasTerrainContext.font = "11px Arial,Calibri,Geneva";
	canvasTerrainContext.fillStyle = "white";
	canvasTerrainContext.fillText(decompte5, positionXCercle5-3, positionYCercle5+3);
	}
    var dessinerCercle6 = function() {
	canvasTerrainContext.fillStyle = "rgba(255, 126, 126, 0.6)";
	canvasTerrainContext.beginPath();
	canvasTerrainContext.arc(positionXCercle6, positionYCercle6, rayonCible6+rayon6, 0, Math.PI*2);
	canvasTerrainContext.fill();
	}
	var dessinerCible6 = function() {
	canvasTerrainContext.fillStyle = "rgba(202, 81, 81, 1)";
	canvasTerrainContext.beginPath(); 
	canvasTerrainContext.arc(positionXCercle6, positionYCercle6, rayonCible6, 0, Math.PI*2); 
	canvasTerrainContext.fill();
	canvasTerrainContext.font = "11px Arial,Calibri,Geneva";
	canvasTerrainContext.fillStyle = "white";
	canvasTerrainContext.fillText(decompte6, positionXCercle6-3, positionYCercle6+3);	
	}
		
	var animerCercle = function () {
			rayon+=0.3;                                                                                                                            
			rayon2+=0.3;
		
		if (compteur > 8)
			rayon3+=0.4;
		if (compteur > 9)
			rayon4+=0.4;
		if (compteur > 10)
			rayon5+=0.4;
		
		if (compteur > 8 && activePosCercle3 == 0)
		{
			positionYCercle3 = Math.floor((Math.random() * 310) + 45);
			activePosCercle3 = 1;
		}
		if (compteur > 9 && activePosCercle4 == 0)
		{
			positionYCercle4 = Math.floor((Math.random() * 310) + 45);
			activePosCercle4 = 1;
		}
		if (compteur > 10 && activePosCercle5 == 0)
		{
			positionYCercle5 = Math.floor((Math.random() * 310) + 45);
			activePosCercle5 = 1;
		}
		if (compteur > 33 && activePosCercle6 == 0)
		{
			positionYCercle6 = Math.floor((Math.random() * 310) + 45);
			activePosCercle6 = 1;
		}
		
		// A partir de 25sec
		if (rayonCible > 12 && compteur > 25)
		{
				rayonCible-=0.01;
				rayonCible2-=0.01;
				rayonCible3-=0.01;
				rayonCible4-=0.01;
				rayonCible5-=0.01;
		}
				

		if (compteur > 33) {
				rayon+=0.1;
				rayon2+=0.1;
				rayon3+=0.1;
				rayon4+=0.1;
				rayon5+=0.1;
				rayon6+=0.5;
		}	

		if (compteur > 40) {
				rayon+=0.1;
				rayon2+=0.1;
				rayon3+=0.1;
				rayon4+=0.1;
				rayon5+=0.1;
				rayon6+=0.1;
		}	

		if (compteur > 50) {
				rayon+=0.1;
				rayon2+=0.1;
				rayon3+=0.1;
				rayon4+=0.1;
				rayon5+=0.1;
				rayon6+=0.1;
		}			
	}
 
  
    var initialisation = function() {
	compteur = 0;
	score = 0;

	setInterval(function() { 
	compteur++;
	}
	,1000);	
	
	activePosCercle3 = 0;
	activePosCercle4 = 0;
	activePosCercle5 = 0;
	activePosCercle6 = 0;
	
	
	positionXCercle = Math.floor((Math.random() * 550) + 30);
	positionYCercle = Math.floor((Math.random() * 310) + 60);
	rayon = 0;
	
	positionXCercle2 = Math.floor((Math.random() * 550) + 1);	
	positionYCercle2 = Math.floor((Math.random() * 310) + 60);
	rayon2 = 0;
	
	positionXCercle3 = Math.floor((Math.random() * 550) + 30);	
	positionYCercle3 = -100;
	rayon3 = 0;
	
	positionXCercle4 = Math.floor((Math.random() * 550) + 30);	
	positionYCercle4 = -100;
	rayon4 = 0;
	
	positionXCercle5 = Math.floor((Math.random() * 550) + 30);	
	positionYCercle5 = -100;
	rayon5 = 0;
	
	positionXCercle6 = Math.floor((Math.random() * 550) + 30);	
	positionYCercle6 = -100;
	rayon6 = 0;
	
    // le code de l'initialisation
	dessinerTerrain();
	dessinerDecompte();
              
      requestAnimId = window.requestAnimationFrame(principale); // premier appel de principale au rafraichissement de la page
    }

	
var principale = function() {

(function() { // si l'onglet est changé pendant le jeu, on quitte le jeu
    var hidden = "hidden";

    // Standards:
    if (hidden in document)
        document.addEventListener("visibilitychange", onchange);
    else if ((hidden = "mozHidden") in document)
        document.addEventListener("mozvisibilitychange", onchange);
    else if ((hidden = "webkitHidden") in document)
        document.addEventListener("webkitvisibilitychange", onchange);
    else if ((hidden = "msHidden") in document)
        document.addEventListener("msvisibilitychange", onchange);
    // IE 9 and lower:
    else if ('onfocusin' in document)
        document.onfocusin = document.onfocusout = onchange;
    // All others:
    else
        window.onpageshow = window.onpagehide 
            = window.onfocus = window.onblur = onchange;

    function onchange (evt) {
        var v = 'visible', h = 'hidden',
            evtMap = { 
                focus:v, focusin:v, pageshow:v, blur:h, focusout:h, pagehide:h 
            };

        evt = evt || window.event;
        if (evt.type in evtMap)
            document.body.className = evtMap[evt.type];
        else
		{
			alert('Merci de na pas changer d\'onglet pendant une partie');
			if (defi == 'off')
			{
				$.get('traitement_cible.php?onglet', function() { });
				document.location.href="cible-onglet";
			}
			else
				document.location.href="defi_jeu.php?onglet";						
		}
    }
})();

	// le code du jeu
	animerCercle();
	dessinerTerrain();
	dessinerDecompte();

// affichage du compte-a-rebour		
	if (rayon*2 +rayonCible*2 > terrainLargeur - 250)
		decompte = 4;
	if (rayon*2 +rayonCible*2 > terrainLargeur - 150)
		decompte = 3;
	if (rayon*2 +rayonCible*2 > terrainLargeur - 100)
		decompte = 2;
	if (rayon*2 +rayonCible*2 > terrainLargeur - 50)
		decompte = 1;
	if (rayon*2 +rayonCible*2 > terrainLargeur - 10)
		decompte = 0;
		
	if (rayon2*2 +rayonCible2*2 > terrainLargeur - 250)
		decompte2 = 4;
	if (rayon2*2 +rayonCible2*2 > terrainLargeur - 150)
		decompte2 = 3;
	if (rayon2*2 +rayonCible2*2 > terrainLargeur - 100)
		decompte2 = 2;
	if (rayon2*2 +rayonCible2*2 > terrainLargeur - 50)
		decompte2 = 1;
	if (rayon2*2 +rayonCible2*2 > terrainLargeur - 10)
		decompte2 = 0;
		
	if (rayon3*2 +rayonCible3*2 > terrainLargeur - 250)
		decompte3 = 4;
	if (rayon3*2 +rayonCible3*2 > terrainLargeur - 150)
		decompte3 = 3;
	if (rayon3*2 +rayonCible3*2 > terrainLargeur - 100)
		decompte3 = 2;
	if (rayon3*2 +rayonCible3*2 > terrainLargeur - 50)
		decompte3 = 1;
	if (rayon3*2 +rayonCible3*2 > terrainLargeur - 10)
		decompte3 = 0;
		
	if (rayon4*2 +rayonCible4*2 > terrainLargeur - 250)
		decompte4 = 4;
	if (rayon4*2 +rayonCible4*2 > terrainLargeur - 150)
		decompte4 = 3;
	if (rayon4*2 +rayonCible4*2 > terrainLargeur - 100)
		decompte4 = 2;
	if (rayon4*2 +rayonCible4*2 > terrainLargeur - 50)
		decompte4 = 1;
	if (rayon4*2 +rayonCible4*2 > terrainLargeur - 10)
		decompte4 = 0;
		
	if (rayon5*2 +rayonCible5*2 > terrainLargeur - 250)
		decompte5 = 4;
	if (rayon5*2 +rayonCible5*2 > terrainLargeur - 150)
		decompte5 = 3;
	if (rayon5*2 +rayonCible5*2 > terrainLargeur - 100)
		decompte5 = 2;
	if (rayon5*2 +rayonCible5*2 > terrainLargeur - 50)
		decompte5 = 1;
	if (rayon5*2 +rayonCible5*2 > terrainLargeur - 10)
		decompte5 = 0;
		
	if (rayon6*2 +rayonCible6*2 > terrainLargeur - 250)
		decompte6 = 4;
	if (rayon6*6 +rayonCible6*2 > terrainLargeur - 150)
		decompte6 = 3;
	if (rayon6*2 +rayonCible6*2 > terrainLargeur - 100)
		decompte6 = 2;
	if (rayon6*2 +rayonCible6*2 > terrainLargeur - 50)
		decompte6 = 1;
	if (rayon6*2 +rayonCible6*2 > terrainLargeur - 10)
		decompte6 = 0;
// fin affichage du compte-a -rebour	

	
// GAME OVER 

	// IL Y A SESSION TOURNOI OU SESSION ENTRAINEMENT DONC PAS BESOIN DE GERER CIBLE.PHP
	if (rayon*2 +rayonCible*2 > terrainLargeur)
	{
		fin = 1;
		if (defi == 'off')
			document.location.href="cible.php?fin_partie&score="+score+"&temps="+compteur;
		else
			document.location.href="defi_jeu.php?fin_partie&score="+score+"&temps="+compteur;
	}
	if (rayon2*2 +rayonCible2*2 > terrainLargeur)
	{
		fin = 1;
		if (defi == 'off')
			document.location.href="cible.php?fin_partie&score="+score+"&temps="+compteur;
		else
			document.location.href="defi_jeu.php?fin_partie&score="+score+"&temps="+compteur;
	}
	if (rayon3*2 +rayonCible3*2 > terrainLargeur)
	{
		fin = 1;
		if (defi == 'off')
			document.location.href="cible.php?fin_partie&score="+score+"&temps="+compteur;
		else
			document.location.href="defi_jeu.php?fin_partie&score="+score+"&temps="+compteur;
	}
	if (rayon4*2 +rayonCible4*2 > terrainLargeur)
	{
		fin = 1;
		if (defi == 'off')
			document.location.href="cible.php?fin_partie&score="+score+"&temps="+compteur;
		else
			document.location.href="defi_jeu.php?fin_partie&score="+score+"&temps="+compteur;
	}
	if (rayon5*2 +rayonCible5*2 > terrainLargeur)
	{
		fin = 1;
		if (defi == 'off')
			document.location.href="cible.php?fin_partie&score="+score+"&temps="+compteur;
		else
			document.location.href="defi_jeu.php?fin_partie&score="+score+"&temps="+compteur;
	}
	if (rayon6*2 +rayonCible6*2 > terrainLargeur)
	{
		fin = 1;
		if (defi == 'off')
			document.location.href="cible.php?fin_partie&score="+score+"&temps="+compteur;
		else
			document.location.href="defi_jeu.php?fin_partie&score="+score+"&temps="+compteur;	
	}
		
		if (fin == 0)
			requestAnimId = window.requestAnimationFrame(principale); // rappel de principale au prochain rafraichissement de la page
}


	canvas.addEventListener('mousedown', function(event) { 
		var d = 0;
		var d2 = 0;
		//calcul pos curseur
        var posX = new Number();
        var posY = new Number();

        if (event.x != undefined && event.y != undefined)
        {
          posX = event.x + document.body.scrollLeft +
              document.documentElement.scrollLeft;
          posY = event.y + document.body.scrollTop +
              document.documentElement.scrollTop;
        }
        else // Firefox method pour avoir la position
        {
          posX = event.clientX + document.body.scrollLeft +
              document.documentElement.scrollLeft;
          posY = event.clientY + document.body.scrollTop +
              document.documentElement.scrollTop;
        }
		
        posX -= canvas.offsetLeft;
        posY -= canvas.offsetTop;		
		// fin calcule pos
		
		d = (posX - positionXCercle)*(posX - positionXCercle) + 
		(posY - positionYCercle)*(posY - positionYCercle);
		if (d <= (rayonCible * rayonCible))
		{
			canvasTerrainContext.clearRect ( 0, 0 , terrainLongueur , terrainLargeur );
			positionXCercle = Math.floor((Math.random() * 550) + 1);
				
			positionYCercle = Math.floor((Math.random() * 310) + 45);
			rayon = 0;
			score += decompte;
			decompte = 5;
		}
		d = (posX - positionXCercle2)*(posX - positionXCercle2) + 
		(posY - positionYCercle2)*(posY - positionYCercle2);
		if (d <= (rayonCible2 * rayonCible2))
		{
			canvasTerrainContext.clearRect ( 0, 0 , terrainLongueur , terrainLargeur );
			positionXCercle2 = Math.floor((Math.random() * 550) + 1);
				
			positionYCercle2 = Math.floor((Math.random() * 310) + 45);
			rayon2 = 0;
			score += decompte2;
			decompte2 = 5;
		}
		d = (posX - positionXCercle3)*(posX - positionXCercle3) + 
		(posY - positionYCercle3)*(posY - positionYCercle3);
		if (d <= (rayonCible3 * rayonCible3))
		{
			canvasTerrainContext.clearRect ( 0, 0 , terrainLongueur , terrainLargeur );
			positionXCercle3 = Math.floor((Math.random() * 550) + 1);
				
			positionYCercle3 = Math.floor((Math.random() * 310) + 45);
			rayon3 = 0;
			score += decompte3;
			decompte3 = 5;
		}
		d = (posX - positionXCercle4)*(posX - positionXCercle4) + 
		(posY - positionYCercle4)*(posY - positionYCercle4);
		if (d <= (rayonCible4 * rayonCible4))
		{
			canvasTerrainContext.clearRect ( 0, 0 , terrainLongueur , terrainLargeur );
			positionXCercle4 = Math.floor((Math.random() * 550) + 1);
				
			positionYCercle4 = Math.floor((Math.random() * 310) + 45);
			rayon4 = 0;
			score += decompte4;
			decompte4 = 5;
		}
		d = (posX - positionXCercle5)*(posX - positionXCercle5) + 
		(posY - positionYCercle5)*(posY - positionYCercle5);
		if (d <= (rayonCible5 * rayonCible5))
		{
			canvasTerrainContext.clearRect ( 0, 0 , terrainLongueur , terrainLargeur );
			positionXCercle5 = Math.floor((Math.random() * 550) + 1);
				
			positionYCercle5 = Math.floor((Math.random() * 310) + 45);
			rayon5 = 0;
			score += decompte5;
			decompte5 = 5;
		}
		d = (posX - positionXCercle6)*(posX - positionXCercle6) + 
		(posY - positionYCercle6)*(posY - positionYCercle6);
		if (d <= (rayonCible6 * rayonCible6))
		{
			canvasTerrainContext.clearRect ( 0, 0 , terrainLongueur , terrainLargeur );
			positionXCercle6 = Math.floor((Math.random() * 550) + 1);
				
			positionYCercle6 = Math.floor((Math.random() * 310) + 45);
			rayon6 = 0;
			score += decompte6;
			decompte6 = 5;
		}
	}, false);	
	
// pour eviter que la scroll barre défile qd les flêches haut et bas sont activées
 // if( event.preventDefault){
 //   event.preventDefault();
 //   event.stopPropagation();
 // }
 // else{
 //   event.returnValue = false;
 //   event.cancelBubble = true;
 // }
// ---
	
	var clique = document.getElementById('clique');
    clique.onclick = function() {
		initialisation(); // appel de la fonction initialisation au chargement de la page
		var remplace = document.createElement('div');
			remplace.id = "en_cours";
		var remplaceText = document.createTextNode("En cours");
			remplace.appendChild(remplaceText);
		clique.parentNode.replaceChild(remplace, clique);
		if(document.getElementById('jeu_defi_intro'))
		{
			jeu_defi_intro.style.display = "none";
		}
		if(document.getElementById('tournoi_kp'))
		{
			tournoi_kp.style.display = "none";
		}
		remplace.style.backgroundColor = "#528661";
		remplace.style.paddingTop = "7px";
		remplace.style.paddingBottom = "7px";
		remplace.style.textAlign = "center";
		remplace.style.color = "white";
		remplace.style.fontSize = "large";
		var canvas  = document.querySelector('#canvas');
		canvas.style.backgroundColor = "#dedede";
		canvas.style.backgroundImage = "none";
		
		if (tournoi == 'on' && defi == 'off' && entrainement == 'off') // C'est tournoi
			$.get('traitement_cible.php?debut_partie&tournoi', function() { });
		else if (defi == 'on' && tournoi == 'off' && entrainement == 'off' ) // C'est defi
			$.get('defi_jeu.php?debut_partie', function() { });	
		else if (entrainement == 'on' && tournoi == 'off' && defi == 'off' ) // C'est entrainement
			$.get('traitement_cible.php?debut_partie', function() { });
		
		canvas.style.opacity = "1";
		fin_partie.style.display = "none";
	};