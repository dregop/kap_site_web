<?php 
if (isset($_GET['debut_partie']))
{
	// ouverture du fichier numero_partie
	$monfichier = fopen('numero_partie.txt', 'r+');				 
	$numero = fgets($monfichier); 
	$numero++; 
	fseek($monfichier, 0); 
	fputs($monfichier,$numero); 
	fclose($monfichier);
	$_SESSION['numero_partie'] = $numero;
	
	if(isset($_SESSION['tournoi_esquive']) OR isset($_SESSION['tournoi_lettre'])
	OR isset($_SESSION['tournoi_cible']))
	{
		// ON ENLEVE 1KP AU JOUEUR ! SI TOURNOI
		$total_kp = $_SESSION['kp'] - 1;
		
		$r_kp= $bdd->prepare('UPDATE membres SET kp=:kp WHERE id=:id')
								or die(print_r($bdd->errorInfo()));
		$r_kp->execute(array('kp' => $total_kp,
								'id' => $_SESSION['id_membre']))
								or die(print_r($bdd->errorInfo()));	
		$r_kp->closeCursor(); // Termine le traitement de la requête 
	}

	
	if(isset($_SESSION['identifiant']))
	{
		$identifiant = $_SESSION['identifiant'];
	}
	else
		$identifiant = "visiteur";
		
    $milliseconds = round(microtime(true) * 1000);
	$re_c2 = $bdd->prepare('INSERT INTO '.$traitement_jeu.' (identifiant,temps_debut_jeu,numero_partie) 
	VALUES(:identifiant,:debut_jeu,:numero_partie)')
	or die(print_r($bdd->errorInfo()));
	$re_c2->execute(array('identifiant' => $identifiant, 'debut_jeu' => $milliseconds,
	'numero_partie' => $_SESSION['numero_partie']))or die(print_r($bdd->errorInfo()));
}	
if (isset($_GET['onglet']))
{
	$req1 = $bdd->prepare('DELETE FROM '.$traitement_jeu.' WHERE numero_partie=:num')
	or die(print_r($bdd->errorInfo()));
	$req1->execute(array('num' => $_SESSION['numero_partie'])) 
	or die(print_r($bdd->errorInfo()));
	$req1->closeCursor(); // Termine le traitement de la requête
}