<?php
$acces = 'on';
include('header.php');

unset($_SESSION['formulaire_password']); // SESSION POUR LE BLOC MDP
unset($_SESSION['formulaire_email']); // SESSION POUR LE BLOC EMAIL
unset($_SESSION['numero_partie']); 
unset($_SESSION['envoyer_message']); // SESSION POUR LE BLOC MESSAGE

// ON DETRUIT LES SESSION ENTRAINEMENT ET TOURNOI AVANT D'ACCEDER AUX JEUX
// ON LE RE CREE PAR LA SUITE COMME CA AUCUN BUG ENTRE SESSION TOURNOI ET ENTRAINEMENT
unset($_SESSION['tournoi_esquive']);
unset($_SESSION['tournoi_lettre']);
unset($_SESSION['tournoi_cible']);

unset($_SESSION['entrainement_esquive']);
unset($_SESSION['entrainement_lettre']);
unset($_SESSION['entrainement_cible']);

echo'
<div class="contener_total" id="bandeau_index" style="height:290px;">
	<div class="contener_980">';
?>

		<div class="menu_index" id="menu_esquive">
<?php	
			if(isset($_SESSION['identifiant']))
			{
				echo'
				<div class="mask_jeux">';
				if(isset($_SESSION['affichage_tournoi']) 
				AND $_SESSION['affichage_tournoi'] == 'on')
				{
					echo'
					<a href="esquive-entrainement">
						<div class="entrainement">
							Entrainement
						</div>
					</a>
					<a href="esquive-tournoi">
						<div class="tournoi_co">
							Tournoi
						</div>
					</a>';
				}
				else
				{
					echo'
					<a href="esquive-entrainement">
						<div class="entrainement">
							Entrainement
						</div>
					</a>
					<div class="tournoi_deco">
						Tournoi
					</div>';
				}
				echo'
				</div>';
			}
			else
			{
				echo'
				<div class="mask_jeux">
					<a href="esquive-entrainement">
						<div class="entrainement">
							Entrainement
						</div>
					</a>
					<div class="tournoi_deco">
						Tournoi
					</div>
				</div>';
			}
			echo'
		
			<div class="menu_bas" id="menu_bas_esquive">
				<p class="menu_mot" id="mot_esquive">
					Esquive
				</p>
			</div>
		</div>
		
		<div class="menu_index" id="menu_lettre">';
		
			if(isset($_SESSION['identifiant']))
			{
				echo'
				<div class="mask_jeux">';
				if(isset($_SESSION['affichage_tournoi']) 
				AND $_SESSION['affichage_tournoi'] == 'on')
				{
					echo'
					<a href="lettre-entrainement">
						<div class="entrainement">
							Entrainement
						</div>
					</a>
					<a href="lettre-tournoi">
						<div class="tournoi_co">
							Tournoi
						</div>
					</a>';
				}
				else
				{
					echo'
					<a href="lettre-entrainement">
						<div class="entrainement">
							Entrainement
						</div>
					</a>
					<div class="tournoi_deco">
						Tournoi
					</div>';
				}
				echo'
				</div>';
			}
			else
			{
				echo'
				<div class="mask_jeux">
					<a href="lettre-entrainement">
						<div class="entrainement">
							Entrainement
						</div>
					</a>
					<div class="tournoi_deco">
						Tournoi
					</div>
				</div>';
			}
			echo'	
			<div class="menu_bas" id="menu_bas_lettre">
				<p class="menu_mot" id="mot_lettre">
					Lettre
				</p>
			</div>
		</div>

			
		<div class="menu_index" id="menu_cible">';
		
			if(isset($_SESSION['identifiant']))
			{
				echo'
				<div class="mask_jeux">';
				if(isset($_SESSION['affichage_tournoi']) 
				AND $_SESSION['affichage_tournoi'] == 'on')
				{
					echo'
					<a href="cible-entrainement">
						<div class="entrainement">
							Entrainement
						</div>
					</a>
					<a href="cible-tournoi">
						<div class="tournoi_co">
							Tournoi
						</div>
					</a>';
				}
				else
				{
					echo'
					<a href="cible-entrainement">
						<div class="entrainement">
							Entrainement
						</div>
					</a>
					<div class="tournoi_deco">
						Tournoi
					</div>';
				}
				echo'
				</div>';
			}
			else
			{
				echo'
				<div class="mask_jeux">
					<a href="cible-entrainement">
						<div class="entrainement">
							Entrainement
						</div>
					</a>
					<div class="tournoi_deco">
						Tournoi
					</div>
				</div>';
			}
			echo'		
			<div class="menu_bas" id="menu_bas_cible">
				<p class="menu_mot" id="mot_cible">
					Cible
				</p>
			</div>
		</div>	
	
	</div>

</div>
<div class="contener_980">

	<div class="informations_jeux" id="informations_esquive">
		<div class="information_kp">
			'.$total_kp_esquive.' 
			<img src="images/lingot_kp.png" alt="Kp"/>
		</div>
		<div class="jeux_regles">
			<h4> Règles: </h4><img class="regles" src="images/regles.png" alt="Règles"/>
			<p>
				Assurez le meilleur temps en évitant toutes les formes avec les 
				touches directionnelles de votre clavier. 
			
			</p>
			
			<h4> Bonus: </h4>
		
		</div>
	</div>

	</div>
	
	<div class="informations_jeux" id="informations_lettre">
		<div class="information_kp">
			'.$total_kp_lettre.' 
			<img src="images/lingot_kp.png" alt="Kp"/>
		</div>
		<div class="jeux_regles">
			<h4> Règles: </h4><img class="regles" src="images/regles.png" alt="Règles"/>
			<p>
				Entrez toutes les lettres avant qu\'elles ne tombent. Vous avez 3
				chances.
			
			</p>
			
			<h4> Bonus: </h4>
		
		</div>
	</div>
	
	<div class="informations_jeux" id="informations_cible">
		<div class="information_kp">
			'.$total_kp_cible.' 
			<img src="images/lingot_kp.png" alt="Kp"/>
		</div>
		<div class="jeux_regles">
			<h4> Règles: </h4><img class="regles" src="images/regles.png" alt="Règles"/>
			<p>
				Cliquez sur les figures centrales correspondantes
				à la forme de l\'arrière plan. L\'erreur peut être fatale. 
			
			</p>
			
			<h4> Bonus: </h4>
		
		</div>
	</div>
	<div style="width:980px;height:1px;clear:left;"></div>

</div>';


include('footer.php');
?>
</body>

</html>