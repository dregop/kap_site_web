<?php
session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arrête tout
    die('Erreur : '.$e->getMessage());
		
}
	$req1 = $bdd->prepare('DELETE FROM connecte WHERE identifiant=:id')
	or die(print_r($bdd->errorInfo()));
	$req1->execute(array('id' => $_SESSION['identifiant'])) 
	or die(print_r($bdd->errorInfo()));
	$req1->closeCursor(); // Termine le traitement de la requête
	
// On detruit les sessions
session_destroy();

header('Location: accueil'); 
