<?php
session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arrête tout
    die('Erreur : '.$e->getMessage());
}
		
// POUR AFFICHER SI LES TOURNOIS SONT LANCES OU PAS
if(!isset($_SESSION['affichage_tournoi']))
{
	$_SESSION['affichage_tournoi'] = 'on';
}

if(!isset($_SESSION['identifiant']) AND !isset($accueil) AND !isset($acces))
{
	header('Location: accueil');
}

if(isset($_SESSION['identifiant']))
{
	$req = $bdd->prepare('SELECT photo_profil, kp, identifiant 
						FROM membres 
						WHERE identifiant=:identifiant')
						or die(print_r($bdd->errorInfo()));
	$req->execute(array('identifiant' => $_SESSION['identifiant']))
						or die(print_r($bdd->errorInfo()));
	$donnees= $req->fetch();
	$_SESSION['photo_profil'] = $donnees['photo_profil'];
	$_SESSION['kp'] = $donnees['kp'];
}

// POUR GERER LES CONNECTES --------------------------------------------------//
if(isset($_SESSION['identifiant']))
{
	$re_c = $bdd->prepare('SELECT identifiant FROM connecte 
							WHERE identifiant=:identifiant')
							or die(print_r($bdd->errorInfo()));
	$re_c->execute(array('identifiant' => $_SESSION['identifiant']))
							or die(print_r($bdd->errorInfo()));
	$donnees_c = $re_c->fetch();

	if (isset($donnees_c['identifiant']))
	{
		$re_c2 = $bdd->prepare('UPDATE connecte SET timestamp=:timestamp 
								WHERE identifiant=:identifiant')
								or die(print_r($bdd->errorInfo()));
		$re_c2->execute(array('identifiant' => $_SESSION['identifiant'], 
								'timestamp' => time()))
								or die(print_r($bdd->errorInfo()));
	}
	else
	{
		$re_c2 = $bdd->prepare('INSERT INTO connecte (identifiant, timestamp) 
								VALUES(:identifiant,:timestamp)')
								or die(print_r($bdd->errorInfo()));
		$re_c2->execute(array('identifiant' => $_SESSION['identifiant'], 
								'timestamp' => time()))
								or die(print_r($bdd->errorInfo()));	
	}
}

// CLASSEMENTS ---------------------------------------------------------------//
if(isset($_SESSION['identifiant']))
{
	$c_e = 1;
	$c_l = 1;
	$c_c = 1;
	$classement_e = $bdd->query('SELECT id,temps,identifiant 
							FROM meilleur_esquive 
							ORDER BY temps DESC')
							or die(print_r($bdd->errorInfo()));
	while ($d_e = $classement_e->fetch())
	{	
		if ($d_e['identifiant'] == $_SESSION['identifiant'])
		{
			$classement_esquive = $c_e;
		}
			
		$c_e++;
	}
	$classement_l = $bdd->query('SELECT id,score,identifiant 
							FROM meilleur_lettre 
							ORDER BY score DESC,temps DESC')
							or die(print_r($bdd->errorInfo()));
	while ($d_l = $classement_l -> fetch())
	{	
		if ($d_l['identifiant'] == $_SESSION['identifiant'])
		{
			$classement_lettre = $c_l;
		}
			
		$c_l++;
	}
	$classement_c = $bdd->query('SELECT id,score,identifiant 
							FROM meilleur_cible
							ORDER BY score DESC,temps DESC')
							or die(print_r($bdd->errorInfo()));
	while ($d_c = $classement_c->fetch())
	{	
		if ($d_c['identifiant'] == $_SESSION['identifiant'])
		{
			$classement_cible = $c_c;
		}
			
		$c_c++;
	}
}

// ON COMPTE LE NOMBRE DE KP DEPENSE (CHAQUE PARTICIPATION AU TOURNOI)
$r_kp1 = $bdd->query('SELECT COUNT(id) AS nombre_esquive FROM tournoi_esquive')
					or die(print_r($bdd->errorInfo()));	
$d_kp1 = $r_kp1->fetch();

$r_kp2 = $bdd->query('SELECT COUNT(id) AS nombre_lettre FROM tournoi_lettre')
					or die(print_r($bdd->errorInfo()));	
$d_kp2 = $r_kp2->fetch();

$r_kp3 = $bdd->query('SELECT COUNT(id) AS nombre_cible FROM tournoi_cible')
					or die(print_r($bdd->errorInfo()));	
$d_kp3 = $r_kp3->fetch();	

$mise_depart = 125; // ON PEUT S'EN SERVIR SI ON VEUT METTRE UN NOMBRE DE BASE

// On fait chaque total
$total_kp_esquive = $mise_depart + $d_kp1['nombre_esquive'];
$total_kp_lettre = $mise_depart + $d_kp2['nombre_lettre'];
$total_kp_cible = $mise_depart + $d_kp3['nombre_cible'];


// ON COMPTE LE NOMBRE TOTAL DE MEMBRES SUR LE SITE
$r_membres = $bdd->query('SELECT COUNT(id) AS nombre FROM membres')
					or die(print_r($bdd->errorInfo()));	
$d_membres = $r_membres->fetch();

// ON COMPTE LE NOMBRE DE PARTIES TOTAL !
$r1_parties = $bdd->query('SELECT COUNT(id) AS parties_esquive FROM traitement_esquive')
					or die(print_r($bdd->errorInfo()));	
$d1_parties = $r1_parties->fetch();

$r2_parties = $bdd->query('SELECT COUNT(id) AS parties_lettre FROM traitement_lettre')
					or die(print_r($bdd->errorInfo()));	
$d2_parties = $r2_parties->fetch();

$r3_parties = $bdd->query('SELECT COUNT(id) AS parties_cible FROM traitement_cible')
					or die(print_r($bdd->errorInfo()));	
$d3_parties = $r3_parties->fetch();	

$total_parties = $d1_parties['parties_esquive'] + $d2_parties['parties_lettre'] + $d3_parties['parties_cible'];

// ON COMPTE LE NOMBRE TOTAL DE MEMBRES DEFIS
$nbr_defis = 0;
$r_defis = $bdd->prepare('SELECT id 
						FROM defi
						WHERE distribution =:distribution')
						or die(print_r($bdd->errorInfo()));
$r_defis->execute(array('distribution' => 1))
						or die(print_r($bdd->errorInfo()));
while ($d_defis = $r_defis->fetch())
{	
	$nbr_defis++;
}
?>

<!DOCTYPE html>
  
<html lang="fr">
	<head>
		<title>Kap</title>
		
		<meta name="description" 
		content="Sur Kap, jouez - faîtes le meilleur score - remportez la cagnotte" />
		<meta charset="utf-8"/>
		<link rel="stylesheet" href="ancien_style.css"/> <!-- ANCIEN CSS --->
		<link rel="stylesheet" href="style.css"/>    
		<link rel="shortcut icon" type="image/x-icon" href="images/kap_logo_ico.ico" />
	</head>

<body> 
	<!-- background moving --->
	<div id="background1" class="mouse-bg"></div>
	<div id="background2" class="mouse-bg"></div>
	<div id="background3" class="mouse-bg"></div>
	


<?php
if(isset($donnees['reponse'], $donnees['question']) AND $donnees['reponse'] ==''
AND $donnees['question'] =='')
{
	echo'
	<div id="fond_overlay" style="display:block;"></div>
	<div id="securite">
		<div class="haut_securite">
			<form action="compte_post.php" method="post">
			<p class="texte_question">Ne l\'oubliez pas !  <br /><br />
				<select name="question" style="height:30px;width:190px;" >
					<option value="question_secrete">
						Question secrète
					</option>
					<option value="Quel est le nom de jeune fille de votre mère?">
						Quel est le nom de jeune fille de votre mère?
					</option>
					<option value="Quelle est votre musique préférée?">
						Quelle est votre musique préférée?
					</option>
					<option value="Quelle est votre ville préférée?">
						Quelle est votre ville préférée?
					</option>
					<option value="Quel est votre jeu préférée">
						Quel est votre jeu préféré?
					</option>
					<option value="Quelle est votre console préférée?">
						Quelle est votre console préférée?
					</option>
					<option value="Quelle est votre voiture préférée?">
						Quelle est votre voiture préférée?
					</option>
					<option value="Où êtes vous né(e)?">
						Où êtes vous né(e)?
					</option>
					<option value="Quel est votre livre préféré?">
						Quel est votre livre préféré?
					</option>
					<option value="Qui est votre meilleur ami d\'enfance?">
						Qui est votre meilleur ami d\'enfance?
					</option>
					<option value="Qui est votre enseignant préféré?">
						Qui est votre enseignant préféré?
					</option>
					<option value="Qui est votre pire enseignant?">
						Qui est votre pire enseignant?
					</option>
					<option value="Quel est le nom de votre lycée?">
						Quel est le nom de votre lycée?
					</option>
					<option value="Où travaillez-vous?">
						Où travaillez-vous?
					</option>
				</select>
				<br /><br />
				<input style="width:186px;height:20px;"type="text" name="reponse" id="reponse" placeholder=" Réponse" />
				<input type="submit" id="enregistrer" name="enregistrer" value="Enregistrer" class="enregistrer_question"/>
			</p>
			</form>			
		</div>
		<div class="bas_securite">
			<img src="images/mot_securite.png" alt="Securite"/>
		</div>
	
	
	
	</div>';
}
	echo'
	<header>';
?>
		<div id="contient_header_top">
			<div id="header_top">
				<a href="accueil">
					<div id="kap_logo"> </div>
				</a>
<?php
				echo'
				<div id="top_haut">';
				
				
					if(!isset($_SESSION['identifiant']))
					{
						echo'
						<form action="logging_post.php" method="post">
							<img src="images/icone_identifiant.png" alt="Identifiant"/>
							<input type="text" name="identifiant" id="identifiant" placeholder=" Identifiant" />
							<img src="images/icone_password.png" alt="Password"/>
							<input type="password" name="password" id="password" placeholder=" Mot de passe" />
							<input type="image" src="images/icone_connecte.png"style="height:21px;width:20px;border:none;position:relative;top:7px;">
						</form>';
					}
					else
					{		
						echo'
						<div id="position_parametre">	
							
							<a href="accueil">
								<div id="icone_accueil" class="icone_header" title="Accueil"> </div>
							</a>
							<a href="jeux">
								<div id="icone_jeux" class="icone_header icone_marginleft" title="Jeux"> </div>
							</a>
							<a href="defi">
								<div id="icone_defis" class="icone_header icone_marginleft" title="Défis"> </div>
							</a>
							<a href="message">
								<div id="icone_message" class="icone_header icone_marginleft" title="Message"> </div>
							</a>
							<a href="compte">
								<div id="icone_parametre" class="icone_header  icone_marginleft" title="Paramètre"> </div>
							</a>
							
							<form action="recherche" method="post">
								<img src="images/icone_recherche.png" alt="Recherche" />';
							if(isset($_POST['recherche']) AND $_POST['recherche'] !='')
							{	
?>
								<input style="width:255px;" id="recherche" name="recherche" type="text" value="<?php echo stripslashes(htmlspecialchars($_POST['recherche']));?>" />
<?php
							}
							else
							{	
?>
								<input style="width:255px;" id="recherche" name="recherche" type="text" placeholder=" Chercher des joueurs" />
<?php				
							}

							echo'
							</form>
						</div>';
					}
				echo'
				</div>
			
			
				<div id="top_bas">';
					if(!isset($_SESSION['identifiant']))
						echo'
						<span>Mot de passe oublié ?</span>
						&nbsp| ';
					
					echo'
					&nbsp<span>Nous contacter</span>
					&nbsp| 
					&nbsp<span class="texte_bleu">Français</span>';
					
					if(isset($_SESSION['identifiant']))
						echo'
						&nbsp|
						<a href="logout.php">
							&nbsp<span class="texte_rouge">Déconnexion</span>
						</a>';
					
				
				echo'
				</div>
			</div>
		</div>
		
		<div id="header_bottom">';
		
			// AFFICHAGE DE LA MISE DES TOURNOIS ET DU TEMPS RESTANT
			if(!isset($_SESSION['identifiant']))
			{
				$total_jeux = ($mise_depart * 3) + $d_kp1['nombre_esquive'] + $d_kp2['nombre_lettre'] + $d_kp3['nombre_cible'];
				echo'
				<span id="kp_header_bottom">
					Mise des tournois:
					<span>'.$total_jeux.'</span>Kp
					<img src="images/lingot_kp.png" alt=" Kp"/> 
				</span>
				<div id="contient_rebours">
					<div id="compte_a_rebours1" class="compte_a_rebours" >
						<noscript>Merci d\'activer votre JavaScript.</noscript>
					</div>
				</div>
				<img class="temps_lingots" src="images/temps_lingots.png" alt="Fin du tournois"/>';
			}
			else
			{
				echo'
				<div id="profil_header">';
					echo'
					<span class="centre_image30">';
						
					if(isset($donnees['photo_profil']) AND $donnees['photo_profil'] != '' AND $donnees['photo_profil'] != 0)
					{
						$source = getimagesize('images_utilisateurs/'.$donnees['photo_profil']); 	// La photo est la source
						if ($source[0] <= 45 AND $source[1] <= 45)
							echo '<img style="width:30px;height:30px;" src="images_utilisateurs/'.$donnees['photo_profil'].'" alt="Photo de profil" />';
						else
							echo '<img style="width:30px;height:30px;" src="images_utilisateurs/mini_2_'.$donnees['photo_profil'].'" alt="Photo de profil" />';
					}
					else
						echo'<img style="width:30px;height:30px;" src="images/image_defaut.png" alt="Image"/>';
						
						echo'
						</span>
						<span id="identifiant">
							'.stripslashes(htmlspecialchars($_SESSION['identifiant'])).'
						</span>
				</div>
				

				
				<div id="kp_connecte">
					<a href="compte"><span class="nbr_header">'.$_SESSION['kp'].' Kp</span></a>
					<img src="images/lingot_kp.png" alt=" Kp"/> 
					|
					Gains:
					<a href="compte"><span class="nbr_header">32.9 €</span></a>
					<img src="images/icone_tirelire.png" alt="€"/>
					<span style="font-size:small;">
						(982 Kp)
					</span>
					
				</div>
								
				<div id="rebours_co">
					<span id="compte_a_rebours1" class="compte_co" >
						<noscript>Merci d\'activer votre JavaScript.</noscript>
					</span>
				</div>
				<img class="temps_lingots" style="margin-right:30px; border:none;" src="images/temps_lingots.png" alt="Fin du tournois"/>';
			
			}
		echo'
		</div>';	

?>	
	</header>