<?php
session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arrête tout
       die('Erreur : '.$e->getMessage());
}
if (isset($_POST['identifiant']) AND isset($_POST['password']))
{
	$requete = $bdd->prepare('SELECT id,identifiant,password FROM membres 
							WHERE identifiant = :identifiant')
							or die(print_r($bdd->errorInfo()));
	$requete->execute(array('identifiant' => $_POST['identifiant']))
							or die(print_r($bdd->errorInfo()));
	$donnees = $requete->fetch();

	
	$mdp_hache = sha1('qw'.$_POST['password']); // on hache le mdp
	
	echo $_POST['identifiant'].'-'.$donnees['identifiant'];
	echo $mdp_hache.'-'.$donnees['password'];

	if ($_POST['identifiant'] == $donnees['identifiant'] 
	AND $mdp_hache == $donnees['password']) 								// si le nom_de_compte et le mdp sont bon 
	{
		$_SESSION['id_membre'] = $donnees['id'];
		$_SESSION['identifiant'] = $_POST['identifiant'];

		if (isset($_POST['connecte']) AND $_POST['connecte'] == 'on')
		{
			setcookie('identifiant', $_POST['identifiant'], time() + 365*24*3600, null, null, false, true);
		}
		unset($_SESSION['inscription']);
		header('Location: accueil');
	}
	else
	{
		$erreur_connection ='fail';
		header('Location: accueil-'.$erreur_connection.'');
	}

}
else
{	
	$erreur_connection ='fail';
	header('Location: accueil-'.$erreur_connection.'');
}