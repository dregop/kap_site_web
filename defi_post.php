<?php
session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arrête tout
       die('Erreur : '.$e->getMessage());
}

// DEMANDE DEFI --------------------------------------------------------------//
if (isset($_POST['identifiant'],$_POST['jeu'],$_POST['somme'])
AND ($_POST['jeu'] == 'esquive' OR $_POST['jeu'] == 'lettre' OR $_POST['jeu'] == 'cible')
AND $_POST['somme'] >= 2 AND $_POST['somme'] <= 100 AND $_POST['identifiant'] != '')
{
	// VERIFICATION QUE LE POST IDENTIFIANT N'EST PAS VIDE
	$re_c = $bdd->prepare('SELECT identifiant FROM membres 
							WHERE identifiant=:id')
							or die(print_r($bdd->errorInfo()));
	$re_c->execute(array('id' => $_POST['identifiant']))
							or die(print_r($bdd->errorInfo()));
	$identifiant_to = $re_c->fetch();
	
	// $identifiant_to est celui a qui on veut envoyer la demande
	if (isset($identifiant_to['identifiant']) AND $identifiant_to['identifiant'] != '')
	{
		/*
			ON RECUPERE LES DEMANDE DE DEFI OU IL A DEJA ENVOYE A CETTE PERSONNE
		*/
		
		$vous = 0;
		$re_c2 = $bdd->prepare('SELECT id FROM demande_defi 
								WHERE identifiant_from=:id_from
								AND identifiant_to = :id_to')
								or die(print_r($bdd->errorInfo()));
		$re_c2->execute(array('id_from' => $_SESSION['identifiant'],
							'id_to' => $identifiant_to['identifiant']))
								or die(print_r($bdd->errorInfo()));
		while($donnees_c2 = $re_c2->fetch())
		{
			// SI LE VOUS S'INCREMENTE C'EST QUE VOUS LUI AVEZ ENVOYE UNE DEMANDE
			$vous++; 
		}

		$lui = 0;
		$re_c2 = $bdd->prepare('SELECT id FROM demande_defi 
								WHERE identifiant_from=:id_from
								AND identifiant_to = :id_to')
								or die(print_r($bdd->errorInfo()));
		$re_c2->execute(array('id_from' => $identifiant_to['identifiant'],
							'id_to' => $_SESSION['identifiant']))
								or die(print_r($bdd->errorInfo()));
		while($donnees_c2 = $re_c2->fetch())
		{
			// SI LUI S'INCREMENTE C'EST QUE CELUI QU'IL VEUT DEFIER LUI A ENVOYE UNE DEMANDE
			$lui++; 
		}
		
		
		if (isset($vous) AND $vous > 0)
		{
			// Vous lui avez déjà envoyé une demande
			header('Location: defi-vous'); 
		}
		elseif (isset($lui) AND $lui > 0)
		{
			// Il vous a déjà envoyé une demande
			header('Location: defi-lui'); 
		}
		elseif ($identifiant_to['identifiant'] == $_SESSION['identifiant'])
		{
			// On ne peut pas se défier soit même !!!
			header('Location: defi.php');
		}
		else // il n'y a rien de tout ca on enregistre
		{	
			$re_c4 = $bdd->prepare('INSERT INTO demande_defi(identifiant_from, 
									identifiant_to,jeu,somme,date) 
									VALUES(:id_from,:id_to,:jeu,:somme,
									NOW())');
			$re_c4->execute(array('id_from' => $_SESSION['identifiant'],
								'id_to' => $identifiant_to['identifiant'],
								'jeu' => $_POST['jeu'],
								'somme' => $_POST['somme']));
			$re_c4->closeCursor(); // Termine le traitement de la requète
			
			// ON RETIRE LA SOMME DE CELUI QUI DEMANDE LE DEFI
			$total_kp= $_SESSION['kp'] - ($_POST['somme'] + 1); // LE +1 PARCE QU'IL PAYE 1 KP
			
			$r_kp= $bdd->prepare('UPDATE membres SET kp=:kp WHERE id=:id')
									or die(print_r($bdd->errorInfo()));
			$r_kp->execute(array('kp' => $total_kp,
									'id' => $_SESSION['id_membre']))
									or die(print_r($bdd->errorInfo()));	
			$r_kp->closeCursor(); // Termine le traitement de la requête 
			
			header('Location: defi-envoye');
		}

	}
}

// TRAITEMENT DEFI SI IL EST ACCEPTE -----------------------------------------//
if (isset($_GET['id_demande'],$_GET['defi_commence'],$_SESSION['kp']))
{
	$requete = $bdd->prepare('SELECT * FROM demande_defi 
							WHERE id=:id')
							or die(print_r($bdd->errorInfo()));
	$requete->execute(array('id' => $_GET['id_demande']))
							or die(print_r($bdd->errorInfo()));
	$donnees_d = $requete->fetch();

	if ($donnees_d['identifiant_to'] == $_SESSION['identifiant'])
	{		
		$requete2 = $bdd->prepare('INSERT INTO defi (identifiant_1, 
								identifiant_2,jeu,somme,debut_defi) 
								VALUES(:id1,:id2,:jeu,:somme,:temps)');
		$requete2->execute(array('id1' => $_SESSION['identifiant'],
								'id2' => $donnees_d['identifiant_from'],
								'jeu' => $donnees_d['jeu'],
								'somme' => $donnees_d['somme'],
								'temps' => time()));
		$requete2->closeCursor(); 
		
		$requete3 = $bdd->prepare('DELETE FROM demande_defi WHERE id=:id')
								or die(print_r($bdd->errorInfo()));
		$requete3->execute(array('id' => $_GET['id_demande'])) 
								or die(print_r($bdd->errorInfo()));
								
		// ON LUI RETIRE LA SOMME DE KP SI IL ACCEPTE
		$total_kp= $_SESSION['kp'] - ($_GET['somme'] + 1); // LE +1 PARCE QU'IL PAYE 1 KP
		
		$r_kp2= $bdd->prepare('UPDATE membres SET kp=:kp WHERE id=:id')
								or die(print_r($bdd->errorInfo()));
		$r_kp2->execute(array('kp' => $total_kp,
								'id' => $_SESSION['id_membre']))
								or die(print_r($bdd->errorInfo()));	
		$r_kp2->closeCursor(); // Termine le traitement de la requête 
							
		header('Location: defi-ready');		
	}
}

// SI LE DEI EST REFUSE ------------------------------------------------------//
if (isset($_GET['id_demande'],$_GET['defi_refuse']))
{
	$requete = $bdd->prepare('SELECT * FROM demande_defi 
							WHERE id=:id')
							or die(print_r($bdd->errorInfo()));
	$requete->execute(array('id' => $_GET['id_demande']))
							or die(print_r($bdd->errorInfo()));
	$donnees_d = $requete->fetch();

	if ($donnees_d['identifiant_to'] == $_SESSION['identifiant'])
	{			
		$requete3 = $bdd->prepare('DELETE FROM demande_defi WHERE id=:id')
							or die(print_r($bdd->errorInfo()));
		$requete3->execute(array('id' => $_GET['id_demande'])) 
							or die(print_r($bdd->errorInfo()));
							
		header('Location: defi.php');		
	}
}

// RETIRER DE L'AFFICHAGE ----------------------------------------------------//
if (isset($_GET['retirer'],$_GET['id'],$_GET['identifiant']))
{
	// ON DOIT SAVOIR SI LE VIEW EST VIDE OU PAS
	$r_view = $bdd->prepare('SELECT view FROM defi
							WHERE id=:id')
							or die(print_r($bdd->errorInfo()));
	$r_view->execute(array('id' => $_GET['id']))
							or die(print_r($bdd->errorInfo()));
	$d_view = $r_view->fetch();
	
	if($d_view['view']== '')
	{
		$r_retirer = $bdd->prepare('UPDATE defi SET view=:view WHERE id=:id')
								or die(print_r($bdd->errorInfo()));
		$r_retirer->execute(array('view' => $_GET['identifiant'],
								'id' => $_GET['id']))
								or die(print_r($bdd->errorInfo()));	
		$r_retirer->closeCursor(); // Termine le traitement de la requête 
		
		header('Location: defi.php');
	}
	else
	{
		$r_retirer = $bdd->prepare('UPDATE defi SET view=:view WHERE id=:id')
								or die(print_r($bdd->errorInfo()));
		$r_retirer->execute(array('view' => 'all',
								'id' => $_GET['id']))
								or die(print_r($bdd->errorInfo()));	
		$r_retirer->closeCursor(); // Termine le traitement de la requête 
			
		header('Location: defi.php');
	}
	
}

?>