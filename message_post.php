<?php
session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arrête tout
    die('Erreur : '.$e->getMessage());
}
if (isset($_POST['message_to']) AND isset($_POST['message']) 
AND $_POST['message'] != '')
{
	if (isset($_POST['recherche']))
	{
		$r_message = $bdd->prepare('SELECT id FROM membres
									WHERE identifiant=:message_to')
									or die(print_r($bdd->errorInfo()));
		$r_message->execute(array('message_to' => $_POST['message_to']))
									or die(print_r($bdd->errorInfo()));
		$d_message= $r_message->fetch();
		$destinataire = $d_message['id'];
	}
	else
		$destinataire = $_POST['message_to'];
	// on insert le message dans la bdd
	$r1 = $bdd->prepare('INSERT INTO messages(message_from,message_to,message,
						view,message_date) 
						VALUES(:message_from,:message_to,:message,:view,NOW())') 
						or die(print_r($bdd->errorInfo()));
	$r1->execute(array('message_from' => $_SESSION['id_membre'],
						'message_to' => $destinataire,
						'message' => $_POST['message'],
						'view' => 0)) 
						or die(print_r($bdd->errorInfo()));
	$r1->closeCursor(); // Termine le traitement de la requête
	
	unset($_SESSION['envoyer_message']);
	
	if (isset($_POST['reponse']))
		header('Location: message-done');
	elseif (!isset($_POST['recherche'],$_POST['reponse']))
		header('Location: '.urlencode(stripslashes(htmlspecialchars($_POST['identifiant_profil']))).'');
		
}
elseif (isset($_GET['id']) AND isset($_GET['supprimer']))
{
	// on insert le message dans la bdd
	$r2 = $bdd->prepare('DELETE FROM messages WHERE id=:id') 
						or die(print_r($bdd->errorInfo()));
	$r2->execute(array('id' => $_GET['id'])) 
						or die(print_r($bdd->errorInfo()));
	$r2->closeCursor(); // Termine le traitement de la requête
	
	header('Location: message');
}
else
{
	$_SESSION['envoyer_message'] = 'on';
	header('Location: '.urlencode(stripslashes(htmlspecialchars($_POST['identifiant_profil']))).'-erreur');
}