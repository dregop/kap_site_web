<?php
include('header.php');
?>
<div id="wrap">
<div class="corps">

	<div style="float:left;width:980px;margin-top:30px;margin-left:22px;">
<?php
		$nbr_demande = 0;
		$r_demande = $bdd->prepare('SELECT * FROM amis 
									WHERE ami_to=:ami_to 
									AND ami_confirm=:ami_confirm
									ORDER BY ami_date DESC LIMIT 0,15')
									or die(print_r($bdd->errorInfo()));
		$r_demande->execute(array('ami_to' => $_SESSION['id_membre'],
									'ami_confirm' => 0))
									or die(print_r($bdd->errorInfo()));
		while ($d_demande= $r_demande->fetch())
		{
			$nbr_demande++;
		}
		echo'
		<p style="text-align:center;margin-top:0px;color:grey;">
			Demande d\'ami ('.$nbr_demande.')
		</p>';

	//REQUETE POUR PRENDRE LES DEMANDE D'AJOUT (ami_to : on lui fait la demande)
	$nbr = 0;
	$r_demande = $bdd->prepare('SELECT * FROM amis 
								WHERE ami_to=:ami_to 
								AND ami_confirm=:ami_confirm
								ORDER BY ami_date DESC LIMIT 0,15')
								or die(print_r($bdd->errorInfo()));
	$r_demande->execute(array('ami_to' => $_SESSION['id_membre'],
								'ami_confirm' => 0))
								or die(print_r($bdd->errorInfo()));
	while ($d_demande= $r_demande->fetch())
	{
		$requete1 = $bdd->prepare('SELECT id, identifiant, photo_profil,
									pays FROM membres
									WHERE id=:id')
									or die(print_r($bdd->errorInfo()));
		$requete1->execute(array('id' => $d_demande['ami_from']))
									or die(print_r($bdd->errorInfo()));
		$donnees1 = $requete1->fetch();
		
		echo'
		<div class="bloc_demande" style="margin-left:15px;">
			<div class="demande_bloc1">';
			
				if(isset($donnees1['photo_profil']) 
				AND $donnees1['photo_profil'] != ''
				AND $donnees1['photo_profil'] != 0)
				{  
					echo'
					<span class="centre_image120">';
					
					$source = getimagesize('images_utilisateurs/'.$donnees1['photo_profil']); 	// La photo est la source
					if ($source[0] <= 30 AND $source[1] <= 30)
						echo '<img src="images_utilisateurs/'.$donnees1['photo_profil'].'" alt="Photo de profil" />';
					else
						echo '<img src="images_utilisateurs/mini_1_'.$donnees1['photo_profil'].'" alt="Photo de profil" />';
					
					echo'
					</span>';
				}
				else
					echo'<img style="opacity:0.6;"src="images/image_defaut_grd.png" alt="Image"/>';
			
			echo'
			</div>
			
			<div class="demande_bloc2">
				<a href="'.urlencode(stripslashes(htmlspecialchars($donnees1['identifiant']))).'">
					<p>'.stripslashes(htmlspecialchars($donnees1['identifiant'].))'</p>
				</a>
			</div>

			<a href="ami_post.php?id_ami='.$donnees1['id'].'&identifiant='.urlencode(stripslashes(htmlspecialchars($donnees1['identifiant']))).'&accepter">
				<div class="demande_bloc3" title="Accepter"></div>
			</a>
			<a href="ami_post.php?id_ami='.$donnees['id'].'&refuser">
				<div class="demande_bloc4" title="Refuser"></div>
			</a>';
			
		echo'
		</div>';
		
		$nbr++;
	
	}
	
	echo'
	</div>';

?>
	<div class="erreur"></div>
</div>
</div>

<?php
include('footer.php');
?>

</body>

</html>