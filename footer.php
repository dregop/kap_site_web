	<footer>
	
		<div id="footer_mountains"></div>
		
		<div id="contient_footer_bottom">
			<div id="footer_bottom">
				<div id="copyright">	
					Kap © 2015 
					Tous droits réservés	
				</div>
				<div id="projet">		
					<p>
						Projet
					</p>
					<span>
						Apporter à toutes et à tous l'opportunité de gagner de l'argent
						par le biais de jeux simples. </br >
						A vos claviers !
					</span>
				</div>
				<div id="social">		
					<p>
						Social
					</p>
					<div id="footer_facebook" class="icone_social"> </div>
					<div id="footer_twitter" class="icone_social"> </div>
				</div>
				<div id="footer_securite">		
					<p>
						Securité
					</p>
					<a href="https://www.paypal.com"  target="_blank">
						<div id="footer_paypal"> </div>
					</a>
				</div>
			</div>
		</div>
	</footer>
	<script type="text/javascript" src="javascript/compte_a_rebours.js"></script>
	
	<!-- Background moving --->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="javascript/mouse.parallax.js"></script>
	<script>
		$(document).ready(function() {
		$('#background5').mouseParallax({ moveFactor: 2 });
		$('#background4').mouseParallax({ moveFactor: 3 });
		$('#background3').mouseParallax({ moveFactor: 8 });
		 
		$('body').height(100);
		});
	</script>