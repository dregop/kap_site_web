<?php

	// PAS BESOIN DE PROTEGER CES DONNEES ON NE LES AFFICHE PAS --------------//
	if (isset($_GET['erreur']) AND $_GET['erreur'] == 'ail')
	{
		echo '
		<div id="fond_overlay" style="display:block;"></div>
		<div id="overlay_rouge" style="display:block;">
			Identifiant ou mot de passe incorrect
		</div>';
	}

	if (isset($_GET['erreur']) AND $_GET['erreur'] == 'ncomplet')
	{
		echo '
		<div id="fond_overlay" style="display:block;"></div>
		<div id="overlay_rouge" style="display:block;">
			Veuillez compléter entièrement l\'inscription
		</div>';
	}
	elseif (isset($_GET['erreur']) AND $_GET['erreur'] == 'same')
	{
		echo '
		<div id="fond_overlay" style="display:block;"></div>
		<div id="overlay_rouge" style="display:block;">
			Cet identifiant existe déjà
		</div>';
	}
	elseif (isset($_GET['erreur']) AND $_GET['erreur'] == 'e1')
	{
		echo '
		<div id="fond_overlay" style="display:block;"></div>
		<div id="overlay_rouge" style="display:block;">
			L\'identifiant n\'est pas conforme. Utilisez une appellation simple 
			comme: azerty12
			ou poi098
		</div>';
	}
	elseif (isset($_GET['erreur']) AND $_GET['erreur'] == 'e2')
	{
		echo '
		<div id="fond_overlay" style="display:block;"></div>
		<div id="overlay_rouge" style="display:block;">
			L\'identifiant doit être compris entre 4 et 12 caractères
		</div>';
	}
	elseif (isset($_GET['erreur']) AND $_GET['erreur'] == 'e3')
	{
		echo '
		<div id="fond_overlay" style="display:block;"></div>
		<div id="overlay_rouge" style="display:block;">
			L\'identifiant doit être compris entre 4 et 12 caractères
		</div>';
	}
	elseif (isset($_GET['erreur']) AND $_GET['erreur'] == 'email1')
	{
		echo '
		<div id="fond_overlay" style="display:block;"></div>
		<div id="overlay_rouge" style="display:block;">
			Cette adresse e-mail existe déjà
		</div>';
	}
	elseif (isset($_GET['erreur']) AND $_GET['erreur'] == 'email2')
	{
		echo'
		<div id="fond_overlay" style="display:block;"></div>
		<div id="overlay_rouge" style="display:block;">
			L\'adresse e-mail n\'est pas conforme. Utilisez ce type d\'adresse: 
			azerty-123@.....
		</div>';
	}
	elseif (isset($_GET['erreur']) AND $_GET['erreur'] == 'diff')
	{
		echo'
		<div id="fond_overlay" style="display:block;"></div>
		<div id="overlay_rouge" style="display:block;">
			Les deux mots de passe ne sont pas identiques
		</div>';
	}
	elseif (isset($_GET['erreur']) AND $_GET['erreur'] == 'fail')
	{
		echo'
		<div id="fond_overlay" style="display:block;"></div>
		<div id="overlay_rouge" style="display:block;">
			Le mot de passe n\'est pas conforme (caractères (4-25) spéciaux acceptés: é & è @ à ç $ * ù)
		</div>';
	}
	if (isset($_GET['onglet']))
	{
		echo'
		<div id="fond_overlay" style="display:block;"></div>
		<div id="overlay_rouge" style="display:block;">
			Merci de ne pas changer l\'onglet pendant le jeu
		</div>';
	}
?>