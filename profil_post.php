<?php
session_start();
try
{
	include('bdd_name.php');
}
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arrête tout
	die('Erreur : '.$e->getMessage());
}

//ON TEST L'IMAGE///////////////////////////////////////////////////////////////
include('test_image.php');
////////////////////////////////////////////////////////////////////////////////


// MODIFICATION SOUS COMPTE INFORMATIONS CONNECTE ------------------------------
if (isset($_FILES['photo_profil']['name']) 
AND $_FILES['photo_profil']['size']<=1000000)
{
	if ($_FILES['photo_profil']['name'] != '')
	{
		if (isset($presence_photo) AND $presence_photo == 'off')  { $photo=''; }
		
		$req = $bdd->prepare('UPDATE membres SET photo_profil=:photo_profil 
								WHERE identifiant=:identifiant')
							or die(print_r($bdd->errorInfo()));
		$req->execute(array('photo_profil' => $photo,
							'identifiant' => $_SESSION['identifiant']))
							or die(print_r($bdd->errorInfo()));	
		$req->closeCursor(); // Termine le traitement de la requête
		header('Location: '.urlencode(stripslashes(htmlspecialchars($_POST['identifiant']))).'');
	}
	else
		header('Location: '.urlencode(stripslashes(htmlspecialchars($_POST['identifiant']))).'-image');
}
else
		header('Location: '.urlencode(stripslashes(htmlspecialchars($_POST['identifiant']))).'-image');
